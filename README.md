# ES5 Minifier

[![wercker status](https://app.wercker.com/status/2093e549b41c5df211ee2fe37225dcbc/s "wercker status")](https://app.wercker.com/project/bykey/2093e549b41c5df211ee2fe37225dcbc)[![codecov.io](https://codecov.io/bitbucket/skoppe/es5-min/coverage.svg?branch=master)](https://codecov.io/bitbucket/skoppe/es5-min?branch=master)

This project is build on top of the [D ES6-Grammar](https://bitbucket.org/skoppe/es6-grammar).

# Goals

1. To improve in speed and compression compared to popular minifiers.
2. To have a minifier written in a sane language.

# Current State

- The unittest suite for RxJS and jQuery has been passed on the JavaScript emitted by this minifier.
- The minified code (for RxJS and jQuery) is around 14 percent smaller than produced by Uglify2.
- It consumes a hell of a lot of resources

# Minifications

- Rewrites if-return statements  
	`function c() { if (a) return; b() }` => `function c() { if (!a) b() }`
- Rewrites if-continue statements  
	`for(;;) { if (a) continue; break }` => `for(;;) { if (!a) break }`
- Rewrites if-break statements in IterationStatements  
	`for (;;) { if (a) break; b() }` => `for (;!a;) { b() }`
- Rewrites if assignments into ConditionalExpressions with assignment  
	`if (a) b = 1; else b = 2;` => `b = a ? 1 : 2;`
- Rewrites IfStatements with else path into ConditionalExpressions  
	`if (a) b(); else c();` => `a ? b() : c();`
- Rewrites ReturnStatements into single Statement  
	`function f() { if (a) return 5; else if (b) return 6; else return 7 }` => `function f() { return a ? 5 : b ? 6 : 7 }`
- Rewrites IfStatements into BinaryExpressions  
	`if (a) b();` => `a && b()`
- Parameterizes often used StringLiterals  
	`var a = "somelongstring", b = "somelongstring";` => `var x = "somelongstring", a = x, b = x;`
- Parameterizes often used NullLiterals  
	`function a(b,c) { if (b == null) return null; if (c == null) return b.x != null; return null }` => `function a(b,c) { var a=null; if (b == a) return a; if (c == a) return b.x != a; return a }`
- Parameterizes often used this keyword  
	`function a(b,c) { if (this.fun(b)) return this.value; if (this.bar(c)) return this.foo(this.value); return this.value; }` => `function a(b,c) { var a=this; if (a.fun(b)) return a.value; if (a.bar(c)) return a.foo(a.value); return a.value; }`
- Parameterizes often used accessors  
	`if (!!obj.longAccessor) obj.longAccessor = 5; else obj.longAccessor = 4;` => `var a="longAccessor"; if (!!obj[a]) obj[a] = 5; else obj[a] = 4;`
- Evaluates BinaryExpressions  
	`var a=10-4*2;` => `var a=2;`
- Short circuit IfStatements  
	`if (true) a(); else b();` => `a();`
- Combines nested IfStatements  
	`if (a) if (b) c();` => `if (a && b) c();`
- Removes unnecessary parentheses  
	`var a = (4 + 5) + 4;` => `var a = 4 + 5 + 4;`
- Removes unused parameters  
	`function a(b,c) { return 7 }` => `function a() { return 7 }`
- Combines AssignmentExpressions with ReturnStatement  
	`function a() { b = 5; return c }` => `function a() { return b = 5, c }`
- Moves AssignmentExpressions into IfStatements  
	`a = 5; if (b) c();` => `if (a=5,b) c();`
- Removes unnecessary BreakStatements  
	`switch(a) { case 4: b(); break }` => `switch(a) { case 4: b() }`
- Unstringify numerical LiteralPropertyNames  
	`var a = {"23":23};` => `var a = {23:23};`
- Moves remaining statements in else block when if path returns  
	`function a() { if (b) return 7; c() }` => `function a() { if (b) return 7 else c() }`
- Converts hexadecimals to decimals  
	`var a=0x20;` => `var a=32;`
- Removes StatementBlocks from switch cases  
	`switch(a){case b: { c() } }` => `switch(a){case b: c() }`

# Known Problems

Due to the way the [ECMAScript 6 grammar](https://bitbucket.org/skoppe/es6-grammar) is specified, the parser generated with [Pegged](https://github.com/PhilippeSigaud/Pegged) consumes lots of time and memory. I am investigating solutions.

# Future

- Improve resource usage
- There are some types of minifications that haven't been implemented yet. See issues.
- Expand this project to emit minified ES6 as well
- Build other tools around it (linter, transpiler, js packager, dev server)