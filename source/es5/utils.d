module es5.utils;

void moveElement(T)(ref T[] arr, size_t from, size_t to)
{
	import std.algorithm : moveAll;
	if (from == to)
		return;
	assert(arr.length > from && arr.length > to);
	T org = arr[from];
	if (from < to)
		moveAll(arr[from+1 .. to+1], arr[from .. to]);
	else
	{
		foreach(idx; 0 .. (from-to))
			arr[from-idx] = arr[from-idx-1];
	}
	arr[to] = org;
}
unittest
{
	auto a = [1,2,3,4,5,6];
	a.moveElement(2,4);
	assert(a == [1,2,4,5,3,6]);
}
unittest
{
	auto a = [1,2,3,4,5,6];
	a.moveElement(5,3);
	assert(a == [1,2,3,6,4,5]);
}
unittest
{
	auto a = [1,2,3,4,5];
	a.moveElement(0,0);
	assert(a == [1,2,3,4,5]);
}
auto removeRange(T,Idx)(ref T[] arr, Idx from, Idx to)
{
	import std.algorithm : min, remove;
	import std.typecons : tuple;
	to = min(arr.length,to);
	if (from >= to)
		return arr;
	return arr.remove(tuple(from,to));
}
unittest
{
	auto a = [1,2,3,4];
	assert(a.removeRange(0,0) == [1,2,3,4]);
	a = [1,2,3,4];
	assert(a.removeRange(10,0) == [1,2,3,4]);
	a = [1,2,3,4];
	assert(a.removeRange(1,1) == [1,2,3,4]);
	a = [1,2,3,4];
	assert(a.removeRange(1,2) == [1,3,4]);
	a = [1,2,3,4];
	assert(a.removeRange(1,909) == [1]);
}
void removeChild(T)(T node, size_t child)
{
	if (child < node.children.length)
	{
		foreach(idx; child .. node.children.length-1)
			node.children[idx] = node.children[idx+1];
		node.children.length = node.children.length - 1;
	}
}
ptrdiff_t findIndex(T)(T grp, T sib)
{
	foreach(idx, n; grp.children)
		if (n is sib)
			return idx;
	return -1;
}
void removeChild(T)(T node, T child)
{
	removeChild(node,findIndex(node,child));
}
import std.traits : isArray;
void insertAtFront(Ts, T)(ref Ts arr, T element)
	if (isArray!Ts && !isArray!T)
{
	import std.array : insertInPlace;
	arr.insertInPlace(0, [ element ]);
}
void insertAtFront(Ts)(ref Ts arr, Ts elements)
	if (isArray!Ts)
{
	import std.array : insertInPlace;
	arr.insertInPlace(0, elements);
}
void insertAtFront(T, T2)(T node, T2 element)
{
	insertAtFront(node.children,element);
	static if (isArray!T2)
	{
		foreach(c; element)
			c.parent = node;
	} else
		element.parent = node;
}
void insertAtBack(Ts, T)(ref Ts arr, T element)
	if (isArray!Ts)
{
	arr ~= element;
}
void insertAtBack(T, T2)(T node, T2 element)
{
	insertAtBack(node.children,element);
	static if (isArray!T2)
	{
		foreach(c; element)
			c.parent = node;
	} else
		element.parent = node;
}
void insertInPlace(T, Ts)(T parent, size_t idx, Ts children)
	if (isArray!Ts)
{
	import std.array : insertInPlace;
	foreach(c; children)
		c.parent = parent;
	parent.children.insertInPlace(idx, children);
}
void insertInPlace(T)(T parent, size_t idx, T child)
{
	import std.array : insertInPlace;
	child.parent = parent;
	parent.children.insertInPlace(idx, child);
}
void setChild(T)(T node, size_t idx, T child)
{
	child.parent = node;
	node.children[idx] = child;
}
void removeFromParent(T)(T item)
{
	item.parent.removeChild(item);
}
unittest
{
	auto a = [2,3,4,5];
	a.insertAtFront(1);
	assert(a == [1,2,3,4,5]);
}
unittest
{
	auto a = [1,2,3,4];
	a.insertAtBack(5);
	assert(a == [1,2,3,4,5]);
	a.insertAtBack([6,7,8]);
	assert(a == [1,2,3,4,5,6,7,8]);
}
enum validStartingIdentifierCharacter = "eatnorcidfsubplhmgkvywjCEDIxMRST_PONALqUFBVJzWHGKYZXQ$";

string generateName(int idx)
{
	enum len = validStartingIdentifierCharacter.length;
	import std.conv : to;
	if (idx < len)
		return validStartingIdentifierCharacter[idx..idx+1];

	return generateName(cast(int)((idx / (len+0))-1))~generateName(idx % (len+0));
}
import std.regex;
auto reservedKeyword = ctRegex!`^(break|do|in|typeof|case|else|instanceof|var|catch|export|new|void|class|extends|return|while|const|finally|super|with|continue|for|switch|yield|debugger|function|this|default|if|throw|delete|import|try|enum|await|null|true|false)$`;
bool isValidIdentifier(string id)
{
	return id.matchFirst(reservedKeyword).empty();
}
version (unittest)
{
	import es5.testhelpers;
	import unit_threaded;
}
unittest
{
	void assertGeneratedName(int idx, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : writeln;
		import std.conv : to;
		auto got = generateName(idx);
		if (got == expected)
			return;
		Message m;
		m.write("Expected generateName(");
		m.write(idx.to!string);
		m.write(") to return `");
		m.write(expected);
		m.write("`, got `");
		m.write(got);
		m.writeln("`");
		failed(m,file,line);
	}
	import std.conv : to;

	assertGeneratedName(0,		"e");
	assertGeneratedName(1,		"a");
	assertGeneratedName(26,		"I");
	assertGeneratedName(51,		"X");
	assertGeneratedName(52,		"Q");
	assertGeneratedName(53,		"$");
	assertGeneratedName(54,		"ee");
	assertGeneratedName(2969,	"$$");
	assertGeneratedName(2970,	"eee");
	assert(!isValidIdentifier("do"));
	assert(isValidIdentifier("doda"));
	assert(!isValidIdentifier("function"));
	assert(isValidIdentifier("functioned"));
}