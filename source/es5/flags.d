module es5.flags;

// NOTE: THIS whole file can be removed WHEN gdc/ldc supports a phobos version that has std.typecons : BitFlags;

import std.typecons : Flag, No;
import std.traits : isIntegral, EnumMembers, OriginalType, isImplicitlyConvertible;

/**
Tests whether all given items satisfy a template predicate, i.e. evaluates to
$(D F!(T[0]) && F!(T[1]) && ... && F!(T[$ - 1])).

Evaluation is $(I not) short-circuited if a false result is encountered; the
template predicate must be instantiable with all the given items.
 */
template allSatisfy(alias F, T...)
{
    static if (T.length == 0)
    {
        enum allSatisfy = true;
    }
    else static if (T.length == 1)
    {
        enum allSatisfy = F!(T[0]);
    }
    else
    {
        enum allSatisfy =
            allSatisfy!(F, T[ 0  .. $/2]) &&
            allSatisfy!(F, T[$/2 ..  $ ]);
    }
}

/**
Detect whether an enum is of integral type and has only "flag" values
(i.e. values with a bit count of exactly 1).
Additionally, a zero value is allowed for compatibility with enums including
a "None" value.
*/
template isBFlagEnum(E)
{
    static if (is(E Base == enum) && isIntegral!Base)
    {
        enum isBFlagEnum = (E.min >= 0) &&
        {
            foreach (immutable flag; EnumMembers!E)
            {
                Base value = flag;
                value &= value - 1;
                if (value != 0) return false;
            }
            return true;
        }();
    }
    else
    {
        enum isBFlagEnum = false;
    }
}

struct BitFlags(E, Flag!"unsafe" unsafe = No.unsafe) if (unsafe || isBFlagEnum!(E))
{
@safe @nogc pure nothrow:
private:
    enum isBaseEnumType(T) = is(E == T);
    alias Base = OriginalType!E;
    Base mValue;
    static struct Negation
    {
    @safe @nogc pure nothrow:
    private:
        Base mValue;

        // Prevent non-copy construction outside the module.
        @disable this();
        this(Base value)
        {
            mValue = value;
        }
    }

public:
    this(E flag)
    {
        this = flag;
    }

    this(T...)(T flags)
        if (allSatisfy!(isBaseEnumType, T))
    {
        this = flags;
    }

    bool opCast(B: bool)() const
    {
        return mValue != 0;
    }

    Base opCast(B)() const
        if (isImplicitlyConvertible!(Base, B))
    {
        return mValue;
    }

    Negation opUnary(string op)() const
        if (op == "~")
    {
        return Negation(~mValue);
    }

    auto ref opAssign(T...)(T flags)
        if (allSatisfy!(isBaseEnumType, T))
    {
        mValue = 0;
        foreach (E flag; flags)
        {
            mValue |= flag;
        }
        return this;
    }

    auto ref opAssign(E flag)
    {
        mValue = flag;
        return this;
    }

    auto ref opOpAssign(string op: "|")(BitFlags flags)
    {
        mValue |= flags.mValue;
        return this;
    }

    auto ref opOpAssign(string op: "&")(BitFlags  flags)
    {
        mValue &= flags.mValue;
        return this;
    }

    auto ref opOpAssign(string op: "|")(E flag)
    {
        mValue |= flag;
        return this;
    }

    auto ref opOpAssign(string op: "&")(E flag)
    {
        mValue &= flag;
        return this;
    }

    auto ref opOpAssign(string op: "&")(Negation negatedFlags)
    {
        mValue &= negatedFlags.mValue;
        return this;
    }

    auto opBinary(string op)(BitFlags flags) const
        if (op == "|" || op == "&")
    {
        BitFlags result = this;
        result.opOpAssign!op(flags);
        return result;
    }

    auto opBinary(string op)(E flag) const
        if (op == "|" || op == "&")
    {
        BitFlags result = this;
        result.opOpAssign!op(flag);
        return result;
    }

    auto opBinary(string op: "&")(Negation negatedFlags) const
    {
        BitFlags result = this;
        result.opOpAssign!op(negatedFlags);
        return result;
    }

    auto opBinaryRight(string op)(E flag) const
        if (op == "|" || op == "&")
    {
        return opBinary!op(flag);
    }
}