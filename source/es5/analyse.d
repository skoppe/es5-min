module es5.analyse;

version (unittest){
	import unit_threaded;
	import es5.testhelpers;
}
import es5.node;
import es6.grammar;
struct AnalysisResult
{
	Scope scp;
	Branch trunk;
	string[] unresolvedIdentifiers;
}
enum Type { Normal, NewScope };
/++
	Sets the hints for this node and returns hints for parent node
+/
Hints processHints(Node node, Hints childHints = Hints.init)
{
	import std.algorithm : startsWith;
	Hints mask;
	if (node.isA!"ES6.FunctionStatementList")
		return mask;
	node.hint = childHints;
	if (node.isA!"ES6.VariableStatement")
		node.hint |= Hint.VariableDecl;
	else if (node.isA!"ES6.Parentheses")
	{
		node.hint &= ~(Hint.LogicalOr | Hint.Assignment);
	}
	else if (node.isA!"ES6.TryStatement")
		node.hint |= Hint.NonValueGeneratingStatement;
	else if (node.isA!"ES6.ThrowStatement")
		node.hint |= Hint.NonValueGeneratingStatement;
	else if (node.isA!"ES6.FunctionDeclaration")
		node.hint |= Hint.FunctionDecl;
	else if (node.isA!"ES6.BreakStatement")
		node.hint |= Hint.Break | Hint.NonValueGeneratingStatement;
	else if (node.isA!"ES6.AssignmentOperator")
		node.hint |= Hint.Assignment;
	else if (node.isA!"ES6.LogicalOperator")
	{
		if (node.matches[0].startsWith("||"))
			node.hint |= Hint.LogicalOr;
	}
	else if (node.isA!"ES6.ReturnStatement")
	{
		node.hint |= Hint.ReturningBranch | Hint.EmitReturn;
		if (node.children.length == 0)
			node.hint |= Hint.EmptyReturn;
		else
			node.hint |= Hint.Return;
	} else if (node.isA!"ES6.IfStatement")
	{
		node.hint |= Hint.NonValueGeneratingStatement;
		node.hint &= ~Hint.Assignment;
		import std.algorithm : all;

		if (node.children.length == 4 && node.children[1].hint & Hint.ReturningBranch && node.children[3].hint & Hint.ReturningBranch)
		{
			node.hint |= Hint.ReturningBranch | Hint.DeducedReturingBranch;
			node.hint &= ~Hint.EmitReturn;
		}
		else
			node.hint &= ~(Hint.EmptyReturn | Hint.Return | Hint.ReturningBranch | Hint.DeducedReturingBranch);
	} else if (node.isA!"ES6.CaseClauses" || node.isA!"ES6.CaseBlock")
	{
		node.hint = node.hint & ~(Hint.Break | Hint.ReturningBranch | Hint.Return | Hint.EmptyReturn | Hint.DeducedReturingBranch | Hint.EmitReturn);
	} else if (node.isA!"ES6.SwitchStatement" || node.isA!"ES6.ContinueStatement")
		node.hint |= Hint.NonValueGeneratingStatement;
	else if (node.isA!"ES6.IterationStatement")
	{
		node.hint |= Hint.NonValueGeneratingStatement;
		node.hint &= ~Hint.Break;
	}

	return node.hint & ~node.getHintMask;
}
Hints getHintMask(Node node)
{
	Hints mask;
	if (node.isA!"ES6.StatementListItem")
	{
		mask |= (Hint.VariableDecl | Hint.FunctionDecl);
	} else if (node.isA!"ES6.FunctionBody")
		mask |= ~Hint.None;
	return mask;
}
/++
	Walks the nodes and sets scopes, branches and hints.

	Returns: AnalysisResult struct with global scope objects and main branch.
+/
AnalysisResult analyseNodes(Node root)
{
	Scope s = new Scope(null);
	s.entry = root;
	Branch b = new Branch(null,root);
	Scope all = s;
	Branch trunk = b;
	void setScopeAndBranch(Node[] ns, Scope s, Branch b)
	{
		foreach(n; ns)
		{
			n.scp = s;
			n.branch = b;
			setScopeAndBranch(n.children,s,b);
		}
	}
	Hints impl(Node node, Scope s, Branch b)
	{
		node.scp = s;
		node.branch = b;
		Hints childHints;
		Hints analyseChildren(Node[] ns, Scope s, Branch b)
		{
			Hints hs;
			foreach(idx, n; ns)
			{
				auto hint = impl(n,s,b);
				hs |= hint;
			}
			return hs;
		}
		if (node.isA!"ES6.FunctionDeclaration")
		{
			auto name = node.getChild("ES6.Identifier").matches[0];
			s.addVariable(Variable(name,IdentifierType.Function,node.children[0]));
			auto ns = new Scope(s);
			analyseChildren(node.children[0..1],s,b);
			s.scopes ~= ns;
			if (node.children[2].children.length > 0)
			{
				auto functionBody = node.getFunctionDeclarationsFunctionBody;
				ns.entry = functionBody;
			} else
				ns.entry = node.children[2];
			analyseChildren(node.children[1..$],ns,new Branch(null,ns.entry));
			return processHints(node);
		} else if (node.isA!"ES6.ArrowFunction")
		{
			auto ns = new Scope(s);
			auto functionBody = node.getArrowFunctionsFunctionBody;
			ns.entry = functionBody;
			s.scopes ~= ns;
			analyseChildren(node.children,ns,new Branch(null,functionBody));
			return processHints(node);
		} else if (node.isA!"ES6.FunctionExpression")
		{
			auto ns = new Scope(s);
			size_t codeIndex = 1;
			if (node.children.length == 3)
			{
				auto name = node.getChild("ES6.Identifier").matches[0];
				s.addVariable(Variable(name,IdentifierType.Function,node.children[0]));
				++codeIndex;
			}
			assert(node.children.length > codeIndex,"Can't find CurlyBrackets");
			if (node.children[codeIndex].children.length > 0)
			{
				ns.entry = node.children[codeIndex].children[0];
				s.scopes ~= ns;
				analyseChildren(node.children,ns,new Branch(null,node.children[codeIndex].children[0]));
			} else
				analyseChildren(node.children,s,b);
			return processHints(node);
		}/* else if (node.isA!"ES6.MethodDefinition")
		{
			auto ns = new Scope(s);
			s.scopes ~= ns;
			analyseChildren(node.children,ns,b);
		}*/
		else if (node.isA!"ES6.VariableDeclarationList")
		{
			foreach(c; node.children)
			{
				auto name = c.getChild("ES6.Identifier").matches[0];
				s.addVariable(Variable(name,IdentifierType.Variable,c));
			}
		}
		else if (node.isA!"ES6.ArrowParameters")
		{
			auto ls = node.getNthChild(3);
			if (ls !is null)
			{
				foreach(c; ls.children)
				{
					/// TODO: c can be CoverParenthesizedExpressionAndArrowParameterList as well, in which case this fails
					if (c.isA!"ES6.Comma")
						continue;
					auto name = c.getChild("ES6.Identifier").matches[0];
					s.addVariable(Variable(name,IdentifierType.Parameter,c));
				}
			}
		}
		else if (node.isA!"ES6.IdentifierReference")
		{
			import std.algorithm : canFind;
			s.addIdentifier(node.matches[0]);
			setScopeAndBranch(node.children,s,b);
			return processHints(node);
		}
		else if (node.isA!"ES6.FieldAccessor")
			return processHints(node);
		else if (node.isA!"ES6.FormalParameterList")
		{
			foreach(c; node.children[0].children)
			{
				auto name = c.matches[0];
				s.addVariable(Variable(name,IdentifierType.Parameter,c));
			}
		} else if (node.isA!"ES6.IfStatement")
		{
			analyseChildren(node.children[0..1],s,b);
			if (node.children.length > 1)
			{
				Branch nb = b.branch(node.children[1]);
				childHints = analyseChildren(node.children[1..2],s,nb);
			}
			if (node.children.length == 4)
			{
				Branch nb = b.branch(node.children[3]);
				childHints |= analyseChildren(node.children[3..$],s,nb);			
			}
			return processHints(node,childHints);
		} else if (node.name == "ES6.CaseClause" || node.name == "ES6.CaseClauseReturn" || node.name == "ES6.CaseClauseYield" || node.name == "ES6.CaseClausesYieldReturn")
		{
			analyseChildren(node.children[0..1],s,b);
			if (node.children.length > 1)
			{
				Branch nb = b.branch(node.children[1]);
				childHints = analyseChildren(node.children[1..2],s,nb);
			}
			return processHints(node,childHints);
		} else if (node.isA!"ES6.DefaultClause")
		{
			if (node.children.length > 0)
			{
				Branch nb = b.branch(node.children[0]);
				childHints = analyseChildren(node.children,s,nb);
			}
			return processHints(node,childHints);
		} else if (node.isA!"ES6.IterationStatement")
		{
			if (node.isDoWhileIteration)
			{
				analyseChildren(node.children[1..$],s,b);
				auto stmt = node.children[1];
				Branch nb = b.branch(stmt);
				childHints = analyseChildren(node.children[1..2],s,nb);
			} else
			{
				analyseChildren(node.children[0..$-1],s,b);
				auto stmt = node.children[$-1];
				Branch nb = b.branch(stmt);
				childHints = analyseChildren(node.children[$-1..$],s,nb);
			}
			return processHints(node,childHints);
		}
		childHints |= analyseChildren(node.children,s,b);
		return processHints(node,childHints);
	}
	impl(root,s,b);
	propagateGlobals(s);
	auto ar = AnalysisResult(all,trunk);
	ar.fetchUnresolvedIdentifiers();
	return ar;
}
/++
	Returns list of identifiers that do not refer to any
	declared variable (but to an global declared elsewhere).

	This is used in variable renaming. A variable cannot
	be renamed to a (used) global identifier.

	Common global identifiers include jQuery, or simply any
	assignment to the window object.
+/
string[] fetchUnresolvedIdentifiers(ref AnalysisResult ar)
{
	ar.unresolvedIdentifiers = es5.node.getNonLocalIdentifiers(ar.scp);
	return ar.unresolvedIdentifiers;
}
unittest
{
	/// test scopes
	auto getScopes(string input)
	{
		auto p = ES6(input);
		auto nodes = createNode(p);
		return analyseNodes(nodes).scp;
	}
	auto s1 = getScopes(`var a = 6; function bla(){ huppa = 4; };`);
	assert(s1.entry.isA!("ES6"));
	assert(s1.scopes[0].entry.isA!"ES6.FunctionBody");
	assert(s1.scopes[0].entry.parent.parent.children[0].matches == ["bla"]);

	auto s2 = getScopes(`var a = (boo)=>{ huppa = 4};`);
	assert(s2.entry.isA!("ES6"));
	assert(s2.scopes[0].entry.isA!"ES6.FunctionBody");
	assert(s2.scopes[0].entry.parent.parent.parent.children[0].matches == ["(","boo",")"]);
	//assert(s2.scopes[0].identifiers == ["boo","huppa"]);

	auto s3 = getScopes(`var f = { a: function (b) { d = b; } };`);
	assert(s3.entry.isA!("ES6"));
	assert(s3.scopes[0].entry.isA!"ES6.FunctionBody");
	
	auto s4 = getScopes(`var f = { a: function name(b) { d = b; } };`);
	assert(s4.entry.isA!("ES6"));
	assert(s4.scopes[0].entry.isA!"ES6.FunctionBody");
	// TODO test MethodDefinition
}
unittest
{
	/// test branches
	auto getBranches(string input)
	{
		auto p = ES6(input);
		auto nodes = createNode(p);
		return analyseNodes(nodes).trunk;
	}
	auto getFirstFunctionsBranch(Branch b)
	{
		return b.entry.scp.scopes[0].entry.branch;
	}
	auto assertJs(Node node, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import es5.tojs;
		import es5.testhelpers;
		string got = node.toJS();
		if (got == expected)
			return;
		Message m;
		m.writeln("Branch Failure");
		m.writeln("Expected:");
		m.writeln(expected);
		m.writeln("Got:");
		m.writeln(got);
		failed(m,file,line);
	}
	auto b1 = getBranches("if (c) d = 5; e = 5;");
	assert(b1.entry.isA!"ES6");
	assert(b1.children[0].entry.matches == ["d", "=", "5"]);

	auto b2 = getBranches("if (c) d = 5; else f = 5; e = 5;");
	assert(b2.entry.isA!"ES6");
	assert(b2.children[0].entry.matches == ["d", "=", "5"]);
	assert(b2.children[1].entry.matches == ["f", "=", "5"]);

	auto b3 = getBranches("if (c) d = 5; else if (g) f = 5; else h = 5; e = 5;");
	assert(b3.entry.isA!"ES6");
	assert(b3.children[0].entry.matches == ["d", "=", "5"]);
	assert(b3.children[1].entry.matches == ["(", "g", ")", "f", "=", "5", "else", "h", "=", "5"]);
	assert(b3.children[1].children[0].entry.matches == ["f", "=", "5"]);
	assert(b3.children[1].children[1].entry.matches == ["h", "=", "5"]);

	// if all branches are a ReturningBranch, the parent branch must also be
	auto b4 = getFirstFunctionsBranch(getBranches("function def (a) { if (c) if (d) return 4; else return 3; }"));
	assert(b4.children[0].children[0].entry.hint & Hint.ReturningBranch);
	assert(b4.children[0].children[1].entry.hint & Hint.ReturningBranch);
	assert(b4.children[0].entry.hint & Hint.ReturningBranch);
	assert(b4.children[0].entry.hint & Hint.DeducedReturingBranch);

	auto b5 = getFirstFunctionsBranch(getBranches("function def(a) { if (c) if (d) return 4; else if (c) { d = 3; return 3 }; else { e = 5; return 5; }}"));
	assert(b5.children[0].children[0].entry.hint & Hint.ReturningBranch);
	assert(b5.children[0].children[1].entry.hint & Hint.ReturningBranch);
	assert(b5.children[0].entry.hint & Hint.ReturningBranch);
	assert(b5.children[0].entry.hint & Hint.DeducedReturingBranch);
	/*
		Todo: What about try-catch-finally, while, for??
	*/
	auto b6 = getFirstFunctionsBranch(getBranches("function bla() { switch(a){ case 7: d=5; case 5: d=7; default: d=3; }; }"));
	assert(b6.entry.getNthChild(2).hint & Hint.NonValueGeneratingStatement);
	assert(b6.children.length == 3);
	assert(b6.children[0].entry.matches == ["d", "=", "5"]);
	assert(b6.children[1].entry.matches == ["d", "=", "7"]);
	assert(b6.children[2].entry.matches == ["d", "=", "3"]);

	auto b7 = getFirstFunctionsBranch(getBranches("function bla() { switch(a){ case 7: if(c) return 7; return 5; case 5: break; default: return 5;}; }"));
	assert(b7.entry.getNthChild(2).hint & Hint.NonValueGeneratingStatement);
	assert(b7.children.length == 3);
	assert(b7.children[0].entry.hint & Hint.ReturningBranch);
	assert(!(b7.children[1].entry.hint & Hint.ReturningBranch));
	assert(b7.children[2].entry.hint & Hint.ReturningBranch);
	assertJs(b7.children[2].entry,"return 55").shouldThrow();
	assertJs(b7.children[2].entry,"return 5");
	assertJs(b7.children[2].entry.parent,"default: return 5");
	assert(b7.children[0].entry.parent.isA!"ES6.CaseClause");
	assert(b7.children[1].entry.parent.isA!"ES6.CaseClause");
	assert(b7.children[2].entry.parent.parent.parent.isA!"ES6.CaseBlock");
}
unittest
{
	/// test hints
	auto getNodes(string js)
	{
		auto p = ES6(js);
		auto nodes = createNode(p);
		analyseNodes(nodes);
		return nodes;
	}
	void assertPredHint(alias errMsg, alias pred)(Node node, Hints h, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.traits : EnumMembers;
		import std.conv : to;
		import std.range : join;
		import std.stdio : writeln;
		string[] hints;
		foreach(M; EnumMembers!Hint)
		{
			if (h & M)
			{
				if (pred(node.hint,M))
					hints ~= M.to!string;
			}
		}
		if (hints.length == 0)
			return;
		Message m;
		m.writeln(node);
		m.writeln(errMsg~":\n"~hints.map!(h=>"\t"~h).join("\n"));
		failed(m,file,line);
	}
	void assertNoHint(Node node, Hints h2)
	{
		assertPredHint!("Node shouldn't have hints",(Hints s, Hint h)=>(s & h))(node,h2);
	}
	void assertHint(Node node, Hints h)
	{
		assertPredHint!("Node should have hints",(Hints s, Hint h)=>!(s & h))(node,h);
	}
	auto n0 = getNodes(`function f(){if (a) return d; if (c) return 7;}`);
	assertHint(n0.getChild("ES6.FunctionBody"),Hints(Hint.DeducedReturingBranch)).shouldThrow;
	assertNoHint(n0.getChild("ES6.ReturnStatement"),Hints(Hint.Return | Hint.ReturningBranch)).shouldThrow;


	auto n1 = getNodes(`function f(){if (a) return d; if (c) return 7;}`);
	assertNoHint(n1.getChild("ES6.FunctionBody"),Hints(Hint.DeducedReturingBranch));
	assertHint(n1.getChild("ES6.ReturnStatement"),Hints(Hint.Return | Hint.ReturningBranch));

	auto n2 = getNodes(`function f(){if (a) return d; else return 7;}`);
	assertHint(n2.getChild("ES6.StatementListReturn"),Hints(Hint.Return | Hint.ReturningBranch | Hint.DeducedReturingBranch));
	assertHint(n2.getChild("ES6.IfStatementReturn"),Hints(Hint.Return | Hint.ReturningBranch | Hint.DeducedReturingBranch));
	assertHint(n2.getChild("ES6.ReturnStatement"),Hints(Hint.Return | Hint.ReturningBranch));

	auto n3 = getNodes(`function f(){if (a) { if (c) return d; else return 5; } }`);
	assertNoHint(n1.getChild("ES6.StatementListReturn"),Hints(Hint.DeducedReturingBranch));
	
	auto n4 = getNodes(`for (var a in b) { if (!a) { continue; } }`);
	assertHint(n4.getChild("ES6.ContinueStatement"),Hints(Hint.NonValueGeneratingStatement));
	assertHint(n4.getChild("ES6.IfStatement"),Hints(Hint.NonValueGeneratingStatement));

	auto n5 = getNodes(`if (g) for (var a in b) g = 5;`);
	assertHint(n5.getChild("ES6.IfStatement"),Hints(Hint.NonValueGeneratingStatement));

	auto n6 = getNodes(`if (g) for (var a in b) g = 5; else dd = 5;`);
	assertHint(n6.getChild("ES6.IfStatement"),Hints(Hint.NonValueGeneratingStatement));

	auto n7 = getNodes(`for (var i = 0; i < e.length; i++) { if (!e[i]) { g = 6; break; } }`);
	assertHint(n7.getChild("ES6.IfStatement"),Hints(Hint.NonValueGeneratingStatement));
	assertHint(n7.getChild("ES6.IterationStatement"),Hints(Hint.NonValueGeneratingStatement));
}
@("assertUnresolvedIdentifiers")
unittest
{
	import unit_threaded;
	import es5.testhelpers;
	void assertUnresolvedIdentifiers(string js, string[] expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : writeln;
		auto p = ES6(js);
		auto nodes = createNode(p);
		auto a = analyseNodes(nodes);
		auto got = a.unresolvedIdentifiers;
		if (got == expected)
			return;
		Message m;
		m.writeln(p);
		m.writeln("Error in assertUnresolvedIdentifiers:");
		m.writeln("Expected:");
		m.writeln(expected);
		m.writeln("Got:");
		m.writeln(got);
		failed(m,file,line);
	}
	assertUnresolvedIdentifiers(
		"function hup() { var a; };",
		["a"]
	).shouldThrow;
	assertUnresolvedIdentifiers(
		"function hup() { var foo = 5; return foo.bla; }; var bla = hup()*p;",
		["p"]
	);
	assertUnresolvedIdentifiers(
		"function hup() { var foo = 5; k = function() { } return foo.bla*t; };",
		["k","t"]
	);
	assertUnresolvedIdentifiers(
		"var bla = (ops)=>{ return (bb,ol)=>{ bb.bla = ops*tt*pp*ol}; }",
		["tt","pp","pp"] // TODO: don't know why pp shows up twice
	);
}
void reanalyse(Node node)
{
	import std.algorithm : map, reduce;
	Hints childHints;
	if (node.children.length > 0)
		childHints = node.children.map!(n=>n.hint & ~n.getHintMask).reduce!((a,b)=>a | b);
	node.processHints(childHints);
	if (node.parent)
		reanalyse(node.parent);
}
@("reanalyse")
unittest {
	auto n = new Node(ParseTree("ES6.VariableStatement",true),[
		new Node(ParseTree("ES6.BreakStatement",true))
	]);

	n.reanalyse();
	n.hint.shouldEqual(Hints(Hint.VariableDecl));
	n.children[0].reanalyse();
	n.hint.shouldEqual(Hints(Hint.VariableDecl | Hint.NonValueGeneratingStatement | Hint.Break));
	n.children[0].hint.shouldEqual(Hints(Hint.Break | Hint.NonValueGeneratingStatement));
}
@("analyse")
unittest
{

}