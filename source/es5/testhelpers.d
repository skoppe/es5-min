module es5.testhelpers;

void unitAssert(in string str)
{
	version (unittest)
	{
		throw new Exception(str);
	} else
	{
		assert(false,str);
	}
}
void unitAssert(bool cond, in string str = "Assertion")
{
	version (unittest)
	{
		if (cond)
			return;
		throw new Exception(str);
	} else
	{
		assert(cond,str);
	}
}
version (unittest)
{
	import es5.node;
	import es6.grammar;
	import es5.analyse;
	import es5.eval;
	import unit_threaded;
	void failed(in string msg, in string file = __FILE__, in size_t line = __LINE__)
	{
		throw new UnitTestException([msg], file, line);
	}
	import std.array : Appender;
	struct Message
	{
		private Appender!string app;
		void writeln(U)(U item)
		{
			app.put("\n");
			write(item);
			app.put("\n");
		}
		void write(U)(U item)
		{
			import std.conv : to;
			app.put(item.to!string);
		}
		auto writeEnum(T)(T type)
		{
			import std.traits : EnumMembers;
			import std.stdio : write;
			import std.conv : to;
			foreach(M; EnumMembers!T)
			{
				if (type == M)
				{
					this.write(M.to!string);
					return;
				}
			}
		}
		string get()
		{
			return app.data();
		}
		alias get this;
	}
	void failAndDumpEntries(string msg, Node entryA, Node entryB, in string file, in size_t line)
	{
		Message m;
		m.writeln(msg);
		m.write("A entry: ");
		m.writeln(entryA);
		m.write("B entry: ");
		m.writeln(entryB);
		failed(m,file,line);
	}
	void assertBranchesEqual(Branch a, Branch b, in string file = __FILE__, in size_t line = __LINE__)
	{
		if (a.parent is null && b.parent !is null)
		{
			failAndDumpEntries("Branch Error: branch a has parent, branch b doesn't",a.entry,b.entry,file,line);
		}
		if (a.parent !is null && b.parent is null)
		{
			failAndDumpEntries("Branch Error: branch b has parent, branch a doesn't",a.entry,b.entry,file,line);
		}
		if (a.entry.name != b.entry.name)
		{
			if (a.parent !is null)
				failAndDumpEntries("Branch Error: branch start at "~a.entry.name~", branch b at "~b.entry.name,a.parent.entry,b.parent.entry,file,line);
			else
				failAndDumpEntries("Branch Error: branch start at "~a.entry.name~", branch b at "~b.entry.name,a.entry,b.entry,file,line);
		}
		if (a.children.length != b.children.length)
		{
			import std.conv : to;
			failAndDumpEntries("Branch Error: branch a has "~a.children.length.to!string~" children, branch b only "~b.children.length.to!string,a.entry,b.entry,file,line);
		}
		import std.range : zip;
		foreach(bb; zip(a.children,b.children))
			assertBranchesEqual(bb[0],bb[1]);
	}
	@Name("Fail assertBrancesEqual due to faulty parent pointers")
	unittest
	{
		auto n = new Node(ParseTree("ES6.Literal",true));
		auto a = new Branch(null,n);
		auto b = new Branch(a,n);
		assertBranchesEqual(a,b).shouldThrow();
		assertBranchesEqual(b,a).shouldThrow();
		a.parent = a;
		a.entry = new Node(ParseTree("ES6.StringLiteral",true));
		assertBranchesEqual(a,b).shouldThrow();
		a.parent = null;
		b.parent = null;
		assertBranchesEqual(a,b).shouldThrow();
		a.entry = n;
		a.children ~= b;
		assertBranchesEqual(a,b).shouldThrow();
	}
	void assertTransformations(fun...)(string input, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : writeln;
		import es5.tojs;
		import es5.optimize;
		import es6.grammar;
		auto p = ES6(input);
		auto nodes = createNode(p);
		moveVariableAndFunctionDeclarationsUpFront(nodes);
		bool halt = false;
		auto a = analyseNodes(nodes);
		runOptimizations!fun(nodes,file,line);
		string got = nodes.toJS();

		if (got != expected)
		{
			Message m;
			m.writeln(nodes);
			m.writeln("Transformation Failure:\nexpected:\n`"~expected~"`\nbut got:\n`"~got~"`");
			failed(m,file,line);
		}
		auto tree = createNode(ES6(expected));
		assertBranchesEqual(a.trunk,analyseNodes(tree).trunk,file,line);
		// Note: we can also compare the nodes tree with the tree we get when we ES6 + createNode of expected
		// this way we can make sure that parents, branches, scopes and nodes are equal. 
		// and more importantly we know exactly when they diverge
	}
	Node getNodes(string input)
	{
		import std.stdio : writeln;
		import es6.grammar;
		auto p = ES6(input);
		if (!p.successful)
			throw new Exception("Invalid input");
		auto nodes = createNode(p);
		auto a = analyseNodes(nodes);
		return nodes;
	}
	@("Test getNodes")
	unittest
	{
		getNodes("var a = 7").shouldNotThrow();
		getNodes("var a =").shouldThrow();
		getNodes("var a = () => a * 7").shouldNotThrow();
	}
	void assertExpressionIs(string input, Ternary expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : write, writeln;
		import es6.grammar;
		import es5.tojs;
		import es5.eval;
		auto p = ES6(input);
		auto nodes = createNode(p);
		auto expr = nodes.getChild("ES6.ExpressionStatement").children[0];
		auto raw = expr.getRawValue!"ES6.Expression";
		if (raw.type == ValueType.NotKnownAtCompileTime)
		{
			if (expected == Ternary.None)
				return;
			Message msg;
			msg.writeln(nodes);
			msg.write("Expected Expression `"~expr.toJS~"` to ");
			if (expected == Ternary.True)
				msg.write("reduce to true, ");
			else if (expected == Ternary.False)
				msg.write("reduce to false, ");
			msg.writeln("but could not be reduced statically");
			failed(msg,file,line);
		}
		auto got = raw.coerceAsBoolean ? Ternary.True : Ternary.False;
		if (got == expected)
			return;
		Message msg;
		msg.writeln(nodes);
		msg.write("Expected Expression `"~expr.toJS~"` to ");
		if (expected == Ternary.True)
			msg.write("reduce to true, ");
		else if (expected == Ternary.False)
			msg.write("reduce to false, ");
		else
			msg.write("not be statically reducible, ");
		msg.write("but ");
		if (got == Ternary.True)
			msg.writeln("reduces to true");
		else if (got == Ternary.False)
			msg.writeln("reduces to false");
		failed(msg,file,line);
	}
	@("Test if assertExpressionIs fails")
	unittest
	{
		assertExpressionIs(`abcd.bla('df') > -1`,Ternary.True).shouldThrow();
		assertExpressionIs(`abcd.bla('df') > -1`,Ternary.False).shouldThrow();
		assertExpressionIs(`abcd.bla('df') > -1`,Ternary.None).shouldNotThrow();
		assertExpressionIs(`"abcd" == "abcde"`,Ternary.None).shouldThrow();
		assertExpressionIs(`"abcd" == "abcde"`,Ternary.True).shouldThrow();
		assertExpressionIs(`"abcd" != "abcde"`,Ternary.False).shouldThrow();
	}
	void assertEvalUnaryExpression(string input, ValueType type, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : write, writeln;
		import es6.grammar;
		import es5.tojs;
		import std.conv : to;
		auto p = ES6(input);
		auto nodes = createNode(p);
		auto expr = nodes.getChild("ES6.ExpressionStatement").children[0];
		auto unExpr = expr.getNthChild(4);
		assert(unExpr.isA!"ES6.UnaryExpression");
		auto postExpr = unExpr.children[$-1];
		auto litExpr = postExpr.getNthChild(5);
		RawValue raw;
		if (litExpr.isA!"ES6.Literal" || litExpr.isA!"ES6.IdentifierReference")
		{
			auto node = litExpr.children[0];
			raw = RawValue(node.matches[0],node.getLiteralNodeValueType);
		} else if (litExpr.isA!"ES6.ObjectLiteral")
		{
			raw = RawValue(litExpr.matches[0],ValueType.Object);
		}
		raw = raw.processPrefixExpressions(unExpr.children[0..$-1]);
		if (raw.type != type)
		{
			Message m;
			m.write("Expected Expression `"~expr.toJS~"` to resolve to type ");
			m.writeEnum!ValueType(type);
			m.write(", but has type ");
			m.writeEnum!ValueType(raw.type);
			m.writeln(".");
			failed(m,file,line);
		}
		if (raw.value == expected)
			return;
		Message m;
		m.writeln("Expected Expression `"~expr.toJS~"` to resolve to value `"~expected~"` but resolved to `"~raw.value~"`.");
		failed(m,file,line);
	}
	@("Test if assertEvalUnaryExpression fails")
	unittest
	{
		assertEvalUnaryExpression(`66`,ValueType.NotKnownAtCompileTime,"").shouldThrow();
		assertEvalUnaryExpression(`66`,ValueType.Numeric,"6").shouldThrow();
	}
	void assertEvalBinaryExpression(string input, ValueType type, string expected = "", in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : write, writeln;
		import es6.grammar;
		import es5.tojs;
		import es5.optimize;
		auto p = ES6(input);
		auto nodes = createNode(p);
		nodes.convertToBase10Decimals();
		auto expr = nodes.getChild("ES6.BinaryExpression");
		auto raw = expr.getRawValue!"ES6.BinaryExpression";
		if (raw.type != type)
		{
			Message m;
			m.writeln(nodes);
			m.write("Expected Expression `"~expr.toJS~"` to resolve to type ");
			m.writeEnum!ValueType(type);
			m.write(", but has type ");
			m.writeEnum!ValueType(raw.type);
			m.writeln(".");
			failed(m,file,line);
		}
		if (raw.type == ValueType.NotKnownAtCompileTime)
			return;
		if (raw.value == expected)
			return;
		Message m;
		m.writeln(nodes);
		m.writeln("Expected Expression `"~expr.toJS~"` to resolve to value `"~expected~"` but resolved to `"~raw.value~"`.");
		failed(m,file,line);
	}
	@("Test if assertEvalBinaryExpression fails")
	unittest
	{
		assertEvalBinaryExpression(`66 * 6`,ValueType.NotKnownAtCompileTime,"").shouldThrow();
		assertEvalBinaryExpression(`66 * 6`,ValueType.Numeric,"6").shouldThrow();
	}
}