module es5.node;

import std.typecons : Nullable;

import es6.grammar;
import es5.utils;
import es5.flags : BitFlags; //NOTE: can be replaced with `std.typecons : BitFlags;` when gdc/ldc supports it
import es5.testhelpers;

version (unittest)
{
	import unit_threaded;
	import es6.grammar;
	import es5.analyse;
	import es5.tojs;
	auto shouldIs(Obj)(Obj a, Obj b)
		if (is(Obj == class))
	{
		return a is b;
	}
	auto shouldNotIs(Obj)(Obj a, Obj b)
		if (is(Obj == class))
	{
		return a !is b;
	}
}

enum IdentifierType
{
	Parameter,
	Variable,
	Function
}
struct Variable
{
	string name;
	string shortName;
	IdentifierType type;
	Node node;
	this(string s, IdentifierType t, Node n)
	{
		name = s;
		type = t;
		node = n;
	}
}
class Scope
{
	Scope parent;
	private Variable[] variables;
	Scope[] scopes;
	private string[] identifiers;
	private int nextFreeIdentifier = -1;
	private string[] globals;
	Node entry;
	this(Scope p = null)
	{
		parent = p;
	}
	Node getRoot()
	{
		if (parent)
			return parent.getRoot();
		if (entry)
			return entry.getRoot();
		return null;
	}
	const(Variable[]) getVariables()
	{
		return variables;
	}
}
auto getVariablesByKey(Scope s)
{
	return s.variables.map!"a.name";
}
@("getVariablesByKey")
unittest
{
	(new Scope()).getVariablesByKey();
}
void propagateGlobals(Scope scp)
{
	import std.algorithm : canFind, each, filter;
	foreach(s; scp.scopes)
	{
		s.propagateGlobals();
		s.globals
			.filter!(id=>!scp.variables.canFind!"a.name == b"(id))
			.each!(id=>scp.globals ~= id);
	}
	foreach(g; scp.identifiers.filter!(a=>!scp.variables.canFind!"a.name == b"(a)))
		scp.globals ~= g;
}
private void propagateGlobal(Scope s, string id)
{
	import std.algorithm : canFind;
	if (s.variables.canFind!"a.name == b"(id))
		return;
	s.globals ~= id;
	if (s.parent)
		s.parent.propagateGlobal(id);
}
void addGlobal(Scope s, string id)
{
	import std.algorithm : canFind;
	if (!s.identifiers.canFind(id))
	{
		s.identifiers ~= id;
		propagateGlobal(s,id);
	}
}
@("addGlobal")
unittest
{
	Scope getScopes(string js)
	{	
		auto p = ES6(js);
		auto nodes = createNode(p);
		nodes.moveVariableAndFunctionDeclarationsUpFront;
		auto a = analyseNodes(nodes);
		return a.scp;
	}
	void assertScopeGlobals(Scope s, string[] globals, in string file = __FILE__, in size_t line = __LINE__)
	{
		s.globals.shouldEqual(globals,file,line);
	}
	auto ss = getScopes(`function fun(a) { function bar() { var e; return a*t; } var n; return e*bar(); }`);
	ss.scopes[0].addGlobal("o");
	assertScopeGlobals(ss,["t","e","o"]);
	assertScopeGlobals(ss.scopes[0],["t","e","o"]);
	assertScopeGlobals(ss.scopes[0].scopes[0],["a","t"]);
 	ss = getScopes(`function fun(a) { function bar() { var e; return a*t; } var n; return e*bar(); }`);
	ss.scopes[0].addVariable(Variable("b",IdentifierType.Variable,null));
	ss.scopes[0].addGlobal("a");
	assertScopeGlobals(ss,["t","e"]);
	assertScopeGlobals(ss.scopes[0],["t","e"]);
	ss.getFreeIdentifier().shouldEqual("a");
	ss.scopes[0].getFreeIdentifier().shouldEqual("o");

}
void addIdentifier(Scope s, string id)
{
	import std.algorithm : canFind;
	if (!s.identifiers.canFind(id))
		s.identifiers ~= id;
}
void addVariable(Scope s, Variable var)
{
	s.variables ~= var;
}
bool hasVariable(Scope s, string id)
{
	import std.algorithm : canFind;
	return s.variables.canFind!"a.name == b"(id);
}
void adjustGlobals(Scope s, string delegate(string) adjust)
{
	import std.algorithm : map, canFind;
	import std.array : array;
	s.globals = s.globals.map!(a => adjust(a)).array;
	s.identifiers = s.identifiers.map!(a => s.variables.canFind!"a.name == b"(a) ? a : adjust(a)).array;
}
// WATCH out, need to call adjustGlobals for each nested scope afterwards to rename the globals and identifiers
// we cannot call it in renameVariable since you might call it twice with a => b and b => c, in which case
// both a and b variables will be renamed to c, when instead you wanted a renamed to b and b renamed to c
void renameVariable(Scope s, string var, string name)
{
	foreach (idx, v; s.variables)
	{
		if (v.name == var)
		{
			s.variables[idx].name = name;
			//foreach (c; s.scopes)
				//adjustGlobalsAndIdentifiers(c, var, name);
			return;
		}
	}
}
bool hasIdentifier(Scope s, string id)
{
	import std.algorithm : canFind;
	return s.identifiers.canFind(id);
}
@("Scope globals")
unittest
{
	Scope getScopes(string js)
	{	
		auto p = ES6(js);
		auto nodes = createNode(p);
		nodes.moveVariableAndFunctionDeclarationsUpFront;
		auto a = analyseNodes(nodes);
		return a.scp;
	}
	void assertScopeGlobals(Scope s, string[] globals, in string file = __FILE__, in size_t line = __LINE__)
	{
		s.globals.shouldEqual(globals,file,line);
	}
	assertScopeGlobals(getScopes(`function fun(b) { function bar() { return b*c; } return a*bar(); }`),["c","a"]);
	assertScopeGlobals(getScopes(`function fun(b) { function bar() { return b*c; } return a*bar(); }`).scopes[0],["c","a"]);
	assertScopeGlobals(getScopes(`function fun(b) { function bar() { return b*c; } return a*bar(); }`).scopes[0].scopes[0],["b","c"]);
	assertScopeGlobals(getScopes(`var a; function fun(b) { function bar() { return b*c; } return a*bar(); }`),["c"]);
	assertScopeGlobals(getScopes(`var a; function fun(b) { function bar() { return b*c; } return a*bar(); }`).scopes[0],["c","a"]);
	assertScopeGlobals(getScopes(`var a; function fun(b) { function bar() { return b*c; } return a*bar(); }`).scopes[0].scopes[0],["b","c"]);
	assertScopeGlobals(getScopes(`function fun() { if (!a) var a = 6; return a*b; } var b = 6;`),[]);
	assertScopeGlobals(getScopes(`function fun() { if (!a) var a = 6; return a*b; } var b = 6;`).scopes[0],["b"]);	// here we test whether usage of a before declaration doesn't trigger adding it to the globals list
}
string getFreeIdentifier(Scope root, bool ignoreLocalVariables = false)
{
	import std.algorithm : canFind;
	int nameIdx = 0;
	if (root.nextFreeIdentifier != -1)
		nameIdx = root.nextFreeIdentifier;
	string name;
	auto usedIdentifiers = root.globals;
	auto variables = root.variables;
	do
	{
		name = generateName(nameIdx++);
	} while(!name.isValidIdentifier || usedIdentifiers.canFind(name) || (!ignoreLocalVariables && variables.canFind!"a.name == b"(name)));
	if (ignoreLocalVariables)
		root.nextFreeIdentifier = nameIdx;
	return name;
}
@("getFreeIdentifier")
unittest
{
	Scope getScopes(string js)
	{	
		auto p = ES6(js);
		auto nodes = createNode(p);
		nodes.moveVariableAndFunctionDeclarationsUpFront;
		auto a = analyseNodes(nodes);
		return a.scp;
	}
	void assertNextFreeIdentifier(Scope s, string expected, bool ignoreLocalVariables = false, in string file = __FILE__, in size_t line = __LINE__)
	{
		import es5.tojs;
		auto got = s.getFreeIdentifier(ignoreLocalVariables);
		if (expected == got)
			return;
		Message m;
		m.write("Expected free identifier ");
		m.write(expected);
		m.write(", got identifier ");
		m.write(got);
		m.write(", in ");
		m.writeln(s.getVariables);
		m.writeln(s.entry.toJS());
		failed(m,file,line);
	}
	assertNextFreeIdentifier(getScopes(`var e,a,t`),"z").shouldThrow;
	assertNextFreeIdentifier(getScopes(`var e,a,t`),"n");
	assertNextFreeIdentifier(getScopes(`var e,a,t`),"e",true);
	assertNextFreeIdentifier(getScopes(`var e,a,t; function n() { }`),"o");
	assertNextFreeIdentifier(getScopes(`var e,a,t; function n() { }`),"e",true);
	assertNextFreeIdentifier(getScopes(`var e,a,t; function n() { var e,a; }`).scopes[0],"t");
	assertNextFreeIdentifier(getScopes(`var x,y,z; function w() { var q,p; }`),"e");
	assertNextFreeIdentifier(getScopes(`var x,y,z; function w() { var q,p; }`).scopes[0],"e");
	assertNextFreeIdentifier(getScopes(`var x,y,z; function w(e) { var q,p; }`).scopes[0],"a");
	assertNextFreeIdentifier(getScopes(`var x,y,z; function w(q) { return e*q*y; }`).scopes[0],"a");
	assertNextFreeIdentifier(getScopes(`var x,y,z; function w(q) { return e*q*y; }`).scopes[0],"a",true);
}
size_t getScopeDepth(Scope s, size_t d = 0)
{
	if (s.parent is null)
		return d;
	return getScopeDepth(s.parent,d+1);
}
unittest
{
	Scope s = new Scope();
	s.getRoot().shouldBeNull();
	Scope s2 = new Scope();
	s.parent = s2;
	s.getRoot().shouldBeNull();
	s2.entry = new Node(ParseTree("Bullocks"));
	s.getRoot().name.shouldEqual("Bullocks");
	s2.getScopeDepth.shouldEqual(0);
	s.getScopeDepth.shouldEqual(1);
}
Scope findCommonParent(Scope a, Scope b)
{
	if (a is b)
		return a;
	auto aDepth = getScopeDepth(a);
	auto bDepth = getScopeDepth(b);
	if (aDepth > bDepth)
		a = a.getNthParent(aDepth - bDepth);
	if (bDepth > aDepth)
		b = b.getNthParent(bDepth - aDepth);
	if (a is b)
		return a;
	auto parentA = a.parent;
	auto parentB = b.parent;
	while (parentA !is parentB)
	{
		assert(parentA !is null);
		assert(parentB !is null);
		parentA = parentA.parent;
		parentB = parentB.parent;
	}
	return parentA;
}
unittest
{
	auto a = new Scope();
	findCommonParent(a,a).shouldIs(a);
	auto b = new Scope();
	auto root = new Scope();
	a.parent = root;
	b.parent = root;
	findCommonParent(a,b).shouldIs(root);
	findCommonParent(root,b).shouldIs(root);
	findCommonParent(a,root).shouldIs(root);
	auto a2 = new Scope(a);
	auto b2 = new Scope(b);
	findCommonParent(a2,b2).shouldIs(root);
}
auto getLastParameter(Scope scp)
{
	import std.typecons : Nullable;
	import std.range : front, empty;
	import std.algorithm : find;
	Nullable!Variable v;
	if (scp.variables.length == 0)
		return v;
	auto formalParameterRange = scp.variables.find!((a)=>a.type == IdentifierType.Parameter);
	if (formalParameterRange.empty)
		return v;
	auto formalParameter = formalParameterRange.front().node;
	Node formalsList = formalParameter.parent;
	Node lastFormalParameter = formalsList.children[$-1];
	auto range = scp.variables.find!"a.node == b"(lastFormalParameter);
	assert(!range.empty());
	v = range.front();
	return v;
}
unittest
{
	import es6.grammar;
	import es5.analyse;
	auto getScope(string js)
	{
		auto nodes = createNode(ES6(js));
		auto ar = nodes.analyseNodes;
		return ar.scp;
	}
	auto getSubScope(string js)
	{
		return getScope(js).scopes[0];
	}
	getScope(`fun()`).getLastParameter().isNull().shouldBeTrue();
	getScope(`var a,b,c,d;`).getLastParameter().isNull().shouldBeTrue();
	getSubScope(`function fun() { var a,b,c,d; }`).getLastParameter().isNull().shouldBeTrue();
	auto abc = getSubScope(`function fun(abc) { var a,b,c,d; }`).getLastParameter();
	abc.isNull().shouldBeFalse();
	abc.name.shouldEqual("abc");
}
void removeLastParameter(Scope scp)
{
	import std.algorithm : find, remove, countUntil;
	import std.range : front, empty;
	if (scp.variables.length == 0)
		return;
	auto formalParameterRange = scp.variables.find!((a)=>a.type == IdentifierType.Parameter);
	if (formalParameterRange.empty)
		return;
	auto formalParameter = formalParameterRange.front().node;
	Node formalsList = formalParameter.parent;
	Node lastFormalParameter = formalsList.children[$-1];
	auto idx = scp.variables.countUntil!"a.node == b"(lastFormalParameter);
	assert(idx != -1);
	scp.variables.remove(idx);
	scp.variables = scp.variables[0..$-1];
	formalsList.children = formalsList.children[0..$-1];
	if (formalsList.children.length == 0)
	{
		if (formalsList.parent.children.length == 1)
		{
			auto formalParameters = formalsList.getNthParent(2);
			formalParameters.removeFromParent();
		}
	}
}
unittest
{
	auto getScope(string js)
	{
		auto nodes = createNode(ES6(js));
		auto ar = nodes.analyseNodes;
		return ar.scp;
	}
	auto getSubScope(string js)
	{
		return getScope(js).scopes[0];
	}
	auto assertRemoveLastParam(Scope scp, string expected)
	{
		scp.removeLastParameter();
		scp.getRoot().toJS().shouldEqual(expected);
	}
	assertRemoveLastParam(getScope(`fun()`),`fun()`);
	assertRemoveLastParam(getScope(`var a, b, c, d;`),`var a, b, c, d;`);
	assertRemoveLastParam(getSubScope(`function fun() { var a, b, c, d }`),`function fun() { var a, b, c, d }`);
	assertRemoveLastParam(getSubScope(`function fun(abc) { var a, b, c, d }`),`function fun() { var a, b, c, d }`);
	assertRemoveLastParam(getSubScope(`function fun(abc,def) { var a, b, c, d }`),`function fun(abc) { var a, b, c, d }`);
}
enum Hint
{
	None = 0x01,
	EmptyReturn = 0x02,
	Return = 0x04,
	Literal = 0x08,
	ReturningBranch = 0x10,
	Break = 0x20,
	DeducedReturingBranch = 0x40,
	NonValueGeneratingStatement = 0x80,
	Assignment = 0x100,
	VariableDecl = 0x200,
	FunctionDecl = 0x400,
	LogicalOr = 0x800,
	EmitReturn = 0x1000
}
alias Hints = BitFlags!Hint;
alias HintsMask = BitFlags!Hint;
bool isA(alias name)(ref ParseTree p)
{
	return p.name.startsWith(name);
}
Node[] createNodes(ParseTree)(ParseTree[] p, Node parent)
{
	import std.algorithm : map;
	import std.range : array;
	return p.map!(
		(c){
			auto n = new Node(c,[],parent);
			n.children = createNodes(c.children,n);
			return n;
		}).array;
}
auto createNode(ParseTree p)
{
	auto n = new Node(p);
	n.children = createNodes(p.children,n);
	return n;
}
class Branch
{
	Branch parent; 		/// The parent branch (can be null)
	Branch[] children;	/// Any nested branches
	Node entry;			/// The node that starts this branch
	this(Branch p = null, Node e = null, Node c = null)
	{
		parent = p;
		entry = e;
		/*if (parent !is null)
			parent.children ~= this;*/
	}
	Branch branch(Node entry)
	{
		auto b = new Branch(this,entry);
		this.children ~= b;
		return b;
	}
}
class Node
{
	string name; /// The node name
	bool successful; /// Indicates whether a parsing was successful or not
	string[] matches; /// The matched input's parts. Some expressions match at more than one place, hence matches is an array.
	Node[] children; /// The sub-trees created by sub-rules parsing.
	string input;
	Scope scp;
	Hints hint;
	Branch branch;
	Node parent;
	this(ParseTree p, Node[] cs = [], Node par = null)
	{
		name = p.name;
		successful = p.successful;
		matches = p.matches;
		if (p.input)
			input = p.input[p.begin .. p.end];
		children = cs;
		parent = par;
		foreach(c; cs)
			c.parent = this;
	}
	//alias toString = Object.toString;
	override string toString() const
	{
		return prettyPrint();
	}
	Node getRoot()
	{
		if (scp !is null && scp.entry !is this)
			return scp.getRoot();
		if (parent !is null)
			return parent.getRoot();
		return this;
	}
	void setChildren(Node[] cs)
	{
		foreach(c; this.children)
			c.parent = null;
		this.children = cs;
		foreach(c; this.children)
		{
			c.scp = scp;
			c.parent = this;
		}
	}
	void setChild(size_t idx, Node node)
	{
		assert(children.length > idx);
		children[idx] = node;
		node.parent = this;
		node.scp = scp;
	}
	void setChildReverse(size_t idx, Node node)
	{
		assert(children.length > idx);
		children[children.length - idx - 1] = node;
		node.parent = this;
		node.scp = scp;
	}
	void addChild(Node c)
	{
		this.children ~= c;
		c.parent = this;
		c.scp = scp;
	}
	void addChildren(Node[] cs)
	{
		this.children ~= cs;
		foreach(c; cs)
		{
			c.scp = scp;
			c.parent = this;
		}
	}
	void remove(size_t idx)
	{
		import std.algorithm : remove;
		children.remove(idx);
		children = children[0..$-1];
	}
    string prettyPrint(string tabs = "", int maxLevel = -1) const
    {
    	return prettyPrintHighlight!(a=>a.name)(this,tabs,maxLevel);
    }
}
string prettyPrintHighlight(alias Highlighter)(const Node node, string tabs = "",int maxLevel = -1)
{
	if (maxLevel == 0)
		return "";
	string r = tabs~Highlighter(node)~" [";
	import std.traits : EnumMembers;
	import std.conv : to;
	string[] hints;
	foreach(M; EnumMembers!Hint)
	{
		if (node.hint & M)
			hints ~= M.to!string;
	}
	import std.conv : text;
	import std.algorithm : joiner;
	r ~= hints.joiner(", ").text();
	r ~= "]";
	r ~= " `"~node.input~"`\n";
	foreach(c; node.children)
		r ~= c.prettyPrintHighlight!Highlighter(tabs~" ",maxLevel-1);
	return r;
}
unittest{
	(new Node(ParseTree("Bla",true))).prettyPrintHighlight!(a=>a.name)("",0).shouldEqual("");
}
Node getNthParent(Node p, size_t parent)
{
	if (parent == 0)
		return p;
	if (p.parent is null)
		return null;
	return getNthParent(p.parent,parent-1);
}
unittest
{
	getNthParent(new Node(ParseTree("Bullocks",true)),1).shouldBeNull();
}
Scope getNthParent(Scope p, size_t parent)
{
	if (parent == 0)
		return p;
	if (p.parent is null)
		return null;
	return getNthParent(p.parent,parent-1);
}
unittest
{
	(new Scope()).getNthParent(4).shouldBeNull();
}
Node getNthChild(Node p, size_t child)
{
	if (p.children.length == 0)
		return null;
	if (child == 1)
		return p.children[0];
	return getNthChild(p.children[0],child-1);
}
bool isNthChild(alias Nth)(Node node, string name)
{
	static if (Nth == 0)
		return node.name.startsWith(name);
	else
	{
		if (node.children.length == 0)
			return false;
		return node.children[0].isNthChild!(Nth-1)(name);
	}
}
Node getChild(Node node, string name)
{
	import std.algorithm : startsWith;
	foreach(c; node.children)
	{
		if (c.name.startsWith(name))
			return c;
	}
	foreach(c; node.children)
	{
		auto t = c.getChild(name);
		if (t !is null)
			return t;
	}
	return null;
}
bool isA(alias name)(const Node n)
{
	return n !is null && n.name.startsWith(name);
}
void bringToFront(Node parent, Node child)
{
	auto idx = findIndex(parent,child);
	assert(idx != -1);
	parent.children.moveElement(idx,0);
}
bool isLastChild(Node parent, Node child)
{
	auto idx = findIndex(parent,child);
	assert(idx != -1);
	return parent.children.length == idx+1;
}
Node createVariableDeclarationNode(string identifier, Node initializer = null)
{
	auto node = new Node(ParseTree("ES6.VariableDeclarationIn",true),
					[
						new Node(ParseTree("ES6.BindingIdentifier",true),
						[
							new Node(ParseTree("ES6.Identifier",true,[identifier]),
							[
								new Node(ParseTree("ES6.IdentifierName",true,[identifier]))
							])
						])
					]);
	if (initializer !is null)
		node.addChild(initializer);
	return node;
}
@("createVariableDeclarationNode")
unittest {
	auto v1 = createVariableDeclarationNode("abc");
	auto v2 = ES6(`var abc;`).createNode().getChild("ES6.VariableDeclarationIn");
	sameTree(v1,v2).shouldBeTrue();
}
@("createVariableDeclarationNode with Initializer")
unittest {
	auto v1 = createVariableDeclarationNode("abc",createStringLiteralInitializer("abc"));
	auto v2 = ES6(`var abc = "abc";`).createNode().getChild("ES6.VariableDeclarationIn");
	sameTree(v1,v2).shouldBeTrue();
}
Node createVariableDeclarationListStatement(Node[] declarations = [])
{
	return new Node(ParseTree("ES6.StatementListItem",true),
					[
						new Node(ParseTree("ES6.Statement",true),
						[
							new Node(ParseTree("ES6.VariableStatement",true),
							[
								new Node(ParseTree("ES6.VariableDeclarationListIn",true),declarations)
							])
						])
					]);	
}
Node createUnaryExpressionPrimaryExpression(Node child)
{
	return new Node(ParseTree("ES6.UnaryExpression",true),[
	     new Node(ParseTree("ES6.PostfixExpression",true),[
	      new Node(ParseTree("ES6.LeftHandSideExpression",true),[
	       new Node(ParseTree("ES6.NewExpression",true),[
	        new Node(ParseTree("ES6.MemberExpression",true),[
	         new Node(ParseTree("ES6.PrimaryExpression",true),[child])
	])])])])]);
}
/*Node createUnaryExpressionIdentifier(string name)
{
	return createUnaryExpressionPrimaryExpression(createIdentifierReference(name));
}*/
Node createUnaryExpressionLiteral(Node child)
{
	return createUnaryExpressionPrimaryExpression(new Node(ParseTree("ES6.Literal",true),[child]));
}
Node createUnaryExpressionBoolLiteral(bool value)
{
	import std.conv : to;
	return createUnaryExpressionLiteral(new Node(ParseTree("ES6.BooleanLiteral",true,[value.to!string])));
}
@("createUnaryExpressionBoolLiteral")
unittest {
	auto v1 = createUnaryExpressionBoolLiteral(true);
	auto v2 = ES6(`true`).createNode().getChild("ES6.UnaryExpression");
	sameTree(v1,v2).shouldBeTrue();
}
Node createUnaryExpressionNullLiteral()
{
	return createUnaryExpressionLiteral(new Node(ParseTree("ES6.NullLiteral",true,["null"])));
}
@("createUnaryExpressionNullLiteral")
unittest {
	auto v1 = createUnaryExpressionNullLiteral();
	auto v2 = ES6(`null`).createNode().getChild("ES6.UnaryExpression");
	sameTree(v1,v2).shouldBeTrue();
}
Node createUnaryExpressionStringLiteral(string str)
{
	return createUnaryExpressionLiteral(new Node(ParseTree("ES6.StringLiteral",true,['"'~str~'"'])));
}
@("createUnaryExpressionStringLiteral")
unittest {
	auto v1 = createUnaryExpressionStringLiteral("abc");
	auto v2 = ES6(`"abc"`).createNode().getChild("ES6.UnaryExpression");
	sameTree(v1,v2).shouldBeTrue();
}
Node createUnaryExpressionDecimalLiteral(string str)
{
	return createUnaryExpressionLiteral(
		new Node(ParseTree("ES6.NumericLiteral",true,[str]),
			[
				new Node(ParseTree("ES6.DecimalLiteral",true,[str]))
			])
	);
}
@("createUnaryExpressionDecimalLiteral")
unittest {
	auto v1 = createUnaryExpressionDecimalLiteral("1234");
	auto v2 = ES6(`1234`).createNode().getChild("ES6.UnaryExpression");
	sameTree(v1,v2).shouldBeTrue();
}
/*Node createIdentifierInitializer(string id)
{
	return createInitializer(createUnaryExpressionIdentifier(id));
}*/
Node createInitializer(Node c)
{
	return new Node(ParseTree("ES6.InitializerIn",true),[
	 new Node(ParseTree("ES6.AssignmentExpressionIn",true),[
	  new Node(ParseTree("ES6.ConditionalExpressionIn",true),[
	   new Node(ParseTree("ES6.RightHandSideExpressionIn",true),[c])
	])])]);
}
Node createStringLiteralInitializer(string str)
{
	return createInitializer(createUnaryExpressionStringLiteral(str));
}
@("createStringLiteralInitializer")
unittest {
	auto v1 = createStringLiteralInitializer("abc");
	auto v2 = ES6(`var abc = "abc";`).createNode().getChild("ES6.InitializerIn");
	sameTree(v1,v2).shouldBeTrue();
}
Node createIdentifierReference(string name)
{
	auto match = [name];
	return new Node(ParseTree("ES6.IdentifierReference",true,match),[
	 new Node(ParseTree("ES6.Identifier",true,match),[
	  new Node(ParseTree("ES6.IdentifierName",true,match))
	 ])]);
}
@("createIdentifierReference")
unittest {
	auto v1 = createIdentifierReference("abc");
	auto v2 = ES6(`abc;`).createNode().getChild("ES6.IdentifierReference");
	sameTree(v1,v2).shouldBeTrue();
}
bool replaceChild(Node parent, Node node, Node sub, bool setBranchAndScope = true)
{
	assert(node !is null);
	assert(sub !is null);
	assert(parent !is null);
	foreach(idx, c; parent.children)
	{
		if (c is node)
		{
			parent.children[idx] = sub;
			sub.parent = parent;
			if (setBranchAndScope)
			{
				sub.branch = node.branch;
				sub.scp = node.scp;
				if (sub.branch && sub.branch.entry is node) sub.branch.entry = sub;
				if (sub.scp && sub.scp.entry is node) sub.scp.entry = sub;
			}
			return true;
		}
	}
	return false;
}
unittest
{
	Node n = new Node(ParseTree("ES6.NodeA",true));
	n.replaceChild(new Node(ParseTree("ES6.NonExistent",true)),new Node(ParseTree("ES6.NonExistent",true))).shouldBeFalse();
}
bool replaceNode(Node node, Node sub, bool setBranchAndScope = true)
{
	return replaceChild(node.parent,node,sub,setBranchAndScope);
}
unittest
{
	auto n = new Node(ParseTree("A.1",true),
		[
			new Node(ParseTree("A.12",true))
		]);
	assert(n.children[0].name == "A.12");
	n.children[0].replaceNode(new Node(ParseTree("B.12",true)));
	assert(n.children[0].name == "B.12");
}
/*void insertBranchBefore(Branch injectee, Branch head)
{
	auto oldParent = head.parent;
	size_t idx = findIndex(oldParent,head);
	head.parent = injectee;
	injectee.children ~= head;
	oldParent.children[idx] = injectee;
	injectee.parent = oldParent;
}*/
// TODO: we need to unittest this
void propagateBranch(Node node, Branch b)
{
	assert(b !is null,"Branch is null");
	assert(node !is null,"Node is null");
	assert(b.entry.scp !is null,"Scope is null");
	node.branch = b;
	node.scp = b.entry.scp;
	if (node.isA!"ES6.FunctionDeclaration")
	{
		node.children[0].propagateBranch(b);
		return;
	} else if (node.isA!"ES6.IfStatement")
	{
		node.children[0].propagateBranch(b);
		return;
	} else if (node.name == "ES6.CaseClause" || node.name == "ES6.CaseClauseReturn" || node.name == "ES6.CaseClauseYield" || node.name == "ES6.CaseClausesYieldReturn")
	{
		node.children[0].propagateBranch(b);
		return;
	} else if (node.isA!"ES6.ArrowFunction")
		return;
	else if (node.isA!"ES6.FunctionExpression")
		return;

	foreach(c; node.children)
		c.propagateBranch(b);
}
unittest
{
	auto root = ES6(`function b() { ; }; var a = function(){ ; }, b = ()=>{ ; }`).createNode();
	auto ar = analyseNodes(root);
	auto b = new Branch(null,root);
	root.propagateBranch(b);
	root.getChild("ES6.BindingIdentifier").branch.shouldIs(b);
	root.getChild("ES6.FunctionBody").branch.shouldNotIs(b);
	root.getChild("ES6.FunctionExpression").branch.shouldNotIs(b);
	root.getChild("ES6.ArrowFunction").branch.shouldNotIs(b);
}
auto generateVoid0Expression()
{
	auto un = createUnaryExpressionDecimalLiteral("0");
	un.insertAtFront(new Node(ParseTree("ES6.PrefixExpression",true,["void"])));
	return new Node(ParseTree("ES6.ExpressionIn",true),
	[
		createAssignmentExpressionFromUnaryExpression(un)
	]);
}
@("generateVoid0Expression")
unittest {
	auto v1 = generateVoid0Expression();
	auto v2 = ES6(`void 0`).createNode().getChild("ES6.ExpressionIn");
	sameTree(v1,v2).shouldBeTrue();
}
auto generateReturnVoid0StatementItem()
{
	return 
		new Node(ParseTree("ES6.StatementListItemReturn",true),
	    [
	    	new Node(ParseTree("ES6.StatementReturn",true),
	      	[
	      		new Node(ParseTree("ES6.ReturnStatement",true),
	       		[
	       			generateVoid0Expression()
				])
	      	])
		]);
}
@("generateReturnVoid0StatementItem")
unittest {
	auto v1 = generateReturnVoid0StatementItem();
	auto v2 = ES6(`function abc() { return void 0 }`).createNode().getChild("ES6.StatementListItemReturn");
	sameTree(v1,v2).shouldBeTrue();
}
enum Ternary
{
	True,
	False,
	None
}
bool someNode(alias pred, Node)(Node root)
{
	auto t = pred(root);
	if (t == Ternary.True)
		return true;
	else if (t == Ternary.None)
		return false;
	foreach(c; root.children)
		if (someNode!(pred,Node)(c))
			return true;
	return false;
}
void negateExpression(Node condition)
{
	size_t shift = 0;
	if (condition.isA!"ES6.AssignmentExpression")
		shift = 3;
	assert(shift != 0);

	if (condition.getNthChild(shift).isA!"ES6.BinaryExpression")
	{
		// we have to do parentheses
		auto t = condition.parent;
		condition.parent.replaceChild(condition,
			createAssignmentExpressionFromUnaryExpression
			(
				new Node(ParseTree("ES6.UnaryExpression",true),
				[
					new Node(ParseTree("ES6.PrefixExpression",true,["!"])),
					parenthesisAssignmentExpressionIntoPostfixExpression(condition)
				])
			)
		);
	} else if (condition.getNthChild(shift+1).isA!"ES6.PrefixExpression")
	{
		// we have to remove prefixexpression
		condition.getNthChild(shift).removeChild(0);
	}
	else
	{
		// we only insert the "!"
		auto old = condition.getNthChild(shift);
		auto p = old.parent;
		old.insertAtFront(new Node(ParseTree("ES6.PrefixExpression",true,["!"])));
	}
}
void negateIfCondition(Node ifStatement)
{
	assert(ifStatement.isA!"ES6.IfStatement");
	negateExpression(ifStatement.getChild("ES6.AssignmentExpression"));
}
Node getLastStatement(Node stmt)
{
	auto list = stmt.getNthChild(3);
	if (list.isA!"ES6.StatementList")
	{
		if (list.children.length == 0)
			return null;
		return list.children[$-1];
	}
	return stmt;
}
unittest{
	(new Node(ParseTree("Bla",true),[
			new Node(ParseTree("Bla",true),[
				new Node(ParseTree("Bla",true),[
					new Node(ParseTree("ES6.StatementList",true))
		])])])).getLastStatement.shouldBeNull;
}
Node convertStatementToExpression(Node stmt)
{
	assert(stmt.isA!"ES6.Statement");
	stmt = stmt.getNthChild(1);
	if (stmt.isA!"ES6.ExpressionStatement")
		return stmt.getNthChild(1);
	assert(stmt.isA!"ES6.ReturnStatement");
	if (stmt.children.length > 0)
		return stmt.getNthChild(1);
	return generateVoid0Expression();
}
struct StatementList
{
	Node node;
	alias node this;
	Node convertToAssignmentExpression()
	{
		import std.algorithm : all;
		import std.array : array;
		Node[] assigns;
		auto exprs = node.children.map!(item=>item.getNthChild(1).convertStatementToExpression).array();
		assert(exprs.all!(c=>c.isA!"ES6.Expression"));
		assigns = exprs.getAssignmentExpressionsFromExpressions();
		if (assigns.length == 1)
			return assigns[0];

		auto expr = assigns.createExpressionFromAssignmentExpressions;
		return expr.parenthesisExprIntoAssignmentExpression;
	}
	Node getParentStatement()
	{
		return node.getNthParent(3);
	}
}
auto asStatementList(Node node)
{
	import std.typecons : Nullable;
	Nullable!StatementList n;
	if (!node.isA!"ES6.StatementListItem" && (node.isA!"ES6.StatementList" || node.isA!"ES6.ModuleItemList"))
		n = StatementList(node);
	return n;
}
Node asNode(T)(T t)
{
	return t;
}
struct IfPath
{
	Node node;
	alias node this;
	bool isBlockStatement()
	{
		return node.children[0].isA!"ES6.BlockStatement";
	}
	bool isExpressionStatement()
	{
		return node.children[0].isA!"ES6.ExpressionStatement";
	}
	Node getExpressionStatement()
	{
		assert(isExpressionStatement());
		return node.children[0];
	}
	StatementList getStatementList()
	{
		assert(isBlockStatement);
		auto curly = node.getNthChild(2);
		if (curly.children.length == 0)
			curly.addChild(new Node(ParseTree("ES6.StatementList",true)));
		return StatementList(curly.children[0]);
	}
	Node getExpression()
	{
		assert(isExpressionStatement);
		return node.getNthChild(2);
	}
	void setListChildren(Node[] cs)
	{
		if (!isBlockStatement)
		{
			bool truePath = false;
			auto branch = node.branch;
			if (node.parent.children[1] is node)
				truePath = true;
			node = createStatementListStatement(cs,true,node.parent);
			if (truePath)
				node.parent.children[1] = node;
			else
				node.parent.children[3] = node;
			node.propagateBranch(branch);
			return;
		}
		getStatementList().setChildren(cs);
		return;
	}
	IfPath convertToBlockStatement()
	{
		assert(!isBlockStatement);
		bool truePath = false;
		if (node.parent.children[1] is node)
			truePath = true;
		auto scp = node.scp;
		auto branch = node.branch;
		node = convertStatementIntoBlockStatement(node);
		node.scp = scp;
		branch.entry = node;
		if (truePath)
			node.parent.children[1] = node;
		else
			node.parent.children[3] = node;
		return this;
	}
	bool returns()
	{
		return cast(bool)(node.hint & (Hint.EmitReturn | Hint.EmptyReturn | Hint.Return));
	}
	bool isEmpty()
	{
		if (isBlockStatement)
		{
			auto list = getStatementList();
			return list is null || list.children.length == 0;
		}
		return node.children[0].isA!"ES6.EmptyStatement";
	}
}
struct IfStatement
{
	Node node;
	alias node this;

	@property auto truthPath()
	{
		return IfPath(node.children[1]);
	}
	@property void truthPath(Node path)
	{
		node.children[1] = path;
		path.parent = node;
	}
	@property auto elsePath()
	{
		assert(hasElsePath);
		return IfPath(node.children[3]);
	}
	@property void elsePath(Node path)
	{
		assert(hasElsePath);
		node.children[3] = path;
		path.parent = node;
	}
	@property Node condition()
	{
		return node.children[0];
	}
	@property Expression conditionExpression()
	{
		return node.getNthChild(2).asExpression;
	}
	bool doBothBranchesAlwaysReturn()
	{
		return cast(bool)(node.hint & Hint.DeducedReturingBranch);
	}
	IfPath createElsePath(Node[] cs = [])
	{
		assert(!hasElsePath);
		node.addChild(new Node(ParseTree("ES6.ElseKeyword",true,["else"])));
		node.addChild(createStatementListStatement(cs,true,node));
		return elsePath();
	}
	bool hasElsePath()
	{
		return node.children.length == 4;
	}
}
auto asIfStatement(Node node)
{
	import std.typecons : Nullable;
	Nullable!IfStatement n;
	if (node.isA!"ES6.IfStatement")
		n = IfStatement(node);
	return n;
}
@("Test getExpressionStatement on IfPath")
unittest
{
	auto root = ES6(`if(a) b; else d;`).createNode();
	root.getChild("ES6.IfStatement").asIfStatement.truthPath().getExpressionStatement.toJS.shouldEqual("b");
}
@("Test setListChildren on IfPath")
unittest
{
	auto root = ES6(`if(a) b; else d;`).createNode();
	root.analyseNodes();
	auto replacer = ES6(`op`).createNode().getChild("ES6.Statement");
	root.getChild("ES6.IfStatement").asIfStatement.truthPath().setListChildren([replacer]);
	root.toJS.shouldEqual(`if (a) { op } else  d`);
	auto replacer2 = ES6(`op`).createNode().getChild("ES6.Statement");
	root.getChild("ES6.IfStatement").asIfStatement.elsePath().setListChildren([replacer2]);
	root.toJS.shouldEqual(`if (a) { op } else { op }`);
}
Node getFirstAssignmentLeftHandSideExpression(Node stmt)
{
	if (stmt is null) return null;
	Node expr;
	if (stmt.isA!"ES6.StatementListItem")
	{
		expr = stmt.getNthChild(3);
	} else 
	{
		assert(stmt.isA!"ES6.Statement");
		expr = stmt.getNthChild(2);
	}
	if (expr is null || expr.children.length == 0)
		return null;
	auto lastAssignExpr = expr.children[$-1];
	if (lastAssignExpr.children.length < 3)
		return null;
	return lastAssignExpr.children[0];
}
Node createExpressionFromAssignmentExpressions(Node[] assigns)
{
	Node[] children;
	children.length = (assigns.length*2)-1;
	foreach (idx, a; assigns[0..$-1])
	{
		children[idx*2] = a;
		children[idx*2+1] = new Node(ParseTree("ES6.Comma",true,[","]));
	}
	children[$-1] = assigns[$-1];
	return new Node(ParseTree("ES6.Expression",true),children);
}
Node createExpressionStatementFromAssignmentExpression(Node assignExpr)
{
	return new Node(ParseTree("ES6.ExpressionStatement",true),[
	 new Node(ParseTree("ES6.Expression",true),[assignExpr])
	]);
}
Node createExpressionStatementFromCommaSeparatedAssignments(Node[] csa)
{
	return new Node(ParseTree("ES6.ExpressionStatement",true),[
	 new Node(ParseTree("ES6.Expression",true),csa)
	]);
}
Node reduceExpressions(Range)(Range nodes)
{
	return getAssignmentExpressionsFromExpressions(nodes).createExpressionFromAssignmentExpressions();
}
import std.range : isInputRange;
auto getAssignmentExpressionsFromExpressions(Range)(Range nodes)
	if (isInputRange!Range)
{
	Node[] assigns;
	import std.range : stride;
	foreach(n; nodes)
		foreach(c; n.children.stride(2))
			assigns~=c;
	return assigns;
}
auto getAssignmentExpressionsFromExpression(Node expr)
{
	import std.range : stride;
	Node[] assigns;
	foreach(c; expr.children.stride(2))
		assigns~=c;
	return assigns;
}
Node createStatementListItemFromCommaSeparatedAssignments(Node[] csa)
{
	return new Node(ParseTree("ES6.StatementListItem",true),[
		new Node(ParseTree("ES6.Statement",true),[
			createExpressionStatementFromCommaSeparatedAssignments(csa)
		])
	]);
}
Node parenthesisAssignmentExpressionIntoPostfixExpression(Node assignExpr)
{
	assert(assignExpr.isA!"ES6.AssignmentExpression");
	return new Node(ParseTree("ES6.PostfixExpression",true),
		[
		 new Node(ParseTree("ES6.LeftHandSideExpression",true),
		 [
		  new Node(ParseTree("ES6.NewExpression",true),
		  [
		   new Node(ParseTree("ES6.MemberExpression",true),
		   [
		    new Node(ParseTree("ES6.PrimaryExpression",true),
		    [
		     new Node(ParseTree("ES6.CoverParenthesizedExpressionAndArrowParameterList",true),
		     [
		      new Node(ParseTree("ES6.Parentheses",true),
		      [
		       new Node(ParseTree("ES6.ExpressionIn",true),[assignExpr])
	])])])])])])]);
}
Node parenthesisExprIntoAssignmentExpression(Node expr)
{
	assert(expr.isA!"ES6.Expression");
	return new Node(ParseTree("ES6.AssignmentExpressionIn",true),
	[
		new Node(ParseTree("ES6.ConditionalExpressionIn",true),
		[
			new Node(ParseTree("ES6.RightHandSideExpressionIn",true),
			[
				new Node(ParseTree("ES6.UnaryExpression",true),
				[
					new Node(ParseTree("ES6.PostfixExpression",true),
					[
						new Node(ParseTree("ES6.LeftHandSideExpression",true),
						[
							new Node(ParseTree("ES6.NewExpression",true),
							[
								new Node(ParseTree("ES6.MemberExpression",true),
								[
									new Node(ParseTree("ES6.PrimaryExpression",true),
									[
										new Node(ParseTree("ES6.CoverParenthesizedExpressionAndArrowParameterList",true),
										[
											new Node(ParseTree("ES6.Parentheses",true),
											[
												expr
											])
										])
									])
								])
							])
						])
					])
				])
			])
		])
	]);
}
Node convertToAssignmentExpression(Node node)
{
	import std.array : array;
	Node[] assigns;
	auto list = node.getNthChild(3);
	if (list.isA!"ES6.StatementList")
	{
		return list.asStatementList.convertToAssignmentExpression();
	} else
	{
		assert(node.isA!"ES6.Statement");
		auto expr = node.convertStatementToExpression();
		assigns = expr.getAssignmentExpressionsFromExpression();
	}
	if (assigns.length == 1)
		return assigns[0];

	auto expr = assigns.createExpressionFromAssignmentExpressions;
	return expr.parenthesisExprIntoAssignmentExpression;
}
unittest
{
	auto getNodes(string js)
	{
		import es5.analyse;
		auto p = ES6(js);
		auto node = createNode(p);
		auto a = analyseNodes(node);
		return node;
	}
	void assertCode(Node n, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : writeln;
		import es5.tojs;
		auto got = n.toJS();
		if (got == expected)
			return;
		Message m;
		m.writeln(n);
		m.writeln("Expected:");
		m.writeln(expected);
		m.writeln("Got:");
		m.writeln(got);
		failed(m,file,line);
	}
	auto n1 = getNodes(`var a = 45 > 56;`);
	auto c1 = n1.getChild("ES6.AssignmentExpression");
	negateExpression(c1);
	assertCode(n1,"var a = !45 > 56;").shouldThrow();
	assertCode(n1,"var a = !(45 > 56);");

	auto n2 = getNodes(`if (b > 6) b = 5;`);
	n2.getChild("ES6.AssignmentExpression").negateExpression;
	assertCode(n2,"if (!(b > 6)) b = 5;");

	auto n3 = getNodes(`if (c) b = 5;`);
	n3.getChild("ES6.AssignmentExpression").negateExpression;
	assertCode(n3,"if (!c) b = 5;");

	auto n4 = getNodes(`if (!!a) b = 5;`);
	n4.getChild("ES6.AssignmentExpression").negateExpression;
	assertCode(n4,"if (!a) b = 5;");

	auto n5 = getNodes(`if (!(a && b)) b = 5;`);
	n5.getChild("ES6.AssignmentExpression").negateExpression;
	assertCode(n5,"if ((a && b)) b = 5;");

	auto n6 = getNodes(`if (a && b) b = 5;`);
	n6.getChild("ES6.AssignmentExpression").negateExpression;
	assertCode(n6,"if (!(a && b)) b = 5;");
}
Node convertStatementIntoBlockStatement(Node node)
{
	assert(node.isA!"ES6.Statement");
	bool inFunction = node.isA!"ES6.StatementReturn";
	// TODO: we have to watch out with the xxxReturn Nodes. Return is only correct if the node itself is a xxxReturn node. Else we need to drop the Return part on all nodes.
	auto scp = node.scp;
	auto parent = node.parent;
	auto idx = parent.findIndex(node);
	auto block = new Node(ParseTree(inFunction ? "ES6.StatementReturn" : "ES6.Statement",true,["{"]),
		[
			new Node(ParseTree(inFunction ? "ES6.BlockStatementReturn" : "ES6.BlockStatement",true),
			[
				new Node(ParseTree("ES6.CurlyBrackets",true),
				[
					new Node(ParseTree(inFunction ? "ES6.StatementListReturn" : "ES6.StatementList",true),
					[
						new Node(ParseTree(inFunction ? "ES6.StatementListItemReturn" : "ES6.StatementListItem",true),
						[
							node
						])
					])
				])
			])
		]);
	block.parent = parent;
	block.scp = scp;
	parent.children[idx] = block;
	return block;
}
@("convertStatementIntoBlockStatement")
unittest {
	auto n = ES6(`bla()`).createNode;
	n.getChild("ES6.StatementList").children[0].convertStatementIntoBlockStatement();
	assertTree(n,`{ bla() }`);
}
Node getStatementListFromScope(Scope scp)
{
	Node list;
	if (scp.entry.isA!"ES6.FunctionBody")
		list = scp.entry.getNthChild(2);
	else if (scp.entry.isA!"ES6")
		list = scp.entry.getNthChild(3);
	assert(list.isA!"ES6.StatementList" || list.isA!"ES6.FunctionStatementList" || list.isA!"ES6.ModuleItemList");
	return list;
}
@("getStatementListFromScope")
unittest
{
	getStatementListFromScope(ES6(`var a = 4;`).createNode.analyseNodes.scp).name.shouldEqual("ES6.ModuleItemList");
}
ptrdiff_t findFirstNonVarStatementAndNonFuncDecl(Node statementList)
{
	if (statementList.children.length == 0)
		return -1;
	foreach(idx, c; statementList.children)
	{
		auto item = c.getNthChild(3);
		if (!item.isA!"ES6.FunctionDeclaration" && !item.isA!"ES6.VariableDeclarationList")
			return idx;
	}
	return -1;
}
@("findFirstNonVarStatementAndNonFuncDecl")
unittest 
{
	findFirstNonVarStatementAndNonFuncDecl(new Node(ParseTree("Bla",true))).shouldEqual(-1);
}
Node getLastVariableDeclarationList(Node statementList)
{
	assert(statementList.isA!"ES6.StatementList" || statementList.isA!"ES6.ModuleItemList");
	Node f;
	foreach(c; statementList.children)
	{
		auto item = c.getNthChild(3);
		if (item.isA!"ES6.VariableDeclarationList")
			f = item;
		else if (!item.isA!"ES6.FunctionDeclaration")
			break;
	}
	return f;
}
Node getFirstVariableDeclarationList(Node statementList)
{
	assert(statementList.isA!"ES6.StatementList" || statementList.isA!"ES6.ModuleItemList");
	foreach(c; statementList.children)
	{
		auto item = c.getNthChild(3);
		if (item.isA!"ES6.VariableDeclarationList")
			return item;
	}
	return null;
}
void addVariableDeclarationToStatementList(Node statementList, string identifier, bool insertAtBack = true)
{
	addVariableDeclarationToStatementList(statementList,createVariableDeclarationNode(identifier));
}
void addVariableDeclarationToStatementList(Node statementList, Node varDecl, bool insertAtBack = true)
{
	assert(statementList.isA!"ES6.StatementList" || statementList.isA!"ES6.ModuleItemList");
	Node declList;
	if (insertAtBack)
		declList = statementList.getLastVariableDeclarationList();
	else
		declList = statementList.getFirstVariableDeclarationList();
	if (declList is null)
	{
		auto stmt = createVariableDeclarationListStatement([varDecl]);
		statementList.insertAtFront(stmt);
		if (statementList.branch !is null)
			stmt.propagateBranch(statementList.branch);
		return;
	}
	if (insertAtBack)
		declList.addChild(varDecl);
	else
		declList.insertAtFront(varDecl);
	if (declList.branch !is null)
		varDecl.propagateBranch(declList.branch);
}
void addVariableDeclarationsToStatementList(Node statementList, Node declList)
{
	assert(declList.isA!"ES6.VariableDeclarationList");
	assert(statementList.isA!"ES6.StatementList" || statementList.isA!"ES6.ModuleItemList");
	auto list = statementList.getLastVariableDeclarationList();
	if (list is null)
	{
		statementList.insertAtFront(createVariableDeclarationListStatement(declList.children));
		return;
	}
	list.addChildren(declList.children);
}
bool isLiteralDeclarationInitializer(Node init)
{
	auto unExpr = init.getNthChild(4);
	if (!unExpr.isA!"ES6.UnaryExpression")
		return false;
	return unExpr.children[$-1].getNthChild(5).isA!"ES6.Literal";
}
/++
	This function will move and group all variable and function declarations first
	in their respective statementlist. Function declarations (if any) come first, then
	possibly one combined VariableStatement.

	Note: This can be done more effective and efficient by combining the bubble
	and groupAndOrder steps; by grouping and ordering while bubbling.

	Note: Run this before analyseNodes(). Not after.
+/
void moveVariableAndFunctionDeclarationsUpFront(Node node)
{
	// this will move all variable and function declarations up front, unordered
	void bubble(Node node, Node statementList = null, bool inBranch = false)
	{
		bool isList = false;
		if (node.isA!"ES6.ModuleItemList")
		{
			statementList = node;
			isList = true;
		}
		else if (statementList is null && node.isA!"ES6.StatementList" && !node.isA!"ES6.StatementListItem")
		{
			statementList = node;
			isList = true;
		}
		else if (node.isA!"ES6.IterationStatement" && node.children.length > 0 && node.children[0].isA!"ES6.ForKeyword")
		{
			auto parentheses = node.children[1];
			// can be while {},  do {} while,  for(var a = 4; a< 10; a++),  for(var b in arr),  for(var b of obj)
			if (parentheses.children[1].isA!"ES6.VariableDeclarationList")
			{
				// a for (var b = 0; b < 10; b++) kind of loop
				auto declarationList = parentheses.children[1];
				Node[] initializers;
				foreach(c; declarationList.children)
				{
					if (c.children.length > 1 && c.children[1].isA!"ES6.Initializer" && (!c.children[1].isLiteralDeclarationInitializer || inBranch))
					{
						initializers ~= c.children[1];
						bubble(c.children[1],statementList,inBranch);
						c.children.length = 1;
					}
				}

				Node[] expressions;
				foreach(idx, i; initializers)
				{
					auto condExpr = i.children[0].children[0];
					auto identifier = i.parent.getChild("ES6.IdentifierName");

					assert(identifier !is null);
					auto assignExpr = new Node(ParseTree("ES6.AssignmentExpressionIn",true),
						[
							createLHSExpressionFromPrimaryExpressionChild(createIdentifierReference(identifier.matches[0]))
							,new Node(ParseTree("ES6.AssignmentOperator",true,["="],"="))
							,condExpr
						]);

					expressions ~= assignExpr;
					if (idx < initializers.length-1)
						expressions ~= new Node(ParseTree("ES6.Comma",true,[","]));
				}
				parentheses.removeChild(1);
				parentheses.setChild(0,new Node(ParseTree("ES6.Expression",true),expressions));

				statementList.addVariableDeclarationsToStatementList(declarationList);
			} else if (parentheses.children[0].isA!"ES6.VarKeyword")
			{
				assert(parentheses.children[1].isA!"ES6.ForBinding");
				auto identifier = parentheses.children[1].getNthChild(2);
				parentheses.setChild(1,createLHSExpressionFromPrimaryExpressionChild(createIdentifierReference(identifier.matches[0])));
				parentheses.children = parentheses.children[1..$];
				statementList.addVariableDeclarationToStatementList(identifier.matches[0]);
			}
			bubble(node.children[$-1],statementList,true);
			return;
		}
		else if (node.isA!"ES6.VariableStatement")
		{
			auto item = node.getNthParent(2);
			auto declarationList = node.children[0];
			bool skip = false;
			if (item.isA!"ES6.StatementListItem" && item.parent == statementList)
			{
				auto idx = item.parent.findIndex(item);
				auto nIdx = statementList.findFirstNonVarStatementAndNonFuncDecl();
				if (nIdx == -1 || idx <= nIdx)
					skip = true;
			}
			// NOTE: this can also be part of a ES6.ExportDeclaration. Should we also split the vars in that case?
			if (!skip)
			{
				Node[] initializers;
				foreach (c; declarationList.children)
					if (c.children.length > 1 && c.children[1].isA!"ES6.Initializer" && (!c.children[1].isLiteralDeclarationInitializer || inBranch))
					{
						initializers ~= c.children[1];
						bubble(c.children[1],statementList,inBranch);
						c.children.length = 1; // cut out the initializer
					}

				auto holder = node.parent.parent;
				Node[] statements;
				// convert the initializers to statements
				foreach(i; initializers)
				{
					auto identifier = i.parent.getChild("ES6.IdentifierName");
					auto assignExpr = i.children[0];
					assignExpr.insertAtFront([
							createLHSExpressionFromPrimaryExpressionChild(createIdentifierReference(identifier.matches[0]))
							,new Node(ParseTree("ES6.AssignmentOperator",true,["="],"="))
						]);
					statements ~= new Node(ParseTree("ES6.Statement",true),
					[
						new Node(ParseTree("ES6.ExpressionStatement",true),
						[
							new Node(ParseTree("ES6.ExpressionIn",true),
							[
								assignExpr
							])
						])
					]);
				}
				if (holder.isA!"ES6.StatementListItem")
				{
					auto list = holder.parent.asStatementList;
					auto listParentStmt = list.getParentStatement();
					if (list.children.length == 1 && statements.length == 1 && listParentStmt.isA!"ES6.Statement")
					{
						listParentStmt.setChild(0, statements[0].children[0]);
					} else
					{
						auto idx = holder.parent.findIndex(holder);
						holder.parent.removeChild(idx);
						import std.array : array;
						if (statements.length > 0)
							list.insertInPlace(idx, statements.map!(s=>new Node(ParseTree("ES6.StatementListItem",true),[s])).array);
					}
				} else
				{
					holder = node.parent;
					if (statements.length == 0)
					{
						holder.setChild(0,new Node(ParseTree("ES6.EmptyStatement",true)));
					} else if (statements.length == 1)
					{
						holder.setChild(0,statements[0].children[0]);
					} else
					{
						import std.array : array;
						holder.matches[0] = "{";
						holder.setChild(0,
							new Node(ParseTree("ES6.BlockStatement",true),
							[
								new Node(ParseTree("ES6.CurlyBrackets",true),
								[
									new Node(ParseTree("ES6.StatementList",true),
										statements.map!(s=>new Node(ParseTree("ES6.StatementListItem",true),[s])).array
									)
								])
							]));
					}
				}
				statementList.addVariableDeclarationsToStatementList(declarationList);
			}
		} else if (node.isA!"ES6.FunctionDeclaration")
		{
			auto sli = node.parent.parent.parent;
			sli.parent.bringToFront(sli);
			inBranch = false;
			statementList = null;
		} else if (node.isA!"ES6.FunctionExpression")
		{
			inBranch = false;
			statementList = null;
		} else if (node.isA!"ES6.BreakableStatement" || node.isA!"ES6.IfStatement")
		{
			inBranch = true;
		}
		if (node.children.length == 0)
			return;
		if (node.children.length == 1)
		{
			bubble(node.children[0],statementList,inBranch);
			return;
		}
		// we have to dup the array since we are shifting elements on the fly
		auto children = node.children.dup;
		foreach(c; children)
			bubble(c,statementList,inBranch);
	}
	// this will group variable declarations and order function declarations up top
	void groupAndOrder(Node node)
	{
		if (node.isA!"ES6.ModuleItemList" || (node.isA!"ES6.StatementList" && !node.isA!"ES6.StatementListItem"))
		{
			size_t firstNonFunctionDeclaration = 0;
			foreach(idx, c; node.children.dup)
			{
				auto t = c.getNthChild(3);

				if (t !is null && t.isA!"ES6.FunctionDeclaration")
				{
					node.children.moveElement(idx,0);
					firstNonFunctionDeclaration++;
				} else if (t is null || !t.isA!"ES6.VariableDeclarationList")
					break;
			}
			Node firstList = null;
			size_t firstNonVariableDeclarationList = firstNonFunctionDeclaration;
			foreach(c; node.children[firstNonFunctionDeclaration..$])
			{
				auto list = c.getNthChild(3);
				if (list is null || !list.isA!"ES6.VariableDeclarationList")
					break;
				firstNonVariableDeclarationList++;
				if (firstList is null)
				{
					firstList = list;
					continue;
				}
				firstList.children ~= list.children;
			}
			node.children = node.children.removeRange(firstNonFunctionDeclaration+1,firstNonVariableDeclarationList);
		}
		foreach (c; node.children)
			groupAndOrder(c);
	}
	bubble(node);
	groupAndOrder(node);
}
Node getVariableDeclarationList(Node node)
{
	assert(node.isA!"ES6.ModuleItemList" || node.isA!"ES6.StatementList");
	foreach(c; node.children)
	{
		auto t = c.getNthChild(3);
		if (t.isA!"ES6.VariableDeclarationList")
			return t;
		if (!t.isA!"ES6.FunctionDeclaration")
			return null;
	}
	return null;
}
@("getVariableDeclarationList")
unittest
{
	(new Node(ParseTree("ES6.StatementList"))).getVariableDeclarationList.shouldBeNull();
}
@("moveVariableAndFunctionDeclarationsUpFront")
unittest
{
	auto analyseAndMinify(Node root)
	{
		import es5.tojs;
		import es5.analyse;
		root.analyseNodes();
		return root.toJS();
	}
	void assertMinified(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : writeln;
		try
		{
			auto nodes = ES6(js).createNode;
			moveVariableAndFunctionDeclarationsUpFront(nodes);
			assertTree(nodes,expected);
			auto got = analyseAndMinify(nodes);
			if (got == expected)
				return;
			Message m;
			m.writeln(nodes);
			m.writeln("Expected:");
			m.writeln(expected);
			m.writeln("Got:");
			m.writeln(got);
			m.writeln("At:");
			m.write(file);
			m.write("@");
			m.write(line);
			failed(m,file,line);
		} catch (Throwable e)
		{
			Message m;
			m.writeln(e.msg);
			failed(m,file,line);
		}
	}
	assertMinified(`var a;`,`var b;`).shouldThrow();
	assertMinified(
		`bla();var d = 5;`,
		"var d = 5; bla()"
	);
	assertMinified(
		`bla();function d(){return 5}`,
		"function d() { return 5 }; bla()"
	);

	// if else cases
	assertMinified(
		`if(d) { var b; e(); }`,
		"var b; if (d) { e() }"
	);
	assertMinified(
		`if(d) { var b = 4; }`,
		"var b; if (d) b = 4"
	);
	assertMinified(
		`var c; if(d) { var b = 4; }`,
		"var c, b; if (d) b = 4"
	);
	assertMinified(
		`var c; if(d) { var b = 4, d, e = 5; }`,
		"var c, b, d, e; if (d) { b = 4; e = 5 }"
	);
	assertMinified(
		`if(d) var b = 4;`,
		"var b; if (d) b = 4;"
	);
	assertMinified(
		`var c; if(d) var b = 4;`,
		"var c, b; if (d) b = 4;"
	);
	assertMinified(
		`var c; if(d) var b = 4, d, e = 5;`,
		"var c, b, d, e; if (d) { b = 4; e = 5 }"
	);
	assertMinified(
		"var c; if (d) var b = 6; else var b = 66;",
		"var c, b, b; if (d) b = 6; else  b = 66"				// yup, two b's, that gets resolved in another pass
	);
	assertMinified(
		`var a,b,c; if(d) var c = 56; else var b =12, a =16;`,
		"var a, b, c, c, b, a; if (d) c = 56; else { b = 12; a = 16 }"
	);
	assertMinified(
		`if(d) var b;`,
		"var b; if (d) ;"
	);

	// switches
	assertMinified(
		`var k; switch(d) { case 6: var b = 4; }`,
		"var k, b; switch (d) { case 6: b = 4; }"
	);

	// whiles
	assertMinified(
		`var p; while(true) { var b = 4; }`,
		"var p, b; while(true)b = 4"
	);
	assertMinified(
		`var o; while(true) var b = 4;`,
		"var o, b; while(true)b = 4"
	);
	assertMinified(
		`var l; while(true) var b = 4, h, g = 12;`,
		"var l, b, h, g; while(true){ b = 4; g = 12 }"
	);
	assertMinified(
		`var a; while(true) { var b = 4; }`,
		"var a, b; while(true)b = 4"
	);
	assertMinified(
		`var a; while(true) var b = 4, h, g = 12;`,
		"var a, b, h, g; while(true){ b = 4; g = 12 }"
	);
	
	// do-whiles
	assertMinified(
		`var p; do var b = 5; while(true);`,
		"var p, b; do b = 5;while (true);"
	);
	assertMinified(
		`var m; do { var b = 5; } while(true);`,
		"var m, b; do b = 5;while (true);"
	);
	assertMinified(
		`var f; do var b = 5, g, h = 12; while(true);`,
		"var f, b, g, h; do { b = 5; h = 12 }while (true);"
	);
	assertMinified(
		`var p; do { var b = 5, g, h = 12; } while(true);`,
		"var p, b, g, h; do { b = 5; h = 12 }while (true);"
	);

	// for in's, for of's
	assertMinified(
		`var g; if (a) for (var b in key) var d = 5;`,
		"var g, b, d; if (a) for(b in key)d = 5;"
	);
	/* Depends on Issue #6 of es6.grammar
	assertMinified(
		`var r; for (var b of key) var d = 5;`,
		"var r, b, d; for(b of key){ d = 5 }"
	);*/

	// normal for loops
	/*assertMinified(
		`for (d = g,b = 8; d < 0; d++) var f = 5;`,
		"var f,d;for(b=g,b=8;d<0;d++)f=5;"
	);*/
	assertMinified(
		`var w; for (var d = g; d < 0; d++) var f = 5;`,
		"var w, d, f; for(d = g;d < 0;d++)f = 5"
	);

	// check functions decl
	assertMinified(
		`b = 6; function d() { return 4; }`,
		"function d() { return 4 }; b = 6"
	);
	assertMinified(
		`var b = 6; function d() { return 4; } var c = 5;`,
		"function d() { return 4 }; var b = 6, c = 5;"
	);
	assertMinified(
		`var b = 6; function d() { var j = 5; if (b) { var c = 55; } return c; } var c = 5;`,
		"function d() { var j = 5, c; if (b) c = 55; return c }; var b = 6, c = 5;"
	);
	assertMinified(
		`var b = 6; function d() { var j = 5; function hoppa() { if(j) var uu = 0; return uu; } if (b) { var c = 55; } return c; } var c = 5;`,
		"function d() { function hoppa() { var uu; if (j) uu = 0; return uu }; var j = 5, c; if (b) c = 55; return c }; var b = 6, c = 5;"
	);
	assertMinified(
		`var a = {b: 5}; var b = {b: a.b};`,
		"var a = { b: 5 }, b = { b: a.b };"
	);
	assertMinified(
		`var a; a = 5; var b = a;`,
		"var a, b; a = 5; b = a"
	);
	assertMinified(
		`var a; var b = 5;`,
		"var a, b = 5;"
	);
	// check function expressions
	assertMinified(
		`var b; b = function d() { var j = 5; if (b) { var c = 55; } return c; } var c = 5;`,
		"var b, c = 5; b = function d(){ var j = 5, c; if (b) c = 55; return c }"
	);
	assertMinified(
		`g = 6;var a = 6;var b = null;var c = 8`,
		"var a = 6, b = null, c = 8; g = 6"
	);
	assertMinified(
		`g = 6;var a = 6;var b = null, d = functionCall();var c = 8; h = 16`,
		"var a = 6, b = null, d, c = 8; g = 6; d = functionCall(); h = 16"
	);
	assertMinified(
		`g = 6;var a = 6;t = 6;var b = null;o = 7;var c = 8`,
		"var a = 6, b = null, c = 8; g = 6; t = 6; o = 7"
	);
	// TODO: check function replacements

	// TODO: change moveVariableAndFunctionDeclarationsUpFront a bit to first dump all tobe moved declarations and function in a temp array. Then finally add them to the statementList. This way we don't get var and function declarations mixed up
	// TODO: what about for (var b = 0; b < 10; b++)
	// TODO: what about `if (b) function bla() { return 4; } else function b(){ return 6}`


	//node = getNodes(`if(d)var b = 5, d, e = function(){return 54;};`);
	//writeln(node);
	//list = node.getChild("ES6.ModuleItemList");
}
Node createStatementListStatement(Node[] children, bool inFunction, Node parent)
{
	auto ffp = new Node(ParseTree(inFunction ? "ES6.StatementListReturn" : "ES6.StatementList",true),children);
	return new Node(ParseTree(inFunction ? "ES6.StatementReturn" : "ES6.Statement",true,["{"]),
		[
			new Node(ParseTree(inFunction ? "ES6.BlockStatementReturn" : "ES6.BlockStatement",true),
			[
				new Node(ParseTree("ES6.CurlyBrackets",true),
				[
					ffp
				])
			])
		],parent);
}
Node createLocigalExpressionOperator(alias op)()
{
	auto a = [op];
	return new Node(ParseTree("ES6.ExpressionOperatorIn",true,a),
	[
		new Node(ParseTree("ES6.LogicalOperator",true,a))
	]);
}
/*Node parenthesesConditionalExpression(Node c)
{
	assert(c.isA!"ES6.ConditionalExpression");
	return parenthesisAssignmentExpressionIntoConditionalExpression(
		new Node(ParseTree("ES6.AssignmentExpressionIn",true),[c])
	);
}*/
Node parenthesisAssignmentExpressionIntoConditionalExpression(Node assign)
{
	assert(assign.isA!"ES6.AssignmentExpression","Node is not an AssignmentExpression");
	return new Node(ParseTree("ES6.ConditionalExpressionIn",true),
	[
		new Node(ParseTree("ES6.RightHandSideExpressionIn",true),
		[
			new Node(ParseTree("ES6.UnaryExpression",true),
			[
				new Node(ParseTree("ES6.PostfixExpression",true),
				[
					new Node(ParseTree("ES6.LeftHandSideExpression",true),
					[
						new Node(ParseTree("ES6.NewExpression",true),
						[
							new Node(ParseTree("ES6.MemberExpression",true),
							[
								new Node(ParseTree("ES6.PrimaryExpression",true),
								[
									new Node(ParseTree("ES6.CoverParenthesizedExpressionAndArrowParameterList",true),
									[
										new Node(ParseTree("ES6.Parentheses",true),
										[
											new Node(ParseTree("ES6.ExpressionIn",true),
											[
												assign
											])
										])
									])
								])
							])
						])
					])
				])
			])
		])
	]);
}
Node getArrowFunctionsFunctionBody(Node node)
{
	assert(node.isA!"ES6.ArrowFunction","Node is not a ArrowFunction");
	return node.children[1].children[0].children[0];
}
Node getFunctionDeclarationsFunctionBody(Node node)
{
	assert(node.isA!"ES6.FunctionDeclaration","Node is not a FunctionDeclaration");
	return node.children[2].children[0];
}
bool isDoWhileIteration(Node node)
{
	assert(node.isA!"ES6.IterationStatement","Node is not a IterationStatement");
	return node.children[0].isA!"ES6.DoKeyword";
}
struct LHSExpression
{
	Node node;
	alias node this;
	bool isPrimaryExpression()
	{
		return node.getNthChild(3).isA!"ES6.PrimaryExpression";
	}
	Node getPrimaryExpression()
	{
		return node.getNthChild(3);
	}
	bool hasArguments()
	{
		return node.children[0].isA!"ES6.CallExpression" || node.children[0].children.length > 1;
	}
	Node[] getMemberExpressionAccessors()
	{
		auto memExpr = node.getNthChild(2);
		assert(!memExpr.isA!"ES6.SuperCall"); // this is a supercall, it has no
		if (memExpr.children[0].isA!"ES6.MemberExpression")
			return memExpr.children[2..$];
		return memExpr.children[1..$];
	}
}
auto asLHSExpression(Node node)
{
	import std.typecons : Nullable;
	Nullable!LHSExpression n;
	if (node.isA!"ES6.LeftHandSideExpression")
		n = LHSExpression(node);
	return n;
}
struct IterationStatement
{
	Node node;
	alias node this;
	bool isForLoop() {
		return node.children[0].isA!"ES6.ForKeyword";
	}
	bool isDoWhileLoop() {
		return node.children[0].isA!"ES6.DoKeyword";
	}
	bool isWhileLoop() {
		return node.children[0].isA!"ES6.WhileKeyword";
	}
	bool canHaveCondition() {
		if (isDoWhileLoop() || isWhileLoop())
			return true;
		if (node.children[1].children[1].isA!"ES6.ForBinding")
			return false;
		if (node.children[1].children[0].isA!"ES6.ForDeclaration")
			return false;
		if (node.children[1].children[0].isA!"ES6.LeftHandSideExpression")
			return false;
		return true;
	}
	bool hasCondition() {
		if (!canHaveCondition())
			return false;
		if (isDoWhileLoop() || isWhileLoop())
			return true;
		auto firstParenthesised = node.children[1].children[0];
		if (firstParenthesised.isA!"ES6.VarKeyword")
			return node.children[1].children[3].isA!"ES6.ExpressionIn";
		if (firstParenthesised.isA!"ES6.LexicalDeclaration")
			return node.children[1].children[1].isA!"ES6.ExpressionIn";
		if (firstParenthesised.isA!"ES6.Expression")
			return node.children[1].children[2].isA!"ES6.ExpressionIn";
		return node.children[1].children[1].isA!"ES6.ExpressionIn";
	}
	Node getCondition() {
		unitAssert(hasCondition(),"Must have condition");
		if (isDoWhileLoop())
			return node.children[3].children[0];
		if (isWhileLoop())
			return node.children[1].children[0];
		auto firstParenthesised = node.children[1].children[0];
		if (firstParenthesised.isA!"ES6.VarKeyword")
			return node.children[1].children[3];
		if (firstParenthesised.isA!"ES6.LexicalDeclaration")
			return node.children[1].children[1];
		if (firstParenthesised.isA!"ES6.Expression")
			return node.children[1].children[2];
		return node.children[1].children[1];
	}
	void setCondition(Expression expr) {
		unitAssert(canHaveCondition(),"Cannot set condition when one is not allowed");
		if (isDoWhileLoop())
			node.children[3].setChild(0,expr);
		else if (isWhileLoop())
			node.children[1].setChild(0,expr);
		else 
		{
			auto firstParenthesised = node.children[1].children[0];
			if (firstParenthesised.isA!"ES6.VarKeyword")
			{
				if (node.children[1].children[3].isA!"ES6.Expression")
					node.children[1].setChild(3,expr);
				else
					node.children[1].insertInPlace(3,expr);
			}
			else if (firstParenthesised.isA!"ES6.LexicalDeclaration" || firstParenthesised.isA!"ES6.Semicolon")
			{
				if (node.children[1].children[1].isA!"ES6.Expression")
					node.children[1].setChild(1,expr);
				else
					node.children[1].insertInPlace(1,expr);
			}
			else if (firstParenthesised.isA!"ES6.Expression")
			{
				if (node.children[1].children[2].isA!"ES6.Expression")
					node.children[1].setChild(2,expr);
				else
					node.children[1].insertInPlace(2,expr);
			}
			else if (node.children[1].children[1].isA!"ES6.ExpressionIn")
				node.children[1].setChild(1,expr);
			else
			{
				node.children[1].insertInPlace(1,expr);
			}
		}
	}
	bool hasBlockStatement() {
		if (isDoWhileLoop)
			return node.children[1].children[0].isA!"ES6.BlockStatement";
		return node.children[$-1].children[0].isA!"ES6.BlockStatement";
	}
	StatementList getStatementList() {
		assert(hasBlockStatement(),"Should have a BlockStatement");

		Node curly;
		if (isDoWhileLoop)
			curly = node.children[1].getNthChild(2);
		else
			curly = node.children[$-1].getNthChild(2);

		if (curly.children.length == 0)
			curly.addChild(new Node(ParseTree("ES6.StatementList",true)));
		return StatementList(curly.children[0]);
	}
	Node getStatement() {
		assert(!hasBlockStatement(),"Should not have a BlockStatement");
		if (isDoWhileLoop)
			return node.children[1];
		return node.children[$-1];
	}
	void setStatement(Node stmt) {
		assert(!hasBlockStatement(),"Should not have a BlockStatement");
		assert(stmt.isA!"ES6.Statement");
		if (isDoWhileLoop)
			node.setChild(1,stmt);
		else
			node.setChild(node.children.length-1,stmt);
	}
}
auto asIterationStatement(Node node)
{
	import std.typecons : Nullable;
	Nullable!IterationStatement n;
	if (node.isA!"ES6.IterationStatement")
		n = IterationStatement(node);
	return n;
}
@("IterationStatement")
unittest
{
	auto getIterationStatement(string js)
	{
		import es6.grammar;
		import es5.analyse;
		auto p = ES6(js);
		auto n = p.createNode();
		return n.getChild("ES6.IterationStatement").asIterationStatement();
	}
	import es5.tojs;
	auto iter = getIterationStatement(`do bla(); while(true);`);
	iter.isDoWhileLoop().shouldBeTrue();
	iter.hasBlockStatement().shouldBeFalse();
	iter.getStatement.toJS().shouldEqual(`bla()`);

	iter = getIterationStatement(`do { bla() } while(true);`);
	iter.hasBlockStatement().shouldBeTrue();
	iter.getStatementList.toJS().shouldEqual(`bla()`);

	iter = getIterationStatement(`while (true) { bla() }`);
	iter.isWhileLoop().shouldBeTrue();
	iter.hasBlockStatement().shouldBeTrue();
	iter.getStatementList.toJS().shouldEqual(`bla()`);

	iter = getIterationStatement(`while (true) bla()`);
	iter.hasBlockStatement().shouldBeFalse();
	iter.getStatement.toJS().shouldEqual(`bla()`);

	iter = getIterationStatement(`for (var k in d) { bla() }`);
	iter.isForLoop().shouldBeTrue();
	iter.hasBlockStatement().shouldBeTrue();
	iter.getStatementList.toJS().shouldEqual(`bla()`);

	iter = getIterationStatement(`for (var k in d) bla()`);
	iter.hasBlockStatement().shouldBeFalse();
	iter.getStatement.toJS().shouldEqual(`bla()`);

	import std.typecons : Flag, Yes, No;

	auto testCondition(string input, Flag!"canHave" canHave, Flag!"has" has, Flag!"canGet" canGet, string jsCondition = "", in string file = __FILE__, in size_t line = __LINE__)
	{
		auto iter = getIterationStatement(input);
		if (canHave == Yes.canHave)
			iter.canHaveCondition.shouldBeTrue(file,line);
		else
			iter.canHaveCondition.shouldBeFalse(file,line);
		if (has == Yes.has)
			iter.hasCondition.shouldBeTrue(file,line);
		else
			iter.hasCondition.shouldBeFalse(file,line);
		if (canGet == Yes.canGet)
			iter.getCondition.toJS.shouldEqual(jsCondition,file,line);
		else
			iter.getCondition.shouldThrow(file,line);
	}

	testCondition(`do { bla = 5; } while(true);`				,Yes.canHave,Yes.has,Yes.canGet,"true");
	testCondition(`while (true) { bla = 5; }`					,Yes.canHave,Yes.has,Yes.canGet,"true");
	testCondition(`for (var k = 0; k < 10; k++) { bla = 5; }`	,Yes.canHave,Yes.has,Yes.canGet,"k < 10");
	testCondition(`for (k = 0; k < 10; k++) { bla = 5; }`		,Yes.canHave,Yes.has,Yes.canGet,"k < 10");
	testCondition(`for (; k < 10; k++) { bla = 5; }`			,Yes.canHave,Yes.has,Yes.canGet,"k < 10");
	testCondition(`for (; ; k++) { bla = 5; }`					,Yes.canHave,No.has,No.canGet);

	testCondition(`for (var k in obj) { bla = 5; }`	,No.canHave,No.has,No.canGet);
	testCondition(`for (k in obj) { bla = 5; }`		,No.canHave,No.has,No.canGet);
	//testCondition(`for (let k in obj) { bla = 5; }`	,No.canHave,No.has,No.canGet); // Wait for Issue #7 in es6-grammar
	//testCondition(`for (const k in obj) { bla = 5; }`	,No.canHave,No.has,No.canGet); // Wait for Issue #7 in es6-grammar
	testCondition(`for (var k of obj) { bla = 5; }`	,No.canHave,No.has,No.canGet);
	//testCondition(`for (k of obj) { bla = 5; }`		,No.canHave,No.has,No.canGet); // Wait for Issue #8 in es6-grammar
	//testCondition(`for (let k of obj) { bla = 5; }`	,No.canHave,No.has,No.canGet); // Wait for Issue #7 in es6-grammar
	//testCondition(`for (const k of obj) { bla = 5; }`	,No.canHave,No.has,No.canGet); // Wait for Issue #7 in es6-grammar

	testCondition(`for (var [k,b] in obj) { l = 6 }`,No.canHave,No.has,No.canGet);
	testCondition(`for (var {k,b} in obj) { l = 6 }`,No.canHave,No.has,No.canGet);
	testCondition(`for ([k,b] in obj) { l = 6 }`	,No.canHave,No.has,No.canGet);
	testCondition(`for ({k,b} in obj) { l = 6 }`	,No.canHave,No.has,No.canGet);

	//testCondition(`for (let [k,b] in obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 9 in es6-grammar
	//testCondition(`for (let [k,b] of obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 9 in es6-grammar
	//testCondition(`for (let {k,b} in obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 10 in es6-grammar
	//testCondition(`for (let {k,b} of obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 10 in es6-grammar
	//testCondition(`for (const [k,b] in obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 11 in es6-grammar
	//testCondition(`for (const [k,b] of obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 11 in es6-grammar
	//testCondition(`for (const {k,b} in obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 12 in es6-grammar
	//testCondition(`for (const {k,b} of obj) { l = 6 }`,	No.canHave,No.has,No.canGet);		// Wait for Issue 12 in es6-grammar

	testCondition(`for (let c = 6, d; c < d; c++) { bla = 5; }`		,Yes.canHave,Yes.has,Yes.canGet,"c < d");
	testCondition(`for (const c = 6, d; c < d; c++) { bla = 5; }`	,Yes.canHave,Yes.has,Yes.canGet,"c < d");

	void testSetCondition(string js, string replace, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import es6.grammar;
		import es5.analyse;
		auto p = ES6(replace);
		auto n = p.createNode();
		auto expr = n.getChild("ES6.Expression").asExpression();

		auto iter = getIterationStatement(js);
		iter.setCondition(expr);
		iter.toJS.shouldEqual(expected,file,line);
	}

	testSetCondition(
		`do { bla = 5; } while(true);`,
		`a`,
		`do bla = 5;while (a);`
	);
	testSetCondition(
		`while (true) { bla = 5; }`,
		`a`,
		`while(a){ bla = 5 }`
	);
	testSetCondition(
		`for (var k = 0; k < 10; k++) { bla = 5; }`,
		`a`,
		`for(var k = 0;a;k++){ bla = 5 }`
	);
	testSetCondition(
		`for (var k = 0; ; k++) { bla = 5; }`,
		`a`,
		`for(var k = 0;a;k++){ bla = 5 }`
	);
	testSetCondition(
		`for (k = 0; k < 10; k++) { bla = 5; }`,
		`a`,
		`for(k = 0;a;k++){ bla = 5 }`
	);
	testSetCondition(
		`for (; k < 10; k++) { bla = 5; }`,
		`a`,
		`for(;a;k++){ bla = 5 }`
	);
	testSetCondition(
		`for (e = 0; ; k++) { bla = 5; }`,
		`a`,
		`for(e = 0;a;k++){ bla = 5 }`
	);
	testSetCondition(
		`for (; ; k++) { bla = 5; }`,
		`a`,
		`for(;a;k++){ bla = 5 }`
	);

	void testSetConditionFails(string js, in string file = __FILE__, in size_t line = __LINE__)
	{
		testSetCondition(js,"a","",file,line).shouldThrow();
	}
	testSetConditionFails(`for (var k in obj) { bla = 5; }`);
	testSetConditionFails(`for (k in obj) { bla = 5; }`);
	//testSetConditionFails(`for (let k in obj) { bla = 5; }`);				// Wait for Issue #7 in es6-grammar
	//testSetConditionFails(`for (const k in obj) { bla = 5; }`);			// Wait for Issue #7 in es6-grammar
	testSetConditionFails(`for (var k of obj) { bla = 5; }`);
	//testSetConditionFails(`for (k of obj) { bla = 5; }`);					// Wait for Issue #8 in es6-grammar
	//testSetConditionFails(`for (let k of obj) { bla = 5; }`);				// Wait for Issue #7 in es6-grammar
	//testSetConditionFails(`for (const k of obj) { bla = 5; }`);			// Wait for Issue #7 in es6-grammar
	testSetConditionFails(`for (var [k,b] in obj) { l = 6 }`);
	testSetConditionFails(`for (var {k,b} in obj) { l = 6 }`);
	testSetConditionFails(`for ([k,b] in obj) { l = 6 }`);
	testSetConditionFails(`for ({k,b} in obj) { l = 6 }`);

	//testSetConditionFails(`for (let [k,b] in obj) { l = 6 }`);			// Wait for Issue 9 in es6-grammar
	//testSetConditionFails(`for (let [k,b] of obj) { l = 6 }`);			// Wait for Issue 9 in es6-grammar
	//testSetConditionFails(`for (let {k,b} in obj) { l = 6 }`);			// Wait for Issue 10 in es6-grammar
	//testSetConditionFails(`for (let {k,b} of obj) { l = 6 }`);			// Wait for Issue 10 in es6-grammar
	//testSetConditionFails(`for (const [k,b] in obj) { l = 6 }`);			// Wait for Issue 11 in es6-grammar
	//testSetConditionFails(`for (const [k,b] of obj) { l = 6 }`);			// Wait for Issue 11 in es6-grammar
	//testSetConditionFails(`for (const {k,b} in obj) { l = 6 }`);			// Wait for Issue 12 in es6-grammar
	//testSetConditionFails(`for (const {k,b} of obj) { l = 6 }`);			// Wait for Issue 12 in es6-grammar

	testSetCondition(
		`for (let c = 6, d; c < d; c++) { bla = 5; }`,
		`a`,
		`for(let c = 6, d; a;c++){ bla = 5 }`
	);
	testSetCondition(
		`for (const c = 6, d; c < d; c++) { bla = 5; }`,
		`a`,
		`for(const c = 6, d; a;c++){ bla = 5 }`
	);
}
struct Expression
{
	Node node;
	alias node this;
	bool isSingleExpression()
	{
		return node.children.length == 1;
	}
	bool isUnaryExpression()
	{
		return node.getNthChild(4).isA!"ES6.UnaryExpression";
	}
	bool isBinaryExpression()
	{
		return node.getNthChild(4).isA!"ES6.BinaryExpression";
	}
	Node getUnaryExpression()
	{
		auto e = node.getNthChild(4);
		assert(e.isA!"ES6.UnaryExpression");
		return e;
	}
	Node getAssignmentExpression()
	{
		auto a = node.children[0];
		assert(a.isA!"ES6.AssignmentExpression");
		return a;
	}
	bool isPartOfBinaryExpression()
	{
		if (!node.parent.isA!"ES6.Parentheses")
			return false;
		return node.getNthParent(9).isA!"ES6.BinaryExpression";
	}
	bool isPartOfConditionalExpression()
	{
		if (!node.parent.isA!"ES6.Parentheses")
			return false;
		auto cnd = node.getNthParent(10);
		return cnd.isA!"ES6.ConditionalExpression" && cnd.children.length > 1;
	}
	bool isConditionalExpression()
	{
		auto cnd = node.children[$-1].getNthChild(1);
		return cnd.isA!"ES6.ConditionalExpression" && cnd.children.length > 1;
	}
	Node getLastAssignmentExpression()
	{
		return node.children[$-1];
	}
	Nullable!Node getFirstPrefixExpression()
	{
		Node pref;
		if (isBinaryExpression)
			pref = node.getNthChild(6);
		else
			pref = node.getNthChild(5);
		Nullable!Node n;
		if (pref.isA!"ES6.PrefixExpression")
			n = pref;
		return n;
	}
	bool isParenthesedExpression()
	{
		if (isBinaryExpression)
			return node.getNthChild(11).isA!"ES6.CoverParenthesizedExpressionAndArrowParameterList";
		return node.getNthChild(10).isA!"ES6.CoverParenthesizedExpressionAndArrowParameterList";
	}
	bool isStringLiteral()
	{
		if (isBinaryExpression)
			return node.getNthChild(12).isA!"ES6.StringLiteral";
		return node.getNthChild(11).isA!"ES6.StringLiteral";
	}
}
auto asExpression(Node node)
{
	import std.typecons : Nullable;
	Nullable!Expression n;
	if (node.isA!"ES6.Expression")
		n = Expression(node);
	return n;
}
@("Expression")
unittest
{
	ES6(`6`).createNode.getChild("ES6.Expression").asExpression.isPartOfBinaryExpression.shouldBeFalse();
	ES6(`6`).createNode.getChild("ES6.Expression").asExpression.isPartOfConditionalExpression.shouldBeFalse();
}
struct TreeDiff
{
	enum Difference { Type, Children, Parent };
	Difference type;
	Node a, b;
}
bool equalNodesNames(Node a, Node b)
{
	if (a is null && b is null)
		return true;
	if (a is null || b is null)
		return false;
	import std.regex;
	auto ctr = ctRegex!(`^(.+?)(?:In|InYield|Yield|Return|InReturn|YieldReturn|Default|YieldDefault)?(?:!.*)?$`);
	auto aMatch = matchFirst(a.name, ctr);
	auto bMatch = matchFirst(b.name, ctr);
	assert(!aMatch.empty);
	assert(!bMatch.empty);
	return equal(aMatch[1],bMatch[1]);
}
import std.typecons : Nullable;
Nullable!TreeDiff getTreeDiff(Node a, Node b)
{
	import std.stdio : writeln, write;
	import std.range : zip;
	import std.algorithm : equal,until,filter;
	import std.array : array;
	auto diff = Nullable!TreeDiff();
	if (!equalNodesNames(a,b))
	{
		return Nullable!TreeDiff(TreeDiff(TreeDiff.Difference.Type,a,b));
	}
	auto ac = a.children.filter!(c=>!c.getNthChild(2).isA!"ES6.EmptyStatement").array;
	auto bc = b.children.filter!(c=>!c.getNthChild(2).isA!"ES6.EmptyStatement").array;

	if (ac.length != bc.length)
	{
		return Nullable!TreeDiff(TreeDiff(TreeDiff.Difference.Children,a,b));
	}
	if (!equalNodesNames(a.parent,b.parent))
	{
		return Nullable!TreeDiff(TreeDiff(TreeDiff.Difference.Parent,a,b));
	}
	foreach(cc; zip(ac,bc))
	{
		auto d = getTreeDiff(cc[0],cc[1]);
		if (!d.isNull)
			return d;
	}
	return diff;
}
version (unittest)
{
	import unit_threaded;
	import es5.testhelpers;
	void failTreeDiff(string msg, string js, TreeDiff diff, string opt, Node root, Node nodes, in string file = __FILE__, in size_t line = __LINE__)
	{
		Message m;
		m.writeln(msg);
		m.writeln("After `"~opt~"`");
		m.writeln(js);
		m.write("** Tree A: ");
		m.writeln(prettyPrintHighlight!((n){if (n is diff.a) return "**"~n.name; return "  "~n.name;})(root));
		m.write("** Tree B: ");
		m.writeln(prettyPrintHighlight!((n){if (n is diff.b) return "**"~n.name; return "  "~n.name;})(nodes));
		failed(m,file,line);
	}
	string getDiffErrorMessage(TreeDiff diff)
	{
		import std.conv : to;
		final switch (diff.type)
		{
			case TreeDiff.Difference.Type:
				return "Tree a has node `"~diff.a.name~"`, but Tree b has node `"~diff.b.name~"`";
			case TreeDiff.Difference.Children:
				return "Tree a has node `"~diff.a.name~"` with "~diff.a.children.length.to!string~", but Tree b has node `"~diff.b.name~"` with "~diff.b.children.length.to!string~" children";
			case TreeDiff.Difference.Parent:
				string parentAName = diff.a.parent is null ? "null" : diff.a.parent.name;
				string parentBName = diff.b.parent is null ? "null" : diff.b.parent.name;
				return "Tree a has node `"~diff.a.name~"` with parent "~parentAName~", but Tree b has node `"~diff.b.name~"` with parent "~parentBName;
		}
	}
	void assertTree(Node a, string opt, in string file = __FILE__, in size_t line = __LINE__)
	{
		import std.stdio : writeln, write;
		import es5.tojs;
		import es6.grammar;
		auto root = a.getRoot();
		auto js = root.toJS;
		if (js.length == 0)
			return;
		auto p = ES6(js);
		if (!p.successful)
		{
			Message m;
			m.writeln(root);
			m.writeln("couldn't parse After `"~opt~"`");
			m.writeln(js);
			failed(m,file,line);
		}
		auto nodes = createNode(p);
		auto diff = getTreeDiff(root,nodes);
		if (diff.isNull)
			return;

		failTreeDiff(diff.getDiffErrorMessage,js,diff,opt,root,nodes,file,line);
	}
	auto createTestNodes(string js = `var a = 6`)
	{
		import es6.grammar;
		auto p = ES6(js);
		return createNode(p);
	}
	@("Checking assertTree not failing on proper tree")
	unittest
	{
		assertTree(createTestNodes,"proper tree");
	}
	@("Checking assertTree not failing on empty tree")
	unittest
	{
		auto nodes = createTestNodes(";");
		nodes.children.length = 0;
		nodes.matches = [];
		assertTree(nodes,"empty tree");
	}
	@ShouldFail @("Malformed tree with invalid syntax")
	unittest
	{
		auto nodes = createTestNodes();
		nodes.getChild("ES6.IdentifierName").matches[0] = "var";
		assertTree(nodes,"malformed tree");
	}
	@ShouldFail @("Malformed tree with missing intermediate node")
	unittest
	{
		auto nodes = createTestNodes();
		auto mExpr = nodes.getChild("ES6.MemberExpression");
		mExpr.setChild(0,mExpr.getNthChild(2));
		assertTree(nodes,"malformed tree");
	}
	@ShouldFail @("Malformed tree with parent null pointer")
	unittest
	{
		auto nodes = createTestNodes();
		auto mExpr = nodes.getChild("ES6.MemberExpression");
		mExpr.parent = null;
		assertTree(nodes,"malformed tree");
	}
	@ShouldFail @("Malformed tree with extra node")
	unittest
	{
		auto nodes = createTestNodes();
		auto mExpr = nodes.getChild("ES6.MemberExpression");
		mExpr.addChild(new Node(ParseTree("ES6.Literal",true)));
		assertTree(nodes,"malformed tree");
	}
}
Node createAssignmentExpressionFromUnaryExpression(Node un)
{
	assert(un.isA!"ES6.UnaryExpression");
	return new Node(ParseTree("ES6.AssignmentExpressionIn",true),
		[	new Node(ParseTree("ES6.ConditionalExpressionIn",true),
			[	new Node(ParseTree("ES6.RightHandSideExpressionIn",true),[un])
	])]);
}
Node createLHSExpressionFromPrimaryExpressionChild(Node child)
{
	return new Node(ParseTree("ES6.LeftHandSideExpression",true),
			[
			new Node(ParseTree("ES6.NewExpression",true),
			[
				new Node(ParseTree("ES6.MemberExpression",true),
				[
					new Node(ParseTree("ES6.PrimaryExpression",true),[child])
	])])]);
}
void getNonLocalIdentifiers(Sink)(Scope scp, Sink sink)
{
	import std.algorithm : filter, map, remove;
	foreach(uid; scp.identifiers.filter!(id=>!scp.hasVariable(id)))
		sink.put(uid);
	auto p1 = sink.data.length;
	foreach (s; scp.scopes)
		getNonLocalIdentifiers(s,sink);
	auto p2 = sink.data.length;
	for (auto idx = p1; idx < p2;)
	{
		if (!scp.hasVariable(sink.data[idx]))
			++idx;
		else
		{
			sink.data.remove(idx);
			--p2;
		}
	}
}
auto getNonLocalIdentifiers(Scope scp)
{
	import std.array : appender;
	auto app = appender!(string[])();
	getNonLocalIdentifiers(scp,app);
	return app.data;
}
auto getNonLocalIdentifiers(Scope[] scps)
{
	import std.array : appender;
	auto app = appender!(string[])();
	foreach(scp; scps)
		getNonLocalIdentifiers(scp,app);
	return app.data;
}
bool sameTree(Node a, Node b)
{
	if (a.children.length != b.children.length)
		return false;
	if (!equalNodesNames(a,b))
		return false;
	if ((a.isA!"ES6.PostfixOperator" ||
		a.isA!"ES6.PrefixExpression" ||
		a.isA!"ES6.ExpressionOperator" ||
		a.isA!"ES6.AssignmentOperator" ||
		a.isA!"ES6.IdentifierReference" ||
		a.isA!"ES6.IdentifierName" ||
		a.isA!"ES6.StringLiteral" ||
		a.isA!"ES6.BooleanLiteral" ||
		a.isA!"ES6.NumericLiteral") && a.matches[0] != b.matches[0])
		return false;
	import std.range : zip;
	foreach(cs; a.children.zip(b.children))
		if (!sameTree(cs[0],cs[1]))
			return false;
	return true;
}
unittest{
	sameTree(ES6(`abc`).createNode,ES6(`var b`).createNode).shouldBeFalse();
	sameTree(ES6(`abc`).createNode,ES6(`123`).createNode).shouldBeFalse();
	sameTree(ES6(`abc;def;`).createNode,ES6(`123`).createNode).shouldBeFalse();
}
string getFreeIdentifierForAllScopesInScope(Scopes)(Scope master, Scopes children)
{
	import std.algorithm : canFind;
	auto nonLocalIdentifiers = master.globals;//getNonLocalIdentifiers();
	//auto usedIdentifiers = nonLocalIdentifiers;//.map!(id=>getShortenedVariableName(master,id));
	// first of all the identifier cannot be a unresolvedIdentifier (note: unless that identifier doesn't get used in the all scopes where we want to inject this shortenedStringLiteral)
	// then, the identifier cannot mask any variable in the scopeTree for each identifier used
	bool masksAUsedVariable(string id)
	{
		bool isIdentifierFree(Scope scp, string id)
		{
			if (scp.variables.canFind!"a.name == b"(id))
				return false;
			if (scp.parent is master)
				return true;
			if (scp.parent !is null)
				return isIdentifierFree(scp.parent,id);
			return true;
		}
		foreach (sc; children)
		{
			if (!isIdentifierFree(sc,id))
				return true;
		}
		return false;
	}
	int nameIdx = 0;
	/*if (master.nextFreeIdentifier != -1)
		nameIdx = master.nextFreeIdentifier;*/
	string name;
	do
	{
		name = generateName(nameIdx++);
	} while(!name.isValidIdentifier /*|| usedIdentifiers.canFind(name)*/ || nonLocalIdentifiers.canFind(name) || masksAUsedVariable(name) || master.variables.canFind!("a.name == b")(name));
	//master.nextFreeIdentifier = nameIdx;
	return name;
}
@("getFreeIdentifierForAllScopesInScope")
unittest
{
	Scope getScopes(string js)
	{	
		auto p = ES6(js);
		auto nodes = createNode(p);
		nodes.moveVariableAndFunctionDeclarationsUpFront;
		auto a = analyseNodes(nodes);
		return a.scp;
	}
	void assertNextFreeIdentifier(Scope s, string got, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import es5.tojs;
		if (expected == got)
			return;
		Message m;
		m.write("Expected free identifier ");
		m.write(expected);
		m.write(", got identifier ");
		m.write(got);
		m.write(", in ");
		m.writeln(s.getVariables);
		m.writeln(s.entry.toJS());
		failed(m,file,line);
	}
	auto ss = getScopes(`var e,a,t; function n() { var e,a; } function o() { var e; }`);
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0]]),"x").shouldThrow;
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0],ss.scopes[1]]),"r");
	ss = getScopes(`function n() { var e,a; } function o() { var e; }`);
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0],ss.scopes[1]]),"t");
	ss = getScopes(`function a() { function t() { var e,a; } var e,a; } function o() { function a() { var e,a; }var e; }`);
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0].scopes[0],ss.scopes[1].scopes[0]]),"n");
	ss = getScopes(`function n() { function t() { var e,a; } var e,a; return r*7 } function o() { function a() { var e,a; } var e; }`);
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0].scopes[0],ss.scopes[1].scopes[0]]),"c");
	ss = getScopes(`function n() { function t() { var e,a; } var e,a; return r*7 } function o() { function a() { var e,a; } var e; return c*8 }`);
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0].scopes[0],ss.scopes[1].scopes[0]]),"i");
	ss = getScopes(`var e = "habbahabba"; (function(){ function e() { return "habbahabba"; } if (n == "habbahabba") a = "habbahabba"; })(); (function(){ function e() { return "habbahabba"; } if (n == "habbahabba") a = "habbahabba"; })()`);
	assertNextFreeIdentifier(ss,ss.getFreeIdentifierForAllScopesInScope([ss.scopes[0],ss.scopes[0].scopes[0],ss.scopes[1],ss.scopes[1].scopes[0]]),"t");

}
auto getStatementListFromBlockStatement(Node blk)
{
	return blk.getNthChild(2);
}
auto createSquareBracketAccessor(string id)
{
	return new Node(ParseTree("ES6.SquareBrackets",true),[
		new Node(ParseTree("ES6.ExpressionIn",true),[
			createAssignmentExpressionFromUnaryExpression(
				createUnaryExpressionPrimaryExpression(
					createIdentifierReference(id)
				)
			)
		])
	]);
}