module es5.eval;

import es5.node;
import es5.testhelpers;

version (unittest)
{
	import unit_threaded;
	import es5.testhelpers;
	import es6.grammar;
}

enum ValueType
{
	NotKnownAtCompileTime,
	Undefined,
	NaN,
	Infinity,
	Null,
	Bool,
	String,
	Numeric,
	Object // arent using it right now.
}
struct RawValue
{
	string value;
	ValueType type;
}
auto processPrefixExpressions(RawValue raw, Node[] prefixs)
{
	import std.range : retro;
	import std.conv : to;
	foreach (pref; prefixs.retro)
	{
		switch (pref.matches[0])
		{
			case "void": raw.type = ValueType.Undefined; raw.value = "undefined"; break;
			case "+":
				return raw.toNumber();
			case "-":
				final switch (raw.type)
				{
					case ValueType.Undefined:
					case ValueType.NaN: raw.type = ValueType.NaN; raw.value = "NaN"; break;
					case ValueType.Infinity: raw.value = "-Infinity"; break;
					case ValueType.Null: raw.type = ValueType.Numeric; raw.value = "-0"; break;
					case ValueType.Bool: raw.type = ValueType.Numeric; raw.value = raw.value == "true" ? "-1" : "-0"; break;
					case ValueType.String:
						raw.type = ValueType.Numeric;
						if (raw.value.length == 2)
						{
							raw.value = "-0";
						} else
						{
							try
							{
								int t = raw.value[1..$-1].to!int;
								if (raw.value[1] == '-')
									raw.value = (-t).to!string;
								else
									raw.value = "-"~t.to!string;
							} catch (Exception e)
							{
								raw.type = ValueType.NaN;
								raw.value = "NaN";
							}
						}
						break;
					case ValueType.Numeric: raw.value = "-"~raw.value; break;
					case ValueType.NotKnownAtCompileTime: break;
					case ValueType.Object: raw.value = "NaN"; raw.type = ValueType.NaN; break;
				}
				break;
			case "typeof":
				final switch (raw.type)
				{
					case ValueType.Undefined: raw.type = ValueType.String; raw.value = `"undefined"`; break;
					case ValueType.NaN: raw.type = ValueType.String; raw.value = `"number"`; break;
					case ValueType.Infinity: raw.type = ValueType.String; raw.value = `"number"`; break;
					case ValueType.Null: raw.type = ValueType.String; raw.value = `"object"`; break;
					case ValueType.Bool: raw.type = ValueType.String; raw.value = `"boolean"`; break;
					case ValueType.String: raw.type = ValueType.String; raw.value = `"string"`; break;
					case ValueType.Numeric: raw.type = ValueType.String; raw.value = `"number"`; break;
					case ValueType.NotKnownAtCompileTime: break;
					case ValueType.Object: raw.value = `"object"`; raw.type = ValueType.String; break;
				}
				break;
			case "delete":
				final switch (raw.type)
				{
					case ValueType.Undefined:
					case ValueType.NaN:
					case ValueType.Infinity: raw.type = ValueType.Bool; raw.value = "false"; break;
					case ValueType.Object:
					case ValueType.Null:
					case ValueType.Bool:
					case ValueType.String:
					case ValueType.Numeric: raw.type = ValueType.Bool; raw.value = "true"; break;
					case ValueType.NotKnownAtCompileTime: break;
				}
				break;
			case "~":
				final switch (raw.type)
				{
					case ValueType.Object:
					case ValueType.Undefined:
					case ValueType.NaN:
					case ValueType.Infinity:
					case ValueType.Null: raw.type = ValueType.Numeric; raw.value = "-1"; break;
					case ValueType.Bool: raw.type = ValueType.Numeric; raw.value = raw.value == "true" ? "-2" : "-1"; break;
					case ValueType.String:
						raw.type = ValueType.Numeric;
						try
						{
							auto t = raw.value[1..$-1].to!int + 1;
							t = -t;
							raw.value = t.to!string;
						} catch (Exception e)
						{
							raw.value = "-1";
						}
						break;
					case ValueType.Numeric: raw.type = ValueType.Numeric; raw.value = (-(raw.value.to!int + 1)).to!string; break;
					case ValueType.NotKnownAtCompileTime: break;
				}
				break;
			case "!":
				final switch (raw.type)
				{
					case ValueType.Object:
					case ValueType.Infinity: raw.type = ValueType.Bool; raw.value = "false"; break;
					case ValueType.Undefined:
					case ValueType.NaN:
					case ValueType.Null: raw.type = ValueType.Bool; raw.value = "true"; break;
					case ValueType.Bool: raw.type = ValueType.Bool; raw.value = raw.value == "true" ? "false" : "true"; break;
					case ValueType.String: raw.type = ValueType.Bool; raw.value = raw.value.length == 2 ? "true" : "false"; break;
					case ValueType.Numeric: raw.type = ValueType.Bool; raw.value = (raw.value == "0" || raw.value == "-0") ? "true" : "false"; break;
					case ValueType.NotKnownAtCompileTime: break;
				}
				break;
			default:
		}
	}
	return raw;
}
version (unittest)
{
	@("Test UnaryExpression evaluation")
	unittest
	{
		assertEvalUnaryExpression(`true`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`false`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`0`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`""`,ValueType.String,`""`);
		assertEvalUnaryExpression(`null`,ValueType.Null,`null`);
		assertEvalUnaryExpression(`NaN`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`undefined`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`Infinity`,ValueType.Infinity,`Infinity`);

		assertEvalUnaryExpression(`!true`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!false`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`!0`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`!-1`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!1`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!""`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`!"0"`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!"1"`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!"-1"`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!"asdf"`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!null`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`!NaN`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`!undefined`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`!Infinity`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`!a`,ValueType.NotKnownAtCompileTime,"a");
		assertEvalUnaryExpression(`!{}`,ValueType.Bool,"false");

		assertEvalUnaryExpression(`~true`,ValueType.Numeric,`-2`);
		assertEvalUnaryExpression(`~false`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~0`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~-1`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`~1`,ValueType.Numeric,`-2`);
		assertEvalUnaryExpression(`~""`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~"0"`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~"1"`,ValueType.Numeric,`-2`);
		assertEvalUnaryExpression(`~"-1"`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`~"asdf"`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~null`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~NaN`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~undefined`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~Infinity`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`~a`,ValueType.NotKnownAtCompileTime,"a");
		assertEvalUnaryExpression(`~{}`,ValueType.Numeric,"-1");

		assertEvalUnaryExpression(`-true`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`-false`,ValueType.Numeric,`-0`);
		assertEvalUnaryExpression(`-0`,ValueType.Numeric,`-0`);
		assertEvalUnaryExpression(`-1`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`-""`,ValueType.Numeric,`-0`);
		assertEvalUnaryExpression(`-"0"`,ValueType.Numeric,`-0`);
		assertEvalUnaryExpression(`-"1"`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`-"-1"`,ValueType.Numeric,`1`);
		assertEvalUnaryExpression(`-"--1"`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`-"+1"`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`-"++1"`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`-"asdf"`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`-null`,ValueType.Numeric,`-0`);
		assertEvalUnaryExpression(`-NaN`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`-undefined`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`-Infinity`,ValueType.Infinity,`-Infinity`);
		assertEvalUnaryExpression(`-a`,ValueType.NotKnownAtCompileTime,"a");
		assertEvalUnaryExpression(`-{}`,ValueType.NaN,"NaN");

		assertEvalUnaryExpression(`+true`,ValueType.Numeric,`1`);
		assertEvalUnaryExpression(`+false`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`+0`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`+-1`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`+1`,ValueType.Numeric,`1`);
		assertEvalUnaryExpression(`+""`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`+"0"`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`+"1"`,ValueType.Numeric,`1`);
		assertEvalUnaryExpression(`+"-1"`,ValueType.Numeric,`-1`);
		assertEvalUnaryExpression(`+"--1"`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`+"+1"`,ValueType.Numeric,`1`);
		assertEvalUnaryExpression(`+"++1"`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`+"asdf"`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`+null`,ValueType.Numeric,`0`);
		assertEvalUnaryExpression(`+NaN`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`+undefined`,ValueType.NaN,`NaN`);
		assertEvalUnaryExpression(`+Infinity`,ValueType.Infinity,`Infinity`);
		assertEvalUnaryExpression(`+a`,ValueType.NotKnownAtCompileTime,"a");
		assertEvalUnaryExpression(`+{}`,ValueType.NaN,"NaN").shouldThrow();

		assertEvalUnaryExpression(`typeof true`,ValueType.String,`"boolean"`);
		assertEvalUnaryExpression(`typeof false`,ValueType.String,`"boolean"`);
		assertEvalUnaryExpression(`typeof 0`,ValueType.String,`"number"`);
		assertEvalUnaryExpression(`typeof -1`,ValueType.String,`"number"`);
		assertEvalUnaryExpression(`typeof 1`,ValueType.String,`"number"`);
		assertEvalUnaryExpression(`typeof ""`,ValueType.String,`"string"`);
		assertEvalUnaryExpression(`typeof "0"`,ValueType.String,`"string"`);
		assertEvalUnaryExpression(`typeof "1"`,ValueType.String,`"string"`);
		assertEvalUnaryExpression(`typeof "-1"`,ValueType.String,`"string"`);
		assertEvalUnaryExpression(`typeof "asdf"`,ValueType.String,`"string"`);
		assertEvalUnaryExpression(`typeof null`,ValueType.String,`"object"`);
		assertEvalUnaryExpression(`typeof NaN`,ValueType.String,`"number"`);
		assertEvalUnaryExpression(`typeof undefined`,ValueType.String,`"undefined"`);
		assertEvalUnaryExpression(`typeof Infinity`,ValueType.String,`"number"`);
		assertEvalUnaryExpression(`typeof a`,ValueType.NotKnownAtCompileTime,"a");
		assertEvalUnaryExpression(`typeof {}`,ValueType.String,`"object"`);

		assertEvalUnaryExpression(`void true`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void false`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void 0`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void 1`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void ""`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void "0"`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void "1"`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void "-1"`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void "asdf"`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void null`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void NaN`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void undefined`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void Infinity`,ValueType.Undefined,`undefined`);
		assertEvalUnaryExpression(`void a`,ValueType.Undefined,"undefined");
		assertEvalUnaryExpression(`void {}`,ValueType.Undefined,`undefined`);

		assertEvalUnaryExpression(`delete true`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete false`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete 0`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete -1`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete 1`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete ""`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete "0"`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete "1"`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete "-1"`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete "asdf"`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete null`,ValueType.Bool,`true`);
		assertEvalUnaryExpression(`delete NaN`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`delete undefined`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`delete Infinity`,ValueType.Bool,`false`);
		assertEvalUnaryExpression(`delete a`,ValueType.NotKnownAtCompileTime,"a");
		assertEvalUnaryExpression(`delete {}`,ValueType.Bool,`true`);
	}
}
auto getLiteralNodeValueType(Node node)
{
	switch (node.name)
	{
		case "ES6.NullLiteral": return ValueType.Null;
		case "ES6.BooleanLiteral": return ValueType.Bool;
		case "ES6.StringLiteral": return ValueType.String;
		case "ES6.NumericLiteral": return ValueType.Numeric;
		case "ES6.Identifier":
			switch (node.matches[0])
			{
				case "NaN": return ValueType.NaN;
				case "undefined": return ValueType.Undefined;
				case "Infinity": return ValueType.Infinity;
				default: return ValueType.NotKnownAtCompileTime;
			}
		default: return ValueType.NotKnownAtCompileTime;
	}
}
unittest{
	import es6.grammar;
	assert((new Node(ParseTree("abc"))).getLiteralNodeValueType == ValueType.NotKnownAtCompileTime);
}
bool coerceAsBoolean(RawValue raw)
{
	final switch (raw.type)
	{
		case ValueType.Undefined: return false;
		case ValueType.NaN: return false;
		case ValueType.Infinity: return true;
		case ValueType.Null: return false;
		case ValueType.Bool: return raw.value == "true" ? true : false;
		case ValueType.String: return raw.value.length == 2 ? false : true;
		case ValueType.Numeric: return (raw.value == "0" || raw.value == "-0") ? false : true;
		case ValueType.Object:
		case ValueType.NotKnownAtCompileTime: unitAssert("Not supported"); assert(0);
	}
}
unittest
{
	import unit_threaded;
	RawValue("",ValueType.Object).coerceAsBoolean.shouldThrow();
}
RawValue toNumber(RawValue raw)
{
	import std.conv : to;
	final switch (raw.type)
	{
		case ValueType.NotKnownAtCompileTime:
		case ValueType.Numeric:
		case ValueType.NaN:
		case ValueType.Infinity: break;
		case ValueType.Undefined: raw.type = ValueType.NaN; raw.value = "NaN"; break;
		case ValueType.Null: raw.type = ValueType.Numeric; raw.value = "0"; break;
		case ValueType.Bool: raw.type = ValueType.Numeric; raw.value = raw.value == "true" ? "1" : "0"; break;
		case ValueType.String:
			raw.type = ValueType.Numeric;
			if (raw.value.length == 2)
				raw.value = "0";
			else
			{
				try
				{
					int t = raw.value[1..$-1].to!int;
					raw.value = t.to!string;
				} catch (Exception e)
				{
					raw.type = ValueType.NaN;
					raw.value = "NaN";
				}
			}
			break;
		case ValueType.Object: return raw.toPrimitive.toNumber;
	}
	return raw;
}
auto toPrimitive(RawValue raw)
{
	if (raw.type != ValueType.Object)
		return raw;
	//  if valueOf returns a primitive, return it. Otherwise if toString returns a primitive return it. Otherwise throw an error
	unitAssert("toPrimitive is not supported"); assert(0);
}
@("toPrimitive identity feature")
unittest
{
	RawValue("Test",ValueType.Undefined).toPrimitive.shouldEqual(RawValue("Test",ValueType.Undefined));
}
bool doStrictEquality(alias type = false)(RawValue a, RawValue b, bool invert)
{
	bool r = doStrictEquality!type(a,b);
	if (invert)
		return !r;
	return r;
}
bool doStrictEquality(alias type = false)(RawValue a, RawValue b)
{
	// NOTE: can be replaced with `import std.traits : isExpressions` when gdc/ldc support it
	template isExpressions(T ...)
	{
		static if (T.length >= 2)
			enum bool isExpressions =
				isExpressions!(T[0 .. $/2]) &&
				isExpressions!(T[$/2 .. $]);
		else static if (T.length == 1)
			enum bool isExpressions =
				!is(T[0]) && __traits(compiles, { auto ex = T[0]; });
		else
			enum bool isExpressions = true; // default
	}
	import std.traits : isBoolean;
	if (a.type == ValueType.Undefined || a.type == ValueType.Null)
		return (b.type == ValueType.Undefined || b.type == ValueType.Null);
	if (a.type != b.type || a.type == ValueType.NaN || b.type == ValueType.NaN)
		return false;
	static if (isExpressions!(type) && isBoolean!(typeof(type)) && type == false)
	{
		switch (a.type)
		{
			case ValueType.Numeric:
			case ValueType.String:
			case ValueType.Bool:
				return a.value == b.value;
			case ValueType.Object:
			default: unitAssert("Not supported"); assert(0,"Not supported"); // Note: the equality is true when a and b reference the same object, but we don't support it now
		}
	} else
	{
		assert(a.type == b.type);
		assert(b.type == b.type);
		static if (type == ValueType.Numeric || type == ValueType.String || type == ValueType.Bool)
		{
			return a.value == b.value;
		} else static if (type == ValueType.Object)
		{
			static assert(false, "Not supported"); // Note: the equality is true when a and b reference the same object, but we don't support it now
		} else
			return false;
	}
}
@("doStrictEquality")
unittest
{
	doStrictEquality(RawValue("",ValueType.Object),RawValue("",ValueType.Object)).shouldThrow;
}
bool doWeakEquality(RawValue a, RawValue b, bool invert)
{
	bool r = doWeakEquality(a,b);
	if (invert)
		return !r;
	return r;
}
bool doWeakEquality(RawValue a, RawValue b)
{
	if (a.type == b.type)
		return doStrictEquality(a,b);
	if (a.type == ValueType.Null || a.type == ValueType.Undefined)
		return (b.type == ValueType.Undefined || b.type == ValueType.Null);
	if (b.type == ValueType.Bool)
		return doStrictEquality!(ValueType.Numeric)(a,b.toNumber);
	if (a.type == ValueType.Numeric)
	{
		if (b.type == ValueType.String)
			return doStrictEquality!(ValueType.Numeric)(a,b.toNumber);
		else if (b.type == ValueType.Object)
			return doStrictEquality!(ValueType.Numeric)(a,b.toPrimitive);
	} else if (a.type == ValueType.String)
	{
		if (b.type == ValueType.Numeric)
			return doStrictEquality!(ValueType.Numeric)(a.toNumber,b);
		else if (b.type == ValueType.Object)
			return doStrictEquality!(ValueType.String)(a,b.toPrimitive);
	} else if (a.type == ValueType.Bool)
		return doWeakEquality(a.toNumber,b);
	else if (a.type == ValueType.Object)
	{
		if (b.type == ValueType.String)
			return doStrictEquality!(ValueType.String)(b,a.toPrimitive);
		else if (b.type == ValueType.Numeric)
			return doStrictEquality!(ValueType.Numeric)(b,a.toPrimitive);
	}
	return false;
}
version (unittest)
{
	@("Test WeakEquality with objects")
	unittest
	{
		doWeakEquality(RawValue("{}",ValueType.Object),RawValue(`"str"`,ValueType.String)).shouldThrow;
		doWeakEquality(RawValue(`"str"`,ValueType.String),RawValue("{}",ValueType.Object)).shouldThrow;
		doWeakEquality(RawValue("{}",ValueType.Object),RawValue(`6`,ValueType.Numeric)).shouldThrow;
		doWeakEquality(RawValue(`6`,ValueType.Numeric),RawValue("{}",ValueType.Object)).shouldThrow;
		doWeakEquality(RawValue(`6`,ValueType.Numeric),RawValue("{}",ValueType.NotKnownAtCompileTime)).shouldEqual(false);
		doWeakEquality(RawValue("{}",ValueType.NotKnownAtCompileTime),RawValue(`6`,ValueType.Numeric)).shouldEqual(false);
	}
	unittest
	{
		void assertBinaryExpression(string input, bool value)
		{
			if (value)
				assertEvalBinaryExpression(input,ValueType.Bool,"true");
			else
				assertEvalBinaryExpression(input,ValueType.Bool,"false");
		}
		assertBinaryExpression(`1 == 1`, true);
		assertBinaryExpression(`null == null`, true);
		assertBinaryExpression(`undefined == null`, true);
		assertBinaryExpression(`null == undefined`, true);
		assertBinaryExpression(`1 == "1"`, true);
		assertBinaryExpression(`1 == "0"`, false);
		assertBinaryExpression(`1 == "asdf"`, false);
		assertBinaryExpression(`"1" == 1`, true);
		assertBinaryExpression(`"0" == 1`, false);
		assertBinaryExpression(`"asdf" == 1`, false);
		assertBinaryExpression(`-1 == "-1"`, true);
		assertBinaryExpression(`"-1" == -1`, true);
		assertBinaryExpression(`true == 1`, true);
		assertBinaryExpression(`1 == true`, true);
		assertBinaryExpression(`false == 0`, true);
		assertBinaryExpression(`0 == false`, true);
		assertBinaryExpression(`true == 2`, false);
		assertBinaryExpression(`2 == true`, false);
		assertBinaryExpression(`false == 2`, false);
		assertBinaryExpression(`2 == false`, false);
		assertBinaryExpression(`1 != 1`, false);
		assertBinaryExpression(`null != null`, false);
		assertBinaryExpression(`undefined != null`, false);
		assertBinaryExpression(`null != undefined`, false);
		assertBinaryExpression(`1 != "1"`, false);
		assertBinaryExpression(`1 != "0"`, true);
		assertBinaryExpression(`1 != "asdf"`, true);
		assertBinaryExpression(`"1" != 1`, false);
		assertBinaryExpression(`"0" != 1`, true);
		assertBinaryExpression(`"asdf" != 1`, true);
		assertBinaryExpression(`-1 != "-1"`, false);
		assertBinaryExpression(`"-1" != -1`, false);
		assertBinaryExpression(`true != 1`, false);
		assertBinaryExpression(`1 != true`, false);
		assertBinaryExpression(`false != 0`, false);
		assertBinaryExpression(`0 != false`, false);
		assertBinaryExpression(`true != 2`, true);
		assertBinaryExpression(`2 != true`, true);
		assertBinaryExpression(`false != 2`, true);
		assertBinaryExpression(`2 != false`, true);

		assertBinaryExpression(`2 === false`, false);
		assertBinaryExpression(`"2" === false`, false);
		assertBinaryExpression(`true === 2`, false);
		assertBinaryExpression(`true === "2"`, false);
		assertBinaryExpression(`null === undefined`, true);
		assertBinaryExpression(`undefined === null`, true);
		assertBinaryExpression(`2 === 5`, false);
		assertBinaryExpression(`5 === 2`, false);
		assertBinaryExpression(`2 === 2`, true);
		assertBinaryExpression(`-2 === -2`, true);
		assertBinaryExpression(`"abc" === "def"`, false);
		assertBinaryExpression(`"def" === "abc"`, false);
		assertBinaryExpression(`"abc" === "abc"`, true);
		assertBinaryExpression(`true === false`, false);
		assertBinaryExpression(`false === true`, false);
		assertBinaryExpression(`true === true`, true);
		assertBinaryExpression(`false === false`, true);
		assertBinaryExpression(`2 !== false`, true);
		assertBinaryExpression(`"2" !== false`, true);
		assertBinaryExpression(`true !== 2`, true);
		assertBinaryExpression(`true !== "2"`, true);
		assertBinaryExpression(`null !== undefined`, false);
		assertBinaryExpression(`undefined !== null`, false);
		assertBinaryExpression(`2 !== 5`, true);
		assertBinaryExpression(`5 !== 2`, true);
		assertBinaryExpression(`2 !== 2`, false);
		assertBinaryExpression(`-2 !== -2`, false);
		assertBinaryExpression(`"abc" !== "def"`, true);
		assertBinaryExpression(`"def" !== "abc"`, true);
		assertBinaryExpression(`"abc" !== "abc"`, false);
		assertBinaryExpression(`true !== false`, true);
		assertBinaryExpression(`false !== true`, true);
		assertBinaryExpression(`true !== true`, false);
		assertBinaryExpression(`false !== false`, false);

		assertEvalBinaryExpression(`false || true`, ValueType.Bool, "true");
		assertEvalBinaryExpression(`true || false`, ValueType.Bool, "true");
		assertEvalBinaryExpression(`false && true`, ValueType.Bool, "false");
		assertEvalBinaryExpression(`true && false`, ValueType.Bool, "false");
		assertEvalBinaryExpression(`false || NaN`, ValueType.NaN, "NaN");
		assertEvalBinaryExpression(`NaN || false`, ValueType.Bool, "false");
		assertEvalBinaryExpression(`false || Infinity`, ValueType.Infinity, "Infinity");
		assertEvalBinaryExpression(`Infinity || false`, ValueType.Infinity, "Infinity");

		assertEvalBinaryExpression(`false || "str"`, ValueType.String, `"str"`);
		assertEvalBinaryExpression(`true || "str"`, ValueType.Bool, "true");
		assertEvalBinaryExpression(`false && "str"`, ValueType.Bool, "false");
		assertEvalBinaryExpression(`true && "str"`, ValueType.String, `"str"`);

		assertEvalBinaryExpression(`NaN | 4`, ValueType.NaN, "NaN");
		assertEvalBinaryExpression(`1 | NaN`, ValueType.NaN, "NaN");
		assertEvalBinaryExpression(`1 | 4`, ValueType.Numeric, "5");
		assertEvalBinaryExpression(`2 & 10`, ValueType.Numeric, "2");
		assertEvalBinaryExpression(`17 ^ 3`, ValueType.Numeric, "18");

		assertEvalBinaryExpression(`NaN >> 3`, ValueType.Numeric, "0");
		assertEvalBinaryExpression(`3 >> NaN`, ValueType.Numeric, "3");
		assertEvalBinaryExpression(`NaN * 3`, ValueType.NaN, "NaN");
		assertEvalBinaryExpression(`3 * NaN`, ValueType.NaN, "NaN");
	}
}
auto toRawValue(bool v)
{
	return v ? RawValue("true",ValueType.Bool) : RawValue("false",ValueType.Bool);
}
auto coerceToDoubleQuotedString(string str)
{
	if (str.length == 0)
		return str;
	if (str[0] == '"')
		return str[1..$-1];
	import std.regex;
	auto escapedSingle = ctRegex!(`\\'`);
	auto unescapedDouble = ctRegex!(`(?<!\\)"`);
	str = str[1..$-1];
	//unescape '
	str = replaceAll(str,escapedSingle,"'");
	//escape "
	str = replaceAll(str,unescapedDouble,"\\\"");
	return str;
}
unittest
{
	assert(`""`.coerceToDoubleQuotedString == "");
	assert("".coerceToDoubleQuotedString == "");
	assert(`'isn\'t it nice'`.coerceToDoubleQuotedString == `isn't it nice`);
	assert(`'a "good" way to go'`.coerceToDoubleQuotedString == `a \"good\" way to go`);
}
auto doOperator(RawValue a, RawValue b, Node opExpr)
{
	import std.conv : to;
	import std.algorithm : startsWith;
	assert(opExpr.isA!"ES6.ExpressionOperator");
	if (a.type == ValueType.NotKnownAtCompileTime)
		return a;
	if (b.type == ValueType.NotKnownAtCompileTime)
		return b;
	auto op = opExpr.children[0];
	switch (op.name)
	{
		case "ES6.LogicalOperator":
			if (op.matches[0][0] == '|')
			{
				if (a.coerceAsBoolean())
					return a;
				return b;
			} else
			{
				if (a.coerceAsBoolean())
					return b;
				return a;
			}
		case "ES6.BitwiseOperator":
			a = a.toNumber;
			b = b.toNumber;
			if (a.type == ValueType.NaN)
				return a;
			if (b.type == ValueType.NaN)
				return b;
			try
			{
				if (op.matches[0][0] == '|')
					return RawValue((a.value.to!int | b.value.to!int).to!string,ValueType.Numeric);
				else if (op.matches[0][0] == '&')
					return RawValue((a.value.to!int & b.value.to!int).to!string,ValueType.Numeric);
				else
					return RawValue((a.value.to!int ^ b.value.to!int).to!string,ValueType.Numeric);
			} catch (Exception e)
			{
				return RawValue("NaN",ValueType.NaN);
			}
		case "ES6.EqualityOperator":
			bool invert = false;
			if (op.matches[0][0] == '!')
				invert = true;
			if (op.matches[0].length > 2 && op.matches[0][2] == '=')
				return doStrictEquality(a,b,invert).toRawValue;
			return doWeakEquality(a,b,invert).toRawValue;
		case "ES6.ShiftOperator":
			/*if (op.matches[0].startsWith(">>>"))
				return RawValue("",ValueType.NotKnownAtCompileTime); // TODO: we don't support zero-fill right shift (see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators)*/
			a = a.toNumber;
			b = b.toNumber;
			if (a.type == ValueType.NaN)
				return RawValue("0",ValueType.Numeric);
			if (b.type == ValueType.NaN)
				return a;
			try
			{
				if (op.matches[0][0] == '<')
				{
					return RawValue((a.value.to!int << b.value.to!int).to!string,ValueType.Numeric);
				} else
					return RawValue((a.value.to!int >> b.value.to!int).to!string,ValueType.Numeric);
			} catch (Exception e)
			{
				return RawValue("NaN",ValueType.NaN);
			}
		case "ES6.RelationalOperator": case "ES6.RelationalOperatorIn":
			if (op.matches[0][0] == 'i') // part of "instanceof" or "in"
				return RawValue("",ValueType.NotKnownAtCompileTime);
			a = a.toNumber;
			b = b.toNumber;
			if (a.type == ValueType.NaN || b.type == ValueType.NaN)
				return RawValue("false",ValueType.Bool);
			try
			{
				if (op.matches[0][0] == '<')
				{
					if (op.matches[0].length > 1 && op.matches[0][1] == '=')
						return RawValue((a.value.to!int <= b.value.to!int).to!string,ValueType.Bool);
					return RawValue((a.value.to!int < b.value.to!int).to!string,ValueType.Bool);
				} else if (op.matches[0].length > 1 && op.matches[0][1] == '=')
					return RawValue((a.value.to!int >= b.value.to!int).to!string,ValueType.Bool);
				else
					return RawValue((a.value.to!int > b.value.to!int).to!string,ValueType.Bool);
			} catch (Exception e)
			{
				return RawValue("NaN",ValueType.NaN);
			}
		case "ES6.AdditiveOperator":
			if (op.matches[0][0] == '+')
			{
				if (a.type == ValueType.String)
				{
					auto aStr = coerceToDoubleQuotedString(a.value);
					if (b.type == ValueType.String)
					{
						auto bStr = coerceToDoubleQuotedString(b.value);
						return RawValue("\""~aStr~bStr~"\"",ValueType.String);
					}
					else
						return RawValue("\""~aStr~b.value~"\"",ValueType.String);
				}
				if (b.type == ValueType.String)
				{
					auto bStr = coerceToDoubleQuotedString(b.value);
					return RawValue("\""~a.value~bStr~"\"",ValueType.String);
				}
			}
			a = a.toNumber;
			b = b.toNumber;
			if (a.type == ValueType.NaN)
				return a;
			if (b.type == ValueType.NaN)
				return b;
			try
			{
				if (op.matches[0][0] == '+')
					return RawValue((a.value.to!int + b.value.to!int).to!string,ValueType.Numeric);
				else
					return RawValue((a.value.to!int - b.value.to!int).to!string,ValueType.Numeric);
			} catch (Exception e)
			{
				return RawValue("NaN",ValueType.NaN);
			}
		case "ES6.MultiplicativeOperator":
			a = a.toNumber;
			b = b.toNumber;
			if (a.type == ValueType.NaN)
				return a;
			if (b.type == ValueType.NaN)
				return b;
			try
			{
				if (op.matches[0][0] == '*')
					return RawValue((a.value.to!int * b.value.to!int).to!string,ValueType.Numeric);
				else if (op.matches[0][0] == '%')
					return RawValue((a.value.to!int % b.value.to!int).to!string,ValueType.Numeric);
				else
					return RawValue((a.value.to!double / b.value.to!double).to!string,ValueType.Numeric);
			} catch (Exception e)
			{
				return RawValue("NaN",ValueType.NaN);
			}
		default: unitAssert("Not supported"); assert(0);
	}
}
@("doOperator")
unittest
{
	doOperator(
		RawValue("A",ValueType.NotKnownAtCompileTime),RawValue("B",ValueType.NotKnownAtCompileTime),
		new Node(ParseTree("ES6.ExpressionOperator",true))
	).shouldEqual(RawValue("A",ValueType.NotKnownAtCompileTime));
	doOperator(
		RawValue("A",ValueType.Object),RawValue("B",ValueType.NotKnownAtCompileTime),
		new Node(ParseTree("ES6.ExpressionOperator",true))
	).shouldEqual(RawValue("B",ValueType.NotKnownAtCompileTime));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.BitwiseOperator",true,["|"]))])
	).shouldEqual(RawValue("NaN",ValueType.NaN));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.ShiftOperator",true,["<<<"]))])
	).shouldEqual(RawValue("NaN",ValueType.NaN));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.RelationalOperator",true,["instanceof"]))])
	).shouldEqual(RawValue("",ValueType.NotKnownAtCompileTime));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.RelationalOperator",true,["<="]))])
	).shouldEqual(RawValue("NaN",ValueType.NaN));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.AdditiveOperator",true,["+"]))])
	).shouldEqual(RawValue("NaN",ValueType.NaN));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.MultiplicativeOperator",true,["*"]))])
	).shouldEqual(RawValue("NaN",ValueType.NaN));

	doOperator(
		RawValue("A",ValueType.Numeric),RawValue("B",ValueType.Numeric),
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.NonExistentOperator",true))])
	).shouldThrow();
}
ptrdiff_t getExprOperatorPrecedence(Node op)
{
	// Note: the Precedence roots are taken from https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
	assert(op.isA!"ES6.ExpressionOperator");
	switch (op.children[0].name)
	{
		case "ES6.LogicalOperator":
			auto root = 5;
			return root + (op.matches[0][0] == '&' ? 1 : 0);
		case "ES6.BitwiseOperator":
			auto root = 7;
			switch(op.matches[0][0])
			{
				case '|': return root;
				case '^': return root + 1;
				case '&': return root + 2;
				default: unitAssert("Operator not implemented"); assert(0);
			}
		case "ES6.EqualityOperator":
			return 10;
		case "ES6.RelationalOperator": case "ES6.RelationalOperatorIn":
			return 11;
		case "ES6.ShiftOperator":
			return 12;
		case "ES6.AdditiveOperator":
			return 13;
		case "ES6.MultiplicativeOperator":
			return 14;
		default: unitAssert("Operator not implemented"); assert(0);
	}
}
@("getExprOperatorPrecedence default throw")
unittest
{
	getExprOperatorPrecedence(
		new Node(ParseTree("ES6.ExpressionOperator",true),[new Node(ParseTree("ES6.NonExistentOperator",true))])
	).shouldThrow();
	getExprOperatorPrecedence(
		new Node(ParseTree("ES6.ExpressionOperator",true,["_"]),[new Node(ParseTree("ES6.BitwiseOperator",true,["_"]))])
	).shouldThrow();
}
Node getLowestOperator(Node binExpr)
{
	import std.algorithm : map, min, reduce;
	import std.range : stride;
	assert(binExpr.isA!"ES6.BinaryExpression");
	return binExpr.children[1..$].stride(2).reduce!(
		(a,b)
		{
			return a.getExprOperatorPrecedence < b.getExprOperatorPrecedence ? a : b;
		});
}
bool isNonCommutative(Node exprOp)
{
	assert(exprOp.isA!"ES6.ExpressionOperator");
	switch(exprOp.matches[0])
	{
		case "-":
		case "/":
			return true;
		default:
			return false;
	}
}
version (unittest)
{
	@("Test isNonCommutative")
	unittest
	{
		import es6.grammar;
		Node operator(string op)
		{
			return new Node(ParseTree("ES6.ExpressionOperator",true,[op]));
		}
		operator("-").isNonCommutative.shouldBeTrue;
		operator("/").isNonCommutative.shouldBeTrue;
		operator("+").isNonCommutative.shouldBeFalse;
		operator("*").isNonCommutative.shouldBeFalse;

	}
}
/// < 0 means opB has precedence. > 0 means opA. 0 means indifference
ptrdiff_t compareExprOperatorPrecedence(Node opA, Node opB)
{
	return getExprOperatorPrecedence(opA) - getExprOperatorPrecedence(opB);
}
auto resolveBinaryExpression(Node expr)
{
	import std.algorithm : remove;
	assert(expr.isA!"ES6.BinaryExpression");
	auto children = expr.children;
	assert(children.length % 2 == 1); //make sure length is odd
	size_t opCount = (children.length-1)/2;
	Node[] operators;
	RawValue[] values;
	foreach (idx; 0..(opCount+1))
	{
		auto raw = getRawValue!"ES6.UnaryExpression"(children[idx*2]);
		if (raw.type == ValueType.NotKnownAtCompileTime)
			return raw;
		values ~= raw;
	}
	foreach(idx; 0..opCount)
		operators ~= children[(idx*2)+1];
	version(Chatty){
		import std.stdio : write, writeln;
		import std.range : zip;
		writeln("Reducing Expression");
		foreach (z; zip(values,operators))
			write(z[0].value," ",z[1].matches[0]," ");
		writeln(values[$-1].value);
	}
	while (true)
	{
		if (opCount == 1)
		{
			// if there is only one operator, there is no precedence
			return doOperator(values[0],values[1],operators[0]);
		} else if (opCount == 2)
		{
			// when there are 2, check if second operator has higher precedence, else do operator 1 first
			if (compareExprOperatorPrecedence(operators[0],operators[1]) < 0)
				return doOperator(values[0],doOperator(values[1],values[2],operators[1]),operators[0]);
			return doOperator(doOperator(values[0],values[1],operators[0]),values[2],operators[1]);
		} else
		{
			// when there are more
			if (compareExprOperatorPrecedence(operators[0],operators[1]) >= 0)
			{
				// and the first operator has higher precedence than the second one, perform and remove operator and value
				values[1] = doOperator(values[0],values[1],operators[0]);
				operators.remove(0);
				values.remove(0);
				operators.length = operators.length-1;
				values.length = values.length-1;
				--opCount;
			} else
			{
				bool didSomething = false;
				// else we search for the operator that has higher or equal precedence than the operator on its left and its right
				foreach (idx; 1..(opCount-1))
				{
					if (compareExprOperatorPrecedence(operators[idx-1],operators[idx]) > 0)
						continue;
					if (compareExprOperatorPrecedence(operators[idx],operators[idx+1]) < 0)
						continue;
					// perform and remove operator and values
					values[idx+1] = doOperator(values[idx],values[idx+1],operators[idx]);
					operators.remove(idx);
					values.remove(idx);
					operators.length = operators.length-1;
					values.length = values.length-1;
					--opCount;
					didSomething = true;
					break;
				}
				// if we didn't do something and the last operator has higher precedence than the second to last one, perform and remove operator and value
				if (!didSomething && compareExprOperatorPrecedence(operators[$-2],operators[$-1]) <= 0)
				{
					values[$-2] = doOperator(values[$-2],values[$-1],operators[$-1]);
					operators.remove(operators.length-1);
					values.remove(values.length-1);
					operators.length = operators.length-1;
					values.length = values.length-1;
					--opCount;
				}
			}
		}
		version(Chatty){
			import std.stdio : write, writeln;
			import std.range : zip;
			foreach (z; zip(values,operators))
				write(z[0].value," ",z[1].matches[0]," ");
			writeln(values[$-1].value);
		}
	}
	assert(false);
}
auto getRawValue(alias NodeType)(Node node)
{
	assert(node.isA!NodeType);
	static if (NodeType == "ES6.RightHandSideExpression")
	{
		return getRawValue!"ES6.BinaryExpression"(node.children[0]);
	}
	else static if (NodeType == "ES6.AssignmentExpression")
	{
		auto expr = node.getNthChild(3);
		if (expr.isA!"ES6.UnaryExpression")
			return getRawValue!"ES6.UnaryExpression"(expr);
		else if (expr.isA!"ES6.BinaryExpression")
			return getRawValue!"ES6.BinaryExpression"(expr);
		return RawValue("",ValueType.NotKnownAtCompileTime);
	}
	else static if (NodeType == "ES6.Expression")
	{
		auto expr = node.getNthChild(4);
		if (expr.isA!"ES6.UnaryExpression")
			return getRawValue!"ES6.UnaryExpression"(expr);
		else if (expr.isA!"ES6.BinaryExpression")
			return getRawValue!"ES6.BinaryExpression"(expr);
		return RawValue("",ValueType.NotKnownAtCompileTime);
	} else static if (NodeType == "ES6.UnaryExpression")
	{
		auto postExpr = node.children[$-1];
		auto expr = postExpr.getNthChild(5);
		if (expr.isA!"ES6.ObjectLiteral")
			return RawValue(expr.matches[0],ValueType.Object).processPrefixExpressions(node.children[0..$-1]);
		else if (expr.isA!"ES6.Literal")
			return RawValue(expr.children[0].matches[0],expr.children[0].getLiteralNodeValueType).processPrefixExpressions(node.children[0..$-1]);
		else if (expr.isA!"ES6.IdentifierReference" && expr.matches[0] == "undefined")
			return RawValue("undefined",ValueType.Undefined).processPrefixExpressions(node.children[0..$-1]);
		else if (expr.isA!"ES6.IdentifierReference" && expr.matches[0] == "NaN")
			return RawValue("NaN",ValueType.NaN).processPrefixExpressions(node.children[0..$-1]);
		else if (expr.isA!"ES6.IdentifierReference" && expr.matches[0] == "Infinity")
			return RawValue("Infinity",ValueType.Infinity).processPrefixExpressions(node.children[0..$-1]);
		return RawValue("",ValueType.NotKnownAtCompileTime);
	} else static if (NodeType == "ES6.BinaryExpression")
	{
		return node.resolveBinaryExpression();
	} else static if (true)
		return RawValue("",ValueType.NotKnownAtCompileTime);
	assert(0);
}
version (unittest)
{
	@("Test BinaryExpression evaluations")
	unittest
	{
		assertEvalBinaryExpression("0 + 1 + 1 - 6 + 5 - 55 + 99 - 12",ValueType.Numeric,"33");
		assertEvalBinaryExpression("6 * 1 / 1 + 6 % 5 - 55 * 99",ValueType.Numeric,"-5438");
		assertEvalBinaryExpression("1 & true != 4 & 15",ValueType.Numeric,"1");
		assertEvalBinaryExpression("1 < 5 && 6 > 3",ValueType.Bool,"true");
		assertEvalBinaryExpression(`6 * "1" / 1 + "6" % 5 - "55" * 99`,ValueType.Numeric,"-5438");
		assertEvalBinaryExpression("1 <= 1 && 7 >= 7",ValueType.Bool,"true");
		assertEvalBinaryExpression("1 << 4",ValueType.Numeric,"16");
		assertEvalBinaryExpression("4 >> 1",ValueType.Numeric,"2");
		assertEvalBinaryExpression("0 || 1 & 1 == 6 < 5 + 10 * 9 + 99 != 1 * 4 & 15",ValueType.Numeric,"1");
		assertEvalBinaryExpression("0 || 1 & 1 == a < 5 + 10 * 9 + b() != 1 * 4 & 15",ValueType.NotKnownAtCompileTime);
		assertEvalBinaryExpression("0xff + 255 - 0xa0",ValueType.Numeric,"350");
		// the following are more to test string coercing rules
		assertEvalBinaryExpression(`"asdf" + "jkl;"`,ValueType.String,`"asdfjkl;"`);
		assertEvalBinaryExpression(`5 + "jkl;"`,ValueType.String,`"5jkl;"`);
		assertEvalBinaryExpression(`"asdf" + 5`,ValueType.String,`"asdf5"`);
		assertEvalBinaryExpression(`"asdf" + true`,ValueType.String,`"asdftrue"`);
		assertEvalBinaryExpression(`true + "jkl;"`,ValueType.String,`"truejkl;"`);
		assertEvalBinaryExpression(`"asdf" + false`,ValueType.String,`"asdffalse"`);
		assertEvalBinaryExpression(`false + "jkl;"`,ValueType.String,`"falsejkl;"`);
		assertEvalBinaryExpression(`"asdf" + null`,ValueType.String,`"asdfnull"`);
		assertEvalBinaryExpression(`null + "jkl;"`,ValueType.String,`"nulljkl;"`);
		assertEvalBinaryExpression(`"asdf" + undefined`,ValueType.String,`"asdfundefined"`);
		assertEvalBinaryExpression(`undefined + "jkl;"`,ValueType.String,`"undefinedjkl;"`);
		assertEvalBinaryExpression(`"asdf" + NaN`,ValueType.String,`"asdfNaN"`);
		assertEvalBinaryExpression(`NaN + "jkl;"`,ValueType.String,`"NaNjkl;"`);
		assertEvalBinaryExpression(`"asdf" + Infinity`,ValueType.String,`"asdfInfinity"`);
		assertEvalBinaryExpression(`Infinity + "jkl;"`,ValueType.String,`"Infinityjkl;"`);
		assertEvalBinaryExpression(`"asdf" + -Infinity`,ValueType.String,`"asdf-Infinity"`);
		assertEvalBinaryExpression(`-Infinity + "jkl;"`,ValueType.String,`"-Infinityjkl;"`);
		assertEvalBinaryExpression(`6 + 5 + "jkl;"`,ValueType.String,`"11jkl;"`);
		assertEvalBinaryExpression(`false + true + "jkl;"`,ValueType.String,`"1jkl;"`);
		assertEvalBinaryExpression(`false + false + "jkl;"`,ValueType.String,`"0jkl;"`);
		assertEvalBinaryExpression(`4 + null + "jkl;"`,ValueType.String,`"4jkl;"`);
		assertEvalBinaryExpression(`true + undefined + "jkl;"`,ValueType.String,`"NaNjkl;"`);
		assertEvalBinaryExpression(`undefined + NaN + "jkl;"`,ValueType.String,`"NaNjkl;"`);
		assertEvalBinaryExpression(`NaN + Infinity + "jkl;"`,ValueType.String,`"NaNjkl;"`);
		assertEvalBinaryExpression(`0 < 0 + 1 * 6`,ValueType.Bool,`true`);
		assertEvalBinaryExpression(`0 << 0 | 1 ^ 6`,ValueType.Numeric,`7`);

		assertEvalBinaryExpression(`NaN > 55`,ValueType.Bool,"false");
		assertEvalBinaryExpression(`55 < NaN`,ValueType.Bool,"false");
	}
	import unit_threaded;
	@Name("Static Reduce")
	unittest
	{
		assertExpressionIs(`true`,Ternary.True);
		assertExpressionIs(`false`,Ternary.False);
		assertExpressionIs(`""`,Ternary.False);
		assertExpressionIs(`"non-empty-string"`,Ternary.True);
		assertExpressionIs(`null`,Ternary.False);
		assertExpressionIs(`0`,Ternary.False);
		assertExpressionIs(`!-1`,Ternary.False);
		assertExpressionIs(`void delete !-1`,Ternary.False);
		assertExpressionIs(`true == true`,Ternary.True);
		assertExpressionIs(`!null == !undefined`,Ternary.True);
		assertExpressionIs(`"abcd" == "abcd"`,Ternary.True);
		assertExpressionIs(`"abcd" == "abcde"`,Ternary.False);
		assertExpressionIs(`"abcd" == fun()`,Ternary.None);
		assertExpressionIs(`abcd.bla('df') == -1`,Ternary.None);
		assertExpressionIs(`abcd.bla('df') > -1`,Ternary.None);
		//assertExpressionIs(`{} == {}`,Ternary.True).shouldThrow();
		assertExpressionIs(`!{}`,Ternary.False);
	}
}
auto toUnaryExpression(RawValue value)
{
	final switch (value.type)
	{
		case ValueType.Undefined:
		case ValueType.NaN:
		case ValueType.Infinity:
		case ValueType.Null:
			return createUnaryExpressionNullLiteral();
		case ValueType.Bool:
			bool v = value.value == "true";
			return v.createUnaryExpressionBoolLiteral();
		case ValueType.String:
			if (value.value.length < 3)
				return "".createUnaryExpressionStringLiteral();
			return value.value[1..$-1].createUnaryExpressionStringLiteral();
		case ValueType.Numeric:
			return value.value.createUnaryExpressionDecimalLiteral();
		case ValueType.Object:
		case ValueType.NotKnownAtCompileTime:
			unitAssert("Not supported"); assert(0);
	}
}
version (unittest)
{
	@("Test toUnaryExpression")
	unittest
	{
		toUnaryExpression(RawValue("",ValueType.Undefined)).getChild("ES6.NullLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.NaN)).getChild("ES6.NullLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.Infinity)).getChild("ES6.NullLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.Null)).getChild("ES6.NullLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.Bool)).getChild("ES6.BooleanLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.String)).getChild("ES6.StringLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue(`"str"`,ValueType.String)).getChild("ES6.StringLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.Numeric)).getChild("ES6.DecimalLiteral").shouldNotBeNull();
		toUnaryExpression(RawValue("",ValueType.Object)).shouldThrow();
		toUnaryExpression(RawValue("",ValueType.NotKnownAtCompileTime)).shouldThrow();
	}
}

/*@("Test ")
unittest {
	negate binary expressions
	!a => a
	!a && !b => !(!a && !b) => a || b
	!(a && b) => (a && b)
	!a || b => !(!a || b) => !!a && !b => a && !b
	a >= 6 => a < 6
	a == 6 => a != 6
	a < 7 => a >= 7
	a > 7 && !b => !(a > 7 && !b) => !(a > 7) || b => a <= 7 || b
	a && !b || (c && !d) => !(a && !b || (c && !d)) => !a || b && !(c && !d) => !a || b && !c || d
	a == 6 && b > 6 => !(a == 6 && b > 6) => a != 6 || b <= 6

	In some cases swapping the condition results a shorted expression
	(a||b)?bla():boo() => !a&&!b?boo():bla()
	but only in presence of prefix operators
	(a||!b)?bla():boo() => !a&&b?boo():bla()
	or with relational operators
	(a==6||b==7)?bla():boo() =>  a!=6&&b!=7?boo():bla()

	When an condition contains prefix operators and parenthesis, it might be shorter if we remove the prefix and negate the parens	
	!(a||!b)?bla():boo() => !a&&b?bla():boo()
	or with relational operators
	!(a>=6||b==6)?bla():boo() => a<6&&b!=6?bla():boo()

	simple if conditions
	!a&&bla() => a||bla();
	!(a||!b)&&doBla() => !a&&b&&doBla();
}*/

