module es5.transforms.voidparenthesis;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

bool parenthesisVoidExpression(Node node)
{
	auto expr = node.asExpression();
	if (expr.isNull || expr.children.length == 1)
		return false;
	auto assign = expr.getLastAssignmentExpression();
	auto prefix = assign.getNthChild(4);
	if (!prefix.isA!"ES6.PrefixExpression" || prefix.matches[0] != "void")
		return false;
	auto unExpr = assign.getNthChild(3);
	auto numLiteral = unExpr.children[1].getNthChild(6);
	if (unExpr.children.length != 2 || !numLiteral.isA!"ES6.NumericLiteral" || numLiteral.matches[0] != "0")
		return false;
	auto assigns = expr.children[0..$-2];
	expr.children = [];
	auto newExpr = new Node(ParseTree("ES6.ExpressionIn",true),assigns);
	auto newAssign = newExpr.parenthesisExprIntoAssignmentExpression();
	expr.setChildren([newAssign]);
	unExpr = expr.getNthChild(4);
	unExpr.insertAtFront(new Node(ParseTree("ES6.PrefixExpression",true,["void"])));
	return true;
}

unittest
{
	import es5.testhelpers;
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(parenthesisVoidExpression)(js,expected,file,line);
	}
	assertTransformation(
		`d = 5, void 0`,
		`void (d = 5)`
	);
	assertTransformation(
		`function b() { return a ? 6 : (d = 5, void 0) }`,
		`function b() { return a ? 6 : (void (d = 5)) }`
	);
}