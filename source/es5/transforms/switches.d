module es5.transforms.switches;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

bool removeUnnecessaryBreakStatements(Node node)
{
	if (!node.isA!"ES6.CaseBlock")
		return false;
	node = node.children[0].children[$-1];
	if (!node.isA!"ES6.CaseClauses")
		return false;
	auto lastCase = node.children[$-1];
	assert(lastCase.isA!"ES6.CaseClause");
	if (lastCase.children.length == 1)
		return false;
	auto list = lastCase.children[1].asStatementList;
	if (list.children.length == 0)
		return false;
	if (list.children[$-1].getNthChild(2).isA!"ES6.BreakStatement")
	{
		list.children = list.children[0..$-1];
		return true;
	}
	return false;
}

unittest
{
	import es5.testhelpers;
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(removeUnnecessaryBreakStatements)(js,expected,file,line);
	}
	/// Issue #14
	assertTransformation(
		`switch (b) { case 6: bla(); break; }`,
		`switch (b) { case 6: bla(); }`
	);
	assertTransformation(
		`switch (b) { case 6: bla(); break;case 5:  }`,
		`switch (b) { case 6: bla(); break;case 5:  }`
	);
	assertTransformation(
		`switch (b) { case 7: hup(); break;case 6: bla(); break; }`,
		`switch (b) { case 7: hup(); break;case 6: bla(); }`
	);
	assertTransformation(
		`switch (b) { default: hup(); break; case 6: bla(); break; }`,
		`switch (b) { default: hup(); break; case 6: bla(); }`
	);
	assertTransformation(
		`switch (b) { case 7: hup(); break;default: goP(); break; case 6: bla(); break; }`,
		`switch (b) { case 7: hup(); break;; default: goP(); break; case 6: bla(); }`
	);
	assertTransformation(
		`switch (b) { case 7: hup(); break;case 6: bla(); break;default: goP(); break }`,
		`switch (b) { case 7: hup(); break;case 6: bla(); break;; default: goP(); break }`
	);
}