module es5.transforms.loops;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

version (unittest){
	import unit_threaded;
}
bool moveBreakstatementsIntoLoopCondition(Node node)
{
	bool test(Node item)
	{
		return (item.hint & Hint.Break) ? true : false;
	}
	import std.algorithm : countUntil, all;
	auto iter = node.asIterationStatement;
	if (iter.isNull)
		return false;
	if (!iter.canHaveCondition())
		return false;
	if (iter.hasBlockStatement)
	{
		auto list = iter.getStatementList;
		auto idx = list.children[0..$].countUntil!test;
		if (idx == -1)
			return false;

		if (idx != 0)
			return false;

		if (!list.children[0..idx].all!(item => !(item.hint & Hint.NonValueGeneratingStatement)))
			return false;
		//auto assigns = list.children[0..idx].map!(c => c.getNthChild(3).stride(2)).joiner;
		//auto expr = assigns.createExpressionFromAssignmentExpressions;
		auto breakItem = list.children[idx];
		auto ifStmt = breakItem.getNthChild(2).asIfStatement;
		if (ifStmt.isNull)
			return false;

		if (ifStmt.hasElsePath && (ifStmt.elsePath.hint & Hint.Break))
			return false;

		if (ifStmt.truthPath.isBlockStatement() && ifStmt.truthPath.getStatementList.children.length > 1)
			return false;

		ifStmt.truthPath.branch.removeFromParent();
		auto assignCondition = ifStmt.conditionExpression.getLastAssignmentExpression;
		assignCondition.negateExpression();

		if (iter.hasCondition)
		{
			auto iterCond = iter.getCondition().asExpression;
			combineConditionalExpressions(iterCond,ifStmt.conditionExpression);
		} else
		{
			iter.setCondition(ifStmt.conditionExpression);
		}

		list.children = list.children[idx+1..$];
		if (ifStmt.hasElsePath)
		{
			ifStmt.elsePath.branch.removeFromParent();
			auto elsePath = ifStmt.elsePath();
			if (elsePath.isBlockStatement())
			{
				list.insertAtFront(ifStmt.elsePath.getStatementList().children);
			} else
			{
				list.insertAtFront(new Node(ParseTree("ES6.StatementListItem",true),[ifStmt.elsePath.asNode]));
			}
		}
		reanalyse(list);
		return true;
	}
	auto stmt = iter.getStatement();
	if (!(stmt.hint & Hint.Break))
		return false;
	auto ifStmt = stmt.getNthChild(1).asIfStatement();
	if (ifStmt.isNull)
		return false;
	if (ifStmt.hasElsePath && (ifStmt.elsePath.hint & Hint.Break))
		return false;
	if (ifStmt.truthPath.isBlockStatement() && ifStmt.truthPath.getStatementList.children.length > 1)
		return false;

	auto assignCondition = ifStmt.conditionExpression.getLastAssignmentExpression;
	assignCondition.negateExpression();

	ifStmt.truthPath.branch.removeFromParent();
	if (iter.hasCondition)
	{
		auto iterCond = iter.getCondition().asExpression;
		combineConditionalExpressions(iterCond,ifStmt.conditionExpression);
	} else
	{
		iter.setCondition(ifStmt.conditionExpression);
	}
	if (ifStmt.hasElsePath)
	{
		ifStmt.elsePath.branch.removeFromParent();
		iter.setStatement(ifStmt.elsePath.node);
	} else
		iter.setStatement(
			new Node(ParseTree("ES6.Statement",true),[
				new Node(ParseTree("ES6.EmptyStatement",true,[";"]))
			])
		);
	reanalyse(iter);
	return true;
} 

unittest
{
	import es5.testhelpers;
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(moveBreakstatementsIntoLoopCondition)(js,expected,file,line);
	}
	assertTransformation(
		`for (e = 0; e < c[dm]; e++) { if (a[bv]()) k = 6; b(a, c[e], d[e]) }`,
		`for(e = 0;e < c[ dm ];e++){ if (a[ bv ]()) k = 6; b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; e < c[dm]; e++) { do { k = 6 } while(true); if (a[bv]()) break; b(a, c[e], d[e]) }`,
		`for(e = 0;e < c[ dm ];e++){ do k = 6;while (true); if (a[ bv ]()) break; b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (k in obj) { if (a[bv]()) break; b(a, c[e], d[e]) }`,
		`for(k in obj){ if (a[ bv ]()) break; b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; e < c[dm]; e++) { if (a[bv]()) break; b(a, c[e], d[e]) }`,
		`for(e = 0;e < c[ dm ] && !a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; ; e++) { if (a[bv]()) break; b(a, c[e], d[e]) }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; ; e++) { if (a[bv]()) { break; } b(a, c[e], d[e]) }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; ; e++) { if (a[bv]()) { break; } else b(a, c[e], d[e]) }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; ; e++) { if (a[bv]()) { break; } else { b(a, c[e], d[e]) } }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; ; e++) { if (a[bv]()) { break; } else { b(a, c[e], d[e]); k = 7 } }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]); k = 7 }`
	);
	assertTransformation(
		`for (e = 0; ; e++) { if (a[bv]()) { break; } else { b(a, c[e], d[e]); k = 7 } g = 7 }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]); k = 7; g = 7 }`
	);

	assertTransformation(
		`for(e = 0;;e++){ for(;;)break }`,
		`for(e = 0;;e++){ for(;;)break }`
	);
	assertTransformation(
		`for(e = 0;;e++)for(;;)break;`,
		`for(e = 0;;e++)for(;;)break`
	);
	assertTransformation(
		`for (e = 0; ; e++) if (a[bv]()) { break; } else b(a, c[e], d[e])`,
		`for(e = 0;!a[ bv ]();e++)b(a, c[ e ], d[ e ])`
	);
	assertTransformation(
		`for (e = 0; ; e++) if (a[bv]()) { break; } else { b(a, c[e], d[e]) }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; ; e++) if (a[bv]()) { break; } else { b(a, c[e], d[e]); k = 7 }`,
		`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]); k = 7 }`
	);

	assertTransformation(
		`for (e = 0; e < c[dm]; e++) if (a[bv]()) break;`,
		`for(e = 0;e < c[ dm ] && !a[ bv ]();e++);`
	);

	assertTransformation(
		`for (;;) { if (b) k = 6; else if (c) break }`,
		`for(;;){ if (b) k = 6; else  if (c) break; }`
	);
	assertTransformation(
		`for (;;) if (b) k = 6; else if (c) break;`,
		`for(;;)if (b) k = 6; else  if (c) break;`
	);
	assertTransformation(
		`for (e = 0; e < c[dm]; e++) { if (a[bv]()) { g = 6; break; } b(a, c[e], d[e]) }`,
		`for(e = 0;e < c[ dm ];e++){ if (a[ bv ]()) { g = 6; break }; b(a, c[ e ], d[ e ]) }`
	);
	assertTransformation(
		`for (e = 0; e < c[dm]; e++) if (a[bv]()) { g = 6; break; } b(a, c[e], d[e])`,
		`for(e = 0;e < c[ dm ];e++)if (a[ bv ]()) { g = 6; break }; b(a, c[ e ], d[ e ])`
	);

	assertTransformation(
		`while(a){  }`,
		`while(a){  }`
	);

	assertTransformation(
		`while ( i-- ) { b = 5; if ( a ) { break; } }`,
		`while(i--){ b = 5; if (a) { break } }`
	);
	//assertTransformation(
	//	`for (e = 0; e < c[dm]; e++) { if (a[bv]()) { g = 6; break; } b(a, c[e], d[e]) }`,
	//	`for(e = 0; e < c[dm] && !(a[ bv ]() && (g = 6,!0)); e++) { b(a, c[e], d[e]) }`
	//);
	//assertTransformation(
	//	`for (e = 0; e < c[dm]; e++) { k = 66; if (a[bv]()) { g = 6; break; } b(a, c[e], d[e]) }`,
	//	`for (n = 0; n < o[Hn] && !e[_r](); n++) t(e, o[n], r[n]);`
	//);
	//assertTransformation(
	//	`for (e = 0; ; e++) { if (a[bv]()) { break; } else if (k == 77) break; else { b(a, c[e], d[e]); k = 7 } g = 7 }`,
	//	`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]); k = 7; g = 7 }`
	//);
	//assertTransformation(
	//	`for (e = 0; ; e++) if (a[bv]()) { break; } else { b(a, c[e], d[e]); k = 7 }`,
	//	`for(e = 0;!a[ bv ]();e++){ b(a, c[ e ], d[ e ]) }`
	//);
	//assertTransformation(
	//	`for (e = 0; e < c[dm]; e++) if (a[bv]()) break; else b(a, c[e], d[e])`,
	//	`for (n = 0; n < o[Hn] && !e[_r](); n++) t(e, o[n], r[n]);`
	//);


	//assertTransformation(
	//	`for (e = 0; e < c[dm]; e++) { h = 7; break; b(a, c[e], d[e]) }`,
	//	`for(e = 0;e < c[ dm ];e++){ if (a[ bv ]()) k = 6; b(a, c[ e ], d[ e ]) }`
	//);
}