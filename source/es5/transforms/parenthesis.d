module es5.transforms.parenthesis;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

bool removeUnnecessaryParentheses(Node node)
{
	bool moveParenthesizedExpressionIntoParentBinaryExpression(ref Expression expr, Node parentBinaryExpression)
	{
		auto unExpr = node.getNthParent(7);
		if (unExpr.children.length > 1) // if the parentheses is prefixed with an PrefixExpression
			return false;
		auto inBinIdx = parentBinaryExpression.findIndex(unExpr);
		if (expr.isSingleExpression && expr.isBinaryExpression)
		{
			auto childOperator = expr.getLastAssignmentExpression().getNthChild(3).getLowestOperator();
			if (inBinIdx > 0)
			{
				auto parentLeftOperator = parentBinaryExpression.children[inBinIdx-1];
				auto pp = parentLeftOperator.getExprOperatorPrecedence;
				auto cp = childOperator.getExprOperatorPrecedence;
				if (pp > cp)
					return false;
				if (pp == cp && 
					(
						/*parentLeftOperator.matches[0] != childOperator.matches[0] &&*/
						parentLeftOperator.isNonCommutative
					))
					return false;
				/*if (parentLeftOperator.isNonCommutative)
					return false;*/
			}
			if (parentBinaryExpression.children.length > inBinIdx+1)
			{
				auto parentRightOperator = parentBinaryExpression.children[inBinIdx+1];
				auto pp = parentRightOperator.getExprOperatorPrecedence;
				auto cp = childOperator.getExprOperatorPrecedence;
				if (pp > cp)
					return false;
			}
		} else if (inBinIdx != 0)
			return false;
		auto exprsToTransfer = expr.children[0..$-1];
		expr.children = expr.children[$-1..$];
		auto binToTransfer = expr.children[$-1].getNthChild(3);
		auto assignExpr = parentBinaryExpression.getNthParent(3);
		assert(assignExpr.isA!"ES6.AssignmentExpression");
		auto exprOrg = assignExpr.parent;
		auto inExprIdx = exprOrg.findIndex(assignExpr);
		exprOrg.insertInPlace(inExprIdx,exprsToTransfer);
		if (!(expr.children[0].hint & Hint.LogicalOr))
		{
			if (binToTransfer.isA!"ES6.UnaryExpression")
			{
				parentBinaryExpression.setChild(inBinIdx,binToTransfer);
			} else 
			{
				assert(binToTransfer.isA!"ES6.BinaryExpression");
				assert(binToTransfer.children.length > 1);
				unExpr.removeFromParent();
				parentBinaryExpression.insertInPlace(inBinIdx,binToTransfer.children);
			}
			reanalyse(parentBinaryExpression);
		}
		reanalyse(exprOrg);
		return true;
	}
	if (!node.isA!"ES6.Parentheses")
		return false;
	if (node.children.length == 0)
	{
		auto membExpr = node.getNthParent(2);
		if (node.parent.isA!"ES6.Arguments" && membExpr.children[0].isA!"ES6.NewKeyword")
		{
			if (membExpr.isLastChild(node.parent))
			{
				membExpr.parent.insertAtFront(membExpr.children[0]);
				membExpr.children = membExpr.children[1..$-1];
				return true;
			}
		}
		return false;
	} else if (node.parent.isA!"ES6.CoverParenthesizedExpressionAndArrowParameterList" && node.parent.parent.isA!"ES6.PrimaryExpression")
	{
		if (node.getNthParent(4).isA!"ES6.CallExpression")
			return false;
		auto expr = node.children[0].asExpression();
		if (expr.isNull)
			return false;
		auto membExpr = node.getNthParent(3);
		if (membExpr.isA!"ES6.MemberExpression" && membExpr.children.length > 1)
			return false;
		if (!expr.isSingleExpression)
		{
			//if (expr.isBinaryExpression)
				//return false;
			auto unParentExpr = node.getNthParent(8);
			if (unParentExpr.isA!"ES6.BinaryExpression")
			{
				if (unParentExpr.getNthParent(4).isA!"ES6.ConditionalExpression")
					return false;
				return moveParenthesizedExpressionIntoParentBinaryExpression(expr,unParentExpr);
			}
			auto tmp = unParentExpr.getNthParent(3);
			if (tmp.isA!"ES6.ConditionalExpression")
				return false;
			bool isPartOfConditionalExpression = unParentExpr.parent.children.length > 1;
			bool hasPrefixExpressionBeforeParenthesis = unParentExpr.children[0].children.length > 1;
			auto assignExpr = unParentExpr.getNthParent(2);
			auto exprOrg = tmp.asExpression();
			assert(!exprOrg.isNull);
			auto idx = exprOrg.findIndex(assignExpr);
			if (hasPrefixExpressionBeforeParenthesis ||
				expr.isConditionalExpression && (expr.isPartOfBinaryExpression || expr.isPartOfConditionalExpression))
			{
				auto exprToTransfer = expr.children[0..$-1];
				expr.children = expr.children[$-1..$];
				exprOrg.insertInPlace(idx,exprToTransfer);
				reanalyse(expr);
			} else
			{
				auto exprToTransfer = expr.children[0..$-1];
				expr.children = expr.children[$-1..$];
				if (expr.isConditionalExpression)
				{
					auto condExpr = expr.getNthChild(2);
					assert(condExpr.isA!"ES6.ConditionalExpression");
					exprOrg.children[idx].setChild(0,condExpr);
				} else
				{
					auto rhsExpr = expr.getNthChild(3);
					assert(rhsExpr.isA!"ES6.RightHandSideExpression");
					exprOrg.children[idx].getNthChild(1).setChild(0,rhsExpr);
				}
				exprOrg.insertInPlace(idx,exprToTransfer);
				reanalyse(expr);
			}
			reanalyse(exprOrg);
			return true;
		}
		if (node.getNthChild(11).isA!"ES6.FunctionExpression")
			return false;
		if (expr.hint & Hint.Assignment)
		{
			if (expr.isPartOfBinaryExpression)
				return false;
			auto unExprOrg = node.getNthParent(7);
			// when the parent's UnaryExpression has a PrefixExpression, we cannot remove parens
			if (unExprOrg.isA!"ES6.UnaryExpression" && unExprOrg.children.length > 1)
				return false;
			auto assignExpr = expr.getAssignmentExpression();
			auto parentExpr = expr.getNthParent(12);
			if (parentExpr.isA!"ES6.ConditionalExpression")
				return false;
			auto childExpr = expr.getNthParent(11);
			auto idx = parentExpr.findIndex(childExpr);
			parentExpr.setChild(idx,assignExpr);
			reanalyse(parentExpr);
			return true;
		}
		if (expr.isConditionalExpression && (expr.isPartOfBinaryExpression || expr.isPartOfConditionalExpression))
			return false;
		auto unExprOrg = node.getNthParent(7);
		if (expr.isConditionalExpression && unExprOrg.children.length > 1)
			return false;
		if (expr.isConditionalExpression)
		{
			auto condExpr = expr.getNthChild(2);
			auto rhsExpr = unExprOrg.parent;
			assert(rhsExpr.isA!"ES6.RightHandSideExpression");
			auto parentCondExpr = rhsExpr.parent;
			parentCondExpr.setChildren(condExpr.children);
			reanalyse(parentCondExpr);
			return true;
		}
		if (expr.isUnaryExpression)
		{
			assert(unExprOrg.isA!"ES6.UnaryExpression");
			auto idx = unExprOrg.parent.findIndex(unExprOrg);
			auto unExpr = expr.getUnaryExpression();
			unExprOrg.parent.setChild(idx,unExpr);
			unExpr.insertAtFront(unExprOrg.children[0..$-1]);
			reanalyse(unExprOrg);
			reanalyse(unExpr);
			return true;
		} else if (expr.isBinaryExpression)
		{
			auto rhsChild = unExprOrg.parent;
			if (unExprOrg.children.length > 1)
				return false; // the parentheses is prefix with a PrefixExpression
			if (rhsChild.isA!"ES6.BinaryExpression")
			{
				if (expr.hint & Hint.LogicalOr)
					return false;
				return moveParenthesizedExpressionIntoParentBinaryExpression(expr,rhsChild);
			} else // its a RightHandSideExpression
			{
				assert(rhsChild.isA!"ES6.RightHandSideExpression");
				auto condExpr = rhsChild.getNthParent(1);
				condExpr.setChild(0,expr.getAssignmentExpression.getNthChild(2));
				reanalyse(condExpr);
				return true;
			}
		}
	}
	return false;
}

unittest
{
	import es5.testhelpers;
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(removeUnnecessaryParentheses)(js,expected,file,line);
	}
	/// Issue #91
	assertTransformation(`(1 - 2) - 3 + 4`,	`1 - 2 - 3 + 4`);
	assertTransformation(`(1 - 2) + 3 + 4`,	`1 - 2 + 3 + 4`);
	assertTransformation(`(1 + 2) - 3 + 4`,	`1 + 2 - 3 + 4`);
	assertTransformation(`(1 + 2) + 3 + 4`,	`1 + 2 + 3 + 4`);
	assertTransformation(`1 - 2 - (3 - 4)`,	`1 - 2 - (3 - 4)`);
	assertTransformation(`1 - 2 - (3 + 4)`,	`1 - 2 - (3 + 4)`);
	assertTransformation(`1 - 2 + (3 - 4)`,	`1 - 2 + 3 - 4`);
	assertTransformation(`1 - 2 + (3 + 4)`,	`1 - 2 + 3 + 4`);

	assertTransformation(`(1 - 2) * 3 + 4`,	`(1 - 2) * 3 + 4`);
	assertTransformation(`(1 - 2) / 3 + 4`,	`(1 - 2) / 3 + 4`);
	assertTransformation(`(1 + 2) * 3 + 4`,	`(1 + 2) * 3 + 4`);
	assertTransformation(`(1 + 2) / 3 + 4`,	`(1 + 2) / 3 + 4`);
	assertTransformation(`1 - 2 * (3 - 4)`,	`1 - 2 * (3 - 4)`);
	assertTransformation(`1 - 2 * (3 + 4)`,	`1 - 2 * (3 + 4)`);
	assertTransformation(`1 - 2 / (3 - 4)`,	`1 - 2 / (3 - 4)`);
	assertTransformation(`1 - 2 / (3 + 4)`,	`1 - 2 / (3 + 4)`);

	assertTransformation(`(1 * 2) * 3 + 4`,	`1 * 2 * 3 + 4`);
	assertTransformation(`(1 * 2) / 3 + 4`,	`1 * 2 / 3 + 4`);
	assertTransformation(`(1 / 2) * 3 + 4`,	`1 / 2 * 3 + 4`);
	assertTransformation(`(1 / 2) / 3 + 4`,	`1 / 2 / 3 + 4`);
	assertTransformation(`1 - 2 * (3 * 4)`,	`1 - 2 * 3 * 4`);
	assertTransformation(`1 - 2 / (3 * 4)`,	`1 - 2 / (3 * 4)`);
	assertTransformation(`1 - 2 * (3 / 4)`,	`1 - 2 * 3 / 4`);
	assertTransformation(`1 - 2 / (3 / 4)`,	`1 - 2 / (3 / 4)`);

	assertTransformation(`!(1 - 2,b) - 3 + 4`, `!(1 - 2,b) - 3 + 4`); //although this can be transformed to `1 - 2, !b - 3 + 4`

	assertTransformation(
		`+(a + b)`,
		`+(a + b)`
	);
	assertTransformation(
		`+(a ? c : d)`,
		`+(a ? c : d)`
	);
	/// Issue #88
	assertTransformation(
		`if ( !( events = elemData.events ) ) { events = elemData.events = {}; }`,
		`if (!(events = elemData.events)) { events = elemData.events = {} }`
	);
	/// Issue #82
	assertTransformation(
		`(freeModule.exports = Rx).Rx = Rx`,
		`(freeModule.exports = Rx).Rx = Rx`
	);
	/// ditto
	assertTransformation(
		`(gekko(),freeModule.exports = Rx).Rx = Rx`,
		`(gekko(),freeModule.exports = Rx).Rx = Rx`
	);
	/// Issue #74
	assertTransformation(
		`function checkGlobal(value) { return (value && value.Object === Object) ? value : null }`,
		`function checkGlobal(value) { return value && value.Object === Object ? value : null }`
	);
	assertTransformation(
		`function checkGlobal(value) { return (a && bla(), value && value.Object === Object) ? value : null }`,
		`function checkGlobal(value) { return a && bla(),value && value.Object === Object ? value : null }`
	);
	/// cannot remove parenthesis when it contains a ConditionalExpression
	assertTransformation(
		`function checkGlobal(value) { return (a ? "bla" : g) ? value : null }`,
		`function checkGlobal(value) { return (a ? "bla" : g) ? value : null }`
	);
	/// ditto
	assertTransformation(
		`function checkGlobal(value) { return (doBla(),a ? "bla" : g) ? value : null }`,
		`function checkGlobal(value) { return doBla(),(a ? "bla" : g) ? value : null }`
	);
	assertTransformation(
		`if ((a && b)) doBla();`,
		`if (a && b) doBla();`
	);
	assertTransformation(
		`if (((a || b))) doBla();`,
		`if (a || b) doBla();`
	);
	assertTransformation(
		`if (((a && b) && c) && d) doBla();`,
		`if (a && b && c && d) doBla();`
	);
	assertTransformation(
		`if (((a || b) && c) && (d || e)) doBla();`,
		`if ((a || b) && c && (d || e)) doBla();`
	);
	assertTransformation(
		`d = 6, (b = 6, a),u = 7`,
		`d = 6,b = 6,a,u = 7`
	);
	assertTransformation(
		`d = 6, (b = 6),u = 7`,
		`d = 6,b = 6,u = 7`
	);
	assertTransformation(
		`c ? (b = 6, a) : 6`,
		`c ? (b = 6,a) : 6`
	);
	assertTransformation(
		`if ((b=6,(a))) doBla();`,
		`if (b = 6,a) doBla();`
	);
	assertTransformation(
		`if ((b=6, (a) || b)) doBla();`,
		`if (b = 6,a || b) doBla();`
	);
	assertTransformation(
		`if ((b=6, (a) && b)) doBla();`,
		`if (b = 6,a && b) doBla();`
	);
	assertTransformation(
		`if (!(b=6, (a) && b)) doBla();`,
		`if (b = 6,!(a && b)) doBla();`
	);
	assertTransformation(
		`b = a ? (d = 5,6) : (g = 12,7)`,
		`b = a ? (d = 5,6) : (g = 12,7)`
	);
	assertTransformation(
		`b = a ? (d = 5,6) && g : (g = 12,7) || h`,
		`b = a ? (d = 5,6) && g : (g = 12,7) || h`
	);
	assertTransformation(
		`b = a ? g && (d = 5,6) : h || (g = 12,7)`,
		`b = a ? g && (d = 5,6) : h || (g = 12,7)`
	);
	assertTransformation(
		`b = a ? (g && (d = 5,6)) : (h || (g = 12,7))`,
		`b = a ? g && (d = 5,6) : h || (g = 12,7)`
	);
	assertTransformation(
		`if ((b=6, (a)) || b) doBla();`,
		`if (b = 6,a || b) doBla();`
	);
	assertTransformation(
		`(b = 6, a)`,
		`b = 6,a`
	);
	assertTransformation(
		`(b = 6)`,
		`b = 6`
	);
	assertTransformation(
		`inst.refs === emptyObject ? (inst.refs = {}) : inst.refs`,
		"inst.refs === emptyObject ? (inst.refs = {}) : inst.refs"
	);
	assertTransformation(
		`a = (b && g in window)`,
		"a = b && g in window"
	);
	assertTransformation(
		`(b = a ? 6 : 7)`,
		`b = a ? 6 : 7`
	);
	assertTransformation(
		`a && (b = a ? 6 : 7)`,
		`a && (b = a ? 6 : 7)`
	);
  	/// Issue #42
  	assertTransformation(
  		`var a = ("a " + propName + " b ") + ("c.");`,
        `var a = "a " + propName + " b " + "c.";`
  	);
  	/// Issue 75
  	assertTransformation(
  		`;(function (factory) { doSomething() }.call(this, function (root, exp, Rx, undefined) { return undefined }));`,
  		`; (function (factory){ doSomething() }.call(this, function (root, exp, Rx, undefined){ return undefined }))`
  	);
  	/// Issue 83
  	assertTransformation(
  		`()`,
  		`()`
  	);
  	assertTransformation(
  		`bla()`,
  		`bla()`
  	);
  	// Issue #79
  	assertTransformation(
  		`new bla()`,
  		`new bla`
  	);
  	assertTransformation(
  		`if (typeof new root.Set()[ '@@iterator' ] === 'function') a = 6;`,
  		`if (typeof new root.Set()[ '@@iterator' ] === 'function') a = 6;`
  	);
  	assertTransformation(
  		`(a ? b : c)(b)`,
  		`(a ? b : c)(b)`
  	);
  	assertTransformation(
  		`(d = 5,a ? b : c)`,
  		`d = 5,a ? b : c`
  	);
  	assertTransformation(
  		`(a ? b : c)`,
  		`a ? b : c`
  	);
  	assertTransformation(
  		`(a && b ? c : d) && e`,
  		`(a && b ? c : d) && e`
  	);
  	assertTransformation(
  		`(...rest)`,	// TODO: we dont support the spread operator but still want the parenthesis function to handle it
  		`(rest)`		// ofcourse, should be (...rest). No matter, will do when we start supporting ES6 as output
  	);
  	assertTransformation(
		`function b() { return a ? 6 : (void (d = 5)) }`,
  		`function b() { return a ? 6 : void (d = 5) }`
  	);
}